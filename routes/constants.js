/**
 * The node-module to hold the constants for the server
 */


function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value: value,
        enumerable: true,
        writable: false,
        configurable: false
    });
}
exports.responseErrors = {};
error = {};
define(exports.responseErrors, "EMAIL_AUTH", {'error': 'An error has been occurred while authentication.', flag: 0});
define(exports.responseErrors, "UN_AUTHORIZED_ACCESS", {'error': 'Unauthorized access.', flag: 1});
define(exports.responseErrors, "FETCHING_DATA", {'error': 'An error has been occurred while fetching data.', flag: 2});
define(exports.responseErrors, "USER_NOT_REGISTERED", {'error': 'User with this email is not registered.', flag: 3});
define(exports.responseErrors, "EMAIL_ALREADY_REGISTERED", {'error': 'Email already registered.', flag: 4});
define(exports.responseErrors, "MANDATORY_FIELDS", {'error': 'Please fill all the required fields.', flag: 5});
define(exports.responseErrors, "INVALID_ACCESS", {'error': 'Please login again. Invalid access.', flag: 6});
define(exports.responseErrors, "INVALID_TOKEN", {'error': 'Invalid token.', flag: 7});
define(exports.responseErrors, "INCORRECT_OLD_PASSWORD", {'error': 'You seem to have entered the current password incorrect. Please try again.', flag: 8});
define(exports.responseErrors, "UPDATION_ERROR", {'error': 'An error has been occurred while processing your request.', flag: 9});
define(exports.responseErrors, "INCORRECT_PROMO_CODE", {'error': 'You seem to have entered the promo code incorrect. Please try again.', flag: 10});
define(exports.responseErrors, "USER_DOES_NOT_EXIST", {'error': 'This user does not exist.', flag: 11});
define(exports.responseErrors, "BANK_DETAILS_NOT_CONFIGURED", {'error': 'User has not configure his/her bank details. Please try after sometime!', flag: 12});
define(exports.responseErrors, "INVALID_STRIPE_TOKEN", {'error': 'Invalid stripe token', flag: 13});
define(exports.responseErrors, "UNABLE_TO_CREATE_NEW_TRIP", {'error': 'Unable to create new trip!', flag: 14});
define(exports.responseErrors, "TRIP_NAME_ALREADY_EXIST", {'error': 'Trip name already exists!', flag: 15});
define(exports.responseErrors, "STRIPE_TOKEN_ERROR", {'error': 'An error has been occurred while processing Stripe.', flag: 16});
define(exports.responseErrors, "TWILIO_ERROR", {'error': 'An error has been occurred while processing Twilio.', flag: 17});
define(exports.responseErrors, "UNABLE_TO_SET_ON_MY_WAY", {'error': 'You have been set as rider.', flag: 18});
define(exports.responseErrors, "RIDER_ALREADY_SET_PICKUP_LOCATION", {'error': 'Rider has already set its pick up location.', flag: 19});
define(exports.responseErrors, "INVALID_REFERRAL_CODE", {'error': 'Invalid Referral code!', flag: 20});
define(exports.responseErrors, "USER_DELETED", {'error': 'User has been deleted!', flag: 21});
define(exports.responseErrors, "USER_BLOCKED", {'error': 'User has been blocked!', flag: 22});
define(exports.responseErrors, "VERSION_ERROR", {'error': 'An error has been occurred while verifying your app version!', flag: 23});
define(exports.responseErrors, "OPPONENT_ALSO_SET_AS_DRIVER", {'error': ' You both set up in driver profile.', flag: 24});
define(exports.responseErrors, "INCORRECT_USER_NAME_OR_PASSWORD", {'error': ' Incorrect username or password.', flag: 25});

exports.responseSuccess = {};
log = {};
define(exports.responseSuccess, "EMAIL_VERIFIED", {'log': 'Success.', flag: 21});
define(exports.responseSuccess, "INSTRUCTIONS_FOR_SIGNING_IN", {'log': 'Instructions for signing in have been emailed to you.', flag: 22});
define(exports.responseSuccess, "TOKEN_VERIFIED", {'log': 'Token verified.', flag: 23});
define(exports.responseSuccess, "USER_LOGOUT_SUCCESSFULLY", {'log': 'User has been logout successfully.', flag: 24});
define(exports.responseSuccess, "PROFILE_UPDATED_SUCCESSFULLY", {'log': 'Your profile has been updated successfully.', flag: 25});
define(exports.responseSuccess, "FEEDBACK_UPDATED_SUCCESSFULLY", {'log': 'Your feedback has been updated successfully.', flag: 26});
define(exports.responseSuccess, "REQUEST_IS_UNDER_CONSIDERATION", {'log': 'Your request is under consideration.', flag: 27});
define(exports.responseSuccess, "USER_RATED_SUCCESSFULLY", {'log': 'User has been rated successfully.', flag: 28});
define(exports.responseSuccess, "SETTINGS_CHANGED", {'log': 'User settings has been changed successfully.', flag: 29});
define(exports.responseSuccess, "USER_DELETED_SUCCESSFULLY", {'log': 'User has been deleted successfully.', flag: 30});
define(exports.responseSuccess, "LOCATION_UPDATED_SUCCESSFULLY", {'log': 'Your location has been updated successfully.', flag: 31});
define(exports.responseSuccess, "CARD_REMOVED_SUCCESSFULLY", {'log': 'Your card has been removed successfully.', flag: 32});
define(exports.responseSuccess, "SECRET_KEY_UPDATED_SUCCESSFULLY", {'log': 'Your secret key has been updated successfully.', flag: 33});
define(exports.responseSuccess, "PAYMENT_MADE_SUCCESSFULLY", {'log': 'Payment has been done successfully.', flag: 34});
define(exports.responseSuccess, "FACEBOOK_INVITATION_SENT_SUCCESSFULLY", {'log': 'Facebook invitation has been done successfully.', flag: 35});
define(exports.responseSuccess, "USER_HYTCHED_SUCCESSFULLY", {'log': 'User has been hytched successfully.', flag: 36});
define(exports.responseSuccess, "USER_DYTCHED_SUCCESSFULLY", {'log': 'User has been dytched successfully.', flag: 37});
define(exports.responseSuccess, "TRIP_VERIFIED", {'log': 'Success.', flag: 38});
define(exports.responseSuccess, "TRIP_SET_AS_DEFAULT_SUCCESSFULLY", {'log': 'Your trip has been set as default successfully.', flag: 39});
define(exports.responseSuccess, "USER_BLOCKED_SUCCESSFULLY", {'log': 'User has been blocked successfully.', flag: 40});
define(exports.responseSuccess, "USER_UN_BLOCK_SUCCESSFULLY", {'log': 'User has been unblocked successfully.', flag: 41});
define(exports.responseSuccess, "USER_REGISTERED_SUCCESSFULLY", {'log': 'User has been registered successfully.', flag: 42});
define(exports.responseSuccess, "RIDE_INFORMATION_UPDATED_SUCCESSFULLY", {'log': 'User ride information has been updated successfully.', flag: 43});
define(exports.responseSuccess, "PROMO_CODE_UPDATED_SUCCESSFULLY", {'log': 'Promo code has been updated successfully.', flag: 44});
define(exports.responseSuccess, "PROMO_CODE_REMOVED_SUCCESSFULLY", {'log': 'Promo code has been removed successfully.', flag: 45});
define(exports.responseSuccess, "PROMO_CODE_CREATED_SUCCESSFULLY", {'log': 'Promo code has been created successfully.', flag: 46});
define(exports.responseSuccess, "MAIL_SENT_SUCCESSFULLY", {'log': 'Mail has been sent successfully.', flag: 47});
define(exports.responseSuccess, "PASSWORD_RESET_SUCCESSFULLY", {'log': 'The password has been reset and sent to your registered email.', flag: 48});
define(exports.responseSuccess, "CARD_UPDATED_SUCCESSFULLY", {'log': 'Your card has been updated successfully.', flag: 49});
define(exports.responseSuccess, "WAIT_LIST_CLEARED_SUCCESSFULLY", {'log': 'Wait list has been cleared successfully.', flag: 50});
define(exports.responseSuccess, "PASSWORD_CHANGE_SUCCESSFULLY", {'log': 'The password has been changed successfully.', flag: 51});

exports.userProfileType = {};
define(exports.userProfileType, "RIDER", 1);
define(exports.userProfileType, "DRIVER", 2);
define(exports.userProfileType, "BOTH", 3);
define(exports.userProfileType, "ADMIN", 4);

exports.userDeviceType = {};
define(exports.userDeviceType, "ANDROID", 1);
define(exports.userDeviceType, "IOS", 2);

exports.userSignUp = {};
define(exports.userSignUp, "VIA_FACEBOOK", 1);
define(exports.userSignUp, "VIA_WAIT_LIST_CODE", 2);

exports.getWaitListUserData = {};
define(exports.getWaitListUserData, "THROUGH_EMAIL", 1);
define(exports.getWaitListUserData, "THROUGH_WAIT_CODE", 2);

exports.defaultValueSet = {};
define(exports.defaultValueSet, "AT_SIGN_UP_TO_ONE",          1);
define(exports.defaultValueSet, "AFTER_SIGN_UP_TO_ZERO",          0);

exports.errorResponseCode = {};
define(exports.errorResponseCode, "WHILE_FACEBOOK_AUTHENTICATION",          200);

exports.defaultUserSettingValue = {};
define(exports.defaultUserSettingValue, "ENABLE",          1);
define(exports.defaultUserSettingValue, "DISABLE",          0);

exports.defaultUserLocationValue = {};
define(exports.defaultUserLocationValue, "LATITUDE",          0.000000);
define(exports.defaultUserLocationValue, "LONGITUDE",          0.000000);

exports.tripManipulationStatus = {};
define(exports.tripManipulationStatus, "EDIT",          1);
define(exports.tripManipulationStatus, "DELETE",          2);

exports.userPaymentCardStatus = {};
define(exports.userPaymentCardStatus, "ADD_NEW_CARD",          0);
define(exports.userPaymentCardStatus, "DELETE_OLD_CARD",          1);

exports.userDeleteStatus = {};
define(exports.userDeleteStatus, "NOT_DELETED",          0);
define(exports.userDeleteStatus, "DELETED",          1);

exports.userBlockedStatus = {};
define(exports.userBlockedStatus, "BLOCKED",          1);
define(exports.userBlockedStatus, "UN_BLOCK",          2);

exports.getConversationListOf = {};
define(exports.getConversationListOf, "BLOCKED_USER",          1);
define(exports.getConversationListOf, "UN_BLOCK_USER",          2);

exports.userPromoCodeStaus = {};
define(exports.userPromoCodeStaus, "NOT_EXIST",          0);
define(exports.userPromoCodeStaus, "EXIST",          1);

exports.userHytchOrDytchStaus = {};
define(exports.userHytchOrDytchStaus, "HYTCH",          1);
define(exports.userHytchOrDytchStaus, "DYTCH",          2);

exports.userRegisterThrough = {};
define(exports.userRegisterThrough, "WEB",          1);
define(exports.userRegisterThrough, "APP",          2);

exports.userChatMessageType = {};
define(exports.userChatMessageType, "TEXT_MESSAGE",          1);
define(exports.userChatMessageType, "SHARE_LOCATION",          2);
define(exports.userChatMessageType, "ON_THE_WAY",          3);

exports.pushMessageToUser = {};
define(exports.pushMessageToUser, "FOR_CONFIGURE_BANK_DETAILS",          ' wants to pay your amount. Please configure your bank details.');
define(exports.pushMessageToUser, "FOR_VIEW_LOCATION",          ' requested to view your location.');
define(exports.pushMessageToUser, "FOR_MUTAL_HYTCH",          'Congratulations you have match with ');
define(exports.pushMessageToUser, "ON_MY_WAY",          ' is on the way!');
define(exports.pushMessageToUser, "I_AM_HERE",          ' has reached!');
define(exports.pushMessageToUser, "START_RIDE",          ' has started the ride!');
define(exports.pushMessageToUser, "END_RIDE",          ' has completed the ride! Rate now.');
define(exports.pushMessageToUser, "CANCEL_RIDE",          ' has canceled the ride!');
define(exports.pushMessageToUser, "PICKUP_LOCATION_SET_BY_RIDER",          'Pickup location has been set by ');


exports.userRideStatus = {};
define(exports.userRideStatus, "NEW_OR_COMPLETED_RIDE",          0);
define(exports.userRideStatus, "ON_MY_WAY",          1);
define(exports.userRideStatus, "I_AM_HERE",          2);
define(exports.userRideStatus, "START_RIDE",          3);
define(exports.userRideStatus, "END_RIDE",          4);
define(exports.userRideStatus, "PAYMENT_OR_RATING",          5);
define(exports.userRideStatus, "CANCEL_RIDE",          6);

exports.userRideCompleteStatus = {};
define(exports.userRideCompleteStatus, "NOT_COMPLETED",          0);
define(exports.userRideCompleteStatus, "RUNNING",          1);
define(exports.userRideCompleteStatus, "COMPLETED",          2);

exports.rideCompletedByDriverOrRider = {};
define(exports.rideCompletedByDriverOrRider, "NOT_COMPLETED",          0);
define(exports.rideCompletedByDriverOrRider, "COMPLETED",          1);

exports.riderPickUpStatus = {};
define(exports.riderPickUpStatus, "UPDATED_BY_RIDER",          1);
define(exports.riderPickUpStatus, "UPDATED_BY_DRIVER",          2);


exports.rideMissedPushStatus = {};
define(exports.rideMissedPushStatus, "ADD_NEW_DETAILS_FOR_TRACK",          1);
define(exports.rideMissedPushStatus, "REMOVE_DETAILS",          2);

exports.defaultMessage = {};
define(exports.defaultMessage, "FOR_CONVERSATION_LIST",          'Say Hello!');

exports.pushNotificationsType = {};
define(exports.pushNotificationsType, "SEND_NEW_MESSAGE",          0);
define(exports.pushNotificationsType, "MUTUAL_HYTCH",          1);
define(exports.pushNotificationsType, "ON_MY_WAY",          2);
define(exports.pushNotificationsType, "DRIVER_HAS_NOT_CONFIGURE_BANK_DETAILS",          3);
define(exports.pushNotificationsType, "I_AM_HERE_OR_START_OR_END_RIDE",          4);
define(exports.pushNotificationsType, "PICK_UP_LOCATION_UPDATED_BY_RIDER",          5);
define(exports.pushNotificationsType, "CANCEL_RIDE",          6);


exports.defaultImage = {};
define(exports.defaultImage, "USER",          'default_user.png');
//Admin constants

exports.showGraphStatus = {};
define(exports.showGraphStatus, "DAILY",          1);
define(exports.showGraphStatus, "MONTHLY",          2);
define(exports.showGraphStatus, "YEARLY",          3);



exports.waitListClearStatus = {};
define(exports.waitListClearStatus, "CLEARED_OFFSET",          1);
define(exports.waitListClearStatus, "CLEARED_ALL",          2);
