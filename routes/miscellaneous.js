/**
 * Created by click labs on 10/15/14.
 */
var func = require('./functions');
var constants = require('./constants');
var async = require('async');
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Update user app settings
 * Input : accces token,new matches,messages,location sharing,push notification,profile rating
 * Output :log message
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.updateUserAppSettingsThroughAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    var newMatches = req.body.newmatches;
    var messages = req.body.messages;
    var locationSharing = req.body.location_sharing;
    var pushNotification = req.body.pushnotification;
    var profileRating = req.body.profilerating;
    var manValues = [accessToken];
    console.log(req.body);
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = [];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            updateUserAppSettingsThroughAccessTokenResult(resultsFromAccessToken, newMatches, messages, locationSharing, pushNotification, profileRating, res, req);
        });
    });
};
function updateUserAppSettingsThroughAccessTokenResult(resultsFromAccessToken, newMatches, messages, locationSharing, pushNotification, profileRating, res, req) {
    var sql = "UPDATE `user` SET `new_matches`=?, `messages`=?, `location_sharing`=?, `push_notifications`=?, `profile_rating`=?  WHERE `user_id` = ? LIMIT 1";
    connection.query(sql, [newMatches, messages, locationSharing, pushNotification, profileRating, resultsFromAccessToken[0].user_id], function (err, result) {
        if (!err) {
            var response = constants.responseSuccess.SETTINGS_CHANGED;
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Delete user account
 * Input : access token
 * Output :log message
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.deleteUserAccountThroughAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    var manValues = [accessToken];
    console.log(req.body);
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ['user_type'];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            deleteUserAccountThroughAccessTokenResult(resultsFromAccessToken, res, req);
        });
    });
};
function deleteUserAccountThroughAccessTokenResult(resultsFromAccessToken, res, req) {
    if (resultsFromAccessToken[0].user_type == constants.userDeviceType.ANDROID) {
        var sql = "UPDATE `user` SET `is_deleted`=?, `android_device_token`=? WHERE `user_id` = ? LIMIT 1";
    }
    else if (resultsFromAccessToken[0].user_type == constants.userDeviceType.IOS) {
        var sql = "UPDATE `user` SET `is_deleted`=?, `ios_device_token`=? WHERE `user_id` = ? LIMIT 1";
    }
    connection.query(sql, [constants.userDeleteStatus.DELETED, '', resultsFromAccessToken[0].user_id], function (err, result) {
        if (!err) {
            var response = constants.responseSuccess.USER_DELETED_SUCCESSFULLY;
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Update user location
 * Input : access token, current Location
 * Output :log message
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.updateUserLocationThroughAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var locationAccuracy = req.body.location_accuracy;
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = [];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            updateUserLocationThroughAccessTokenResult(resultsFromAccessToken, latitude, longitude, locationAccuracy, res, req);
        });
    });
};
function updateUserLocationThroughAccessTokenResult(resultsFromAccessToken, latitude, longitude, locationAccuracy, res, req) {
    var sql = "UPDATE `user` SET `current_latitude`=?,`current_longitude`=?,`location_accuracy`=?  WHERE `user_id` = ? LIMIT 1";
    connection.query(sql, [latitude, longitude, locationAccuracy, resultsFromAccessToken[0].user_id], function (err, result) {
        if (!err) {
            var response = constants.responseSuccess.LOCATION_UPDATED_SUCCESSFULLY;
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Save trip as default
 * Input : access token, trip id
 * Output :log message
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.saveTripAsDefaultThroughAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    var tripId = req.body.tripid;
    var profile = req.body.profile;
    var tripName = req.body.tripname;
    var manValues = [accessToken];
    console.log(req.body);
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["default_trip"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            saveTripAsDefaultThroughAccessTokenResult(resultsFromAccessToken, tripId, tripName, profile, res, req);
        });
    });
};
function saveTripAsDefaultThroughAccessTokenResult(resultsFromAccessToken, tripId, tripName, profile, res, req) {
    var sql = "UPDATE `user` SET `default_trip`=?,`default_trip_name`=?, `profile`=?  WHERE `user_id`=? LIMIT 1";
    connection.query(sql, [tripId, tripName, profile, resultsFromAccessToken[0].user_id], function (err, result) {
        var sql = "UPDATE `trip` SET `default`=? WHERE `user_id` = ?";
        connection.query(sql, [0, resultsFromAccessToken[0].user_id], function (err, result) {
            var sql = "UPDATE `trip` SET `default`=? WHERE `id` = ? LIMIT 1";
            connection.query(sql, [constants.defaultValueSet.AT_SIGN_UP_TO_ONE, tripId], function (err, result) {
                if (!err) {
                    var response = constants.responseSuccess.TRIP_SET_AS_DEFAULT_SUCCESSFULLY;
                    res.jsonp(response);
                }
            });
        });
    });
}
/*
 * -----------------------------------------------------------------------------
 * Add or remove user card from profile
 * Input : email
 * Output : log or error message
 * -----------------------------------------------------------------------------
 */
exports.addOrRemoveUserCardFromProfileThroughCardId = function (req, res) {
    var accessToken = req.body.accesstoken;
    var stripeToken = req.body.stripetoken;
    var card = req.body.card;
    var id = req.body.id;
    var type = parseInt(req.body.type);
    console.log(req.body)
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["email"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            addOrRemoveUserCardFromProfileThroughCardIdResult(resultsFromAccessToken, stripeToken, card, id, type, res);
        });
    });
};
function addOrRemoveUserCardFromProfileThroughCardIdResult(resultsFromAccessToken, stripeToken, card, id, type, res) {
    if (type === constants.userPaymentCardStatus.ADD_NEW_CARD) {
        var stripe = require("stripe")(config.get('stripeSettings.key'));
        stripe.customers.create({
            card: stripeToken,
            email: resultsFromAccessToken[0].email
        }, function (err, customer) {
            if (err) {
                var response = constants.responseErrors.STRIPE_TOKEN_ERROR;
                res.jsonp(response);
            }
            else if (!err) {
                //var cardNumber = customer['cards']['data'][0].last4;
                var cardNumber = customer['sources']['data'][0].last4;
                var customerId = customer.id;
                var userId = resultsFromAccessToken[0].user_id;
                registerUserCardInformationForFuturePayments(customerId, userId, cardNumber, card, constants.defaultUserSettingValue.DISABLE, function (id) {
                    if (id) {
                        var cardId = id;
                        cardArray = [];
                        cardArray.push({"id": id, "customer_id": customerId, "card": card, "card_number": cardNumber, "default": constants.defaultUserSettingValue.DISABLE});
                        var response = {"cards": cardArray};
                        res.jsonp(response);
                    }
                });
            }
        });
    }
    else if (type === constants.userPaymentCardStatus.DELETE_OLD_CARD) {
        var sql = "DELETE FROM `user_card` WHERE `id`=? && `user_id`=? LIMIT 1";
        connection.query(sql, [id, resultsFromAccessToken[0].user_id], function (err, resultsFromDeletion) {
            if (!err) {
                var response = constants.responseSuccess.CARD_REMOVED_SUCCESSFULLY;
                res.jsonp(response);
            }
        });
    }
}
/*
 * -----------------------------------------------------------------------------
 * Register user card information for future payments
 * Input : email
 * Output : log or error message
 * -----------------------------------------------------------------------------
 */
function registerUserCardInformationForFuturePayments(customerId, userId, cardNumber, card, defaultCard, callback) {
    var timestamp = new Date();
    var sql = "INSERT INTO `user_card`(`customer_id`,`user_id`,`card_number`,`card`,`default`,`created_at`) VALUES (?,?,?,?,?,?)";
    connection.query(sql, [customerId, userId, cardNumber, card, defaultCard, timestamp], function (err, resultFromCards) {
        if (!err) {
            var cardId = resultFromCards.insertId;
            return callback(cardId);
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Update user secret key for payment
 * Input : access token, secret key
 * Output : log or error message
 * -----------------------------------------------------------------------------
 */
exports.updateUserSecretKeyForPayment = function (req, res) {
    var accessToken = req.body.accesstoken;
    var secretKey = req.body.secretkey;
    var manValues = [accessToken, secretKey];
    console.log(req.body)
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = [];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            updateUserSecretKeyForPaymentResult(resultsFromAccessToken, secretKey, res);
        });
    });
};
function updateUserSecretKeyForPaymentResult(resultsFromAccessToken, secretKey, res) {
    var sql = "UPDATE `user` SET `stripe_secret_key`=?  WHERE  `user_id`=? LIMIT 1";
    connection.query(sql, [secretKey, resultsFromAccessToken[0].user_id], function (err, resultsFromCards) {
        if (!err) {
            var response = constants.responseSuccess.SECRET_KEY_UPDATED_SUCCESSFULLY;
            res.jsonp(response);
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Make payment for trip
 * Input : access token, id
 * Output : log or error message
 * -----------------------------------------------------------------------------
 */
exports.MakePaymentForTrip = function (req, res) {
    var accessToken = req.body.accesstoken;
    var userId = req.body.user_id;
    var userName = req.body.username;
    var customerId = req.body.customerid;
    var amount = req.body.amount;
    var promoAmount = req.body.promoamount;
    var isPromo = req.body.ispromo;
    var rating = req.body.rating;
    var rideId = req.body.ride_id;
    var rating = req.body.rating;
    var promoCode = req.body.promo_code;
    var manValues = [accessToken, userId];
    console.log(req.body)
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["user_type", "first_name", "last_name","total_transactions", "payment_made"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            MakePaymentForTripResult(resultsFromAccessToken, userId, amount, userName, customerId, promoAmount, isPromo, rideId, rating, promoCode, res);
        });
    });
};
function MakePaymentForTripResult(resultsFromAccessToken, userId, amount, userName, customerId, promoAmount, isPromo, rideId, rating, promoCode, res) {
    var sql = "SELECT `first_name`,`stripe_secret_key`,`user_type`,`android_device_token`,`ios_device_token`,`total_transactions`,`payment_received`,`is_deleted` FROM `user` WHERE `user_id`=? LIMIT 1"
    connection.query(sql, [userId], function (err, resultFromUsers) {
        var userName = resultsFromAccessToken[0].first_name ;
        var message = userName + constants.pushMessageToUser.FOR_CONFIGURE_BANK_DETAILS;
        if (resultFromUsers[0].is_deleted == constants.userDeleteStatus.DELETED) {
            if(rideId){
                var sql = "UPDATE `user_ride` SET `ride_status`=?, `ride_running_status`=?, `driver_rating`=?,";
                sql += " `is_ride_completed_by_rider`=?,`rider_pickup_latitude`=?, `rider_pickup_longitude`=?,`driver_start_latitude`=?,";
                sql += " `driver_start_longitude`=?,`is_set_pickup_location_by_rider`=?  WHERE `id` = ?  LIMIT 1";
                connection.query(sql, [constants.userRideStatus.NEW_OR_COMPLETED_RIDE, constants.userRideCompleteStatus.COMPLETED,
                    rating,constants.rideCompletedByDriverOrRider.COMPLETED, constants.defaultUserLocationValue.LATITUDE,
                    constants.defaultUserLocationValue.LONGIUDE,constants.defaultUserLocationValue.LATITUDE,
                    constants.defaultUserLocationValue.LONGIUDE,constants.userRideStatus.NEW_OR_COMPLETED_RIDE, rideId], function (err, result) {
                });
            }
            var response = constants.responseErrors.USER_DOES_NOT_EXIST;
            res.jsonp(response);
        }
        else if (resultFromUsers[0].stripe_secret_key == '') {
            var response = constants.responseErrors.BANK_DETAILS_NOT_CONFIGURED;
            res.jsonp(response);
            var androidTokenArray = new Array();
            var iosTokenArray = new Array();
            if (resultFromUsers[0].android_device_token != '') {
                androidTokenArray.push(resultFromUsers[0].android_device_token);
            }
            if (resultFromUsers[0].ios_device_token != '' || resultFromUsers[0].ios_device_token != '(null)') {
                iosTokenArray.push(resultFromUsers[0].ios_device_token);
            }
            if (resultFromUsers[0].user_type == constants.userDeviceType.ANDROID) {
                var androidMessage = ({"message_key": message, "type": constants.pushNotificationsType.DRIVER_HAS_NOT_CONFIGURE_BANK_DETAILS, "badge": 1})
                func.androidPushNotification(androidTokenArray, androidMessage);
            }
            else {
                var iosSendMessage = ({"type": constants.pushNotificationsType.DRIVER_HAS_NOT_CONFIGURE_BANK_DETAILS})
                var iosUnread = new Array();
                iosUnread.push(1);
                func.iosPushNotification(iosTokenArray, message, iosSendMessage, iosUnread);
            }

        }
        else {
            var customerIdForPayment;
            var money;
            async.series([
                function(callback1){

            var flag  = 0;
            if (isPromo == constants.userPromoCodeStaus.EXIST) {
                console.log('DDDaa');
                var stripe = require("stripe")(config.get('stripeSettings.key'));
                stripe.charges.create({
                    amount: amount,
                    currency: "usd",
                    customer: customerId, // obtained with Stripe.js
                    description: "Charge for Hytch. User applied a promocode."

                }, function (err, charge) {
                    if (err) {
                        var response = {"error": err, "flag": 1};
                        res.jsonp(response);
                    }
                    else{

                        var sql = "SELECT `customer_id` FROM `admin_card` WHERE `default`=? LIMIT 1 ";
                        connection.query(sql, [constants.defaultUserSettingValue.ENABLE], function (err, resultsFromAdminCard) {
                            console.log(resultsFromAdminCard);
                         customerIdForPayment = 'cus_6CxWQRcbRozgwo';
                            console.log(customerIdForPayment);
                            //
                             money = (parseInt(promoAmount) - 0) + (parseInt(amount) - 0);
                            callback1();
                        });
                    }
                });
            }
            else {
           customerIdForPayment = customerId;
                money = amount;
                callback1();
            }
                },function(callback1){
                    console.log(customerIdForPayment)
            var secretKey = resultFromUsers[0].stripe_secret_key;
            var id = customerIdForPayment;
            money = Math.ceil(money);
            var stripe = require("stripe")(secretKey);
            stripe.tokens.create(
                {customer: id},
                secretKey,
                function (err, token) {
                    console.log('HELLSS');
                    if (err) {
                        console.log(err);
                        var response = constants.responseErrors.INVALID_STRIPE_TOKEN;
                        res.jsonp(response);
                    }
                    else {
                        pushArray = [];
                        async.series([
                            function (callback) {
                                var applicationFee = Math.ceil(.0005 * money)
                                console.log(applicationFee);
                                stripe.charges.create({
                                    amount: money,
                                    currency: "usd",
                                    card: token.id, // obtained with Stripe.js
                                    description: "Driver payment for Hytch.",
                                    application_fee: applicationFee
                                }, function (err, charge) {
                                    if (err) {
                                        console.log(err);
                                        var response = constants.responseErrors.INVALID_STRIPE_TOKEN;
                                        return callback(response)
                                    }
                                    console.log(charge);
                                    callback();
                                });
                            },
                            function (callback) {
                                var timestamp = new Date();
                                var sql = "INSERT INTO `payment`(`from_id`,`to_id`,`from_name`,`to_name`,`amount`,`customer_id`,`ride_id`,`promocode`,`created_at`) VALUES (?,?,?,?,?,?,?,?,?)";
                                connection.query(sql, [resultsFromAccessToken[0].user_id, userId, resultsFromAccessToken[0].first_name, resultFromUsers[0].first_name, amount, id, rideId, promoCode, timestamp],
                                    function (err, resultsFromPayments) {
                                        if (!err) {
                                            var userTransactions = resultsFromAccessToken[0].total_transactions + 1;
                                            var opponentTransactions = resultFromUsers[0].total_transactions + 1;
                                            var resultForPaymentMade = resultsFromAccessToken[0].payment_made;
                                            var userPaymentMade = (resultForPaymentMade - 0) + (amount - 0);
                                            var resultForPaymentReceived = resultFromUsers[0].payment_received;
                                            var opponentPaymentReceive = (resultForPaymentReceived - 0) + (amount - 0);
                                            var sql = "UPDATE `user` SET `total_transactions`=?,`payment_made`=? WHERE  `user_id`=? LIMIT 1";
                                            connection.query(sql, [userTransactions, userPaymentMade, resultsFromAccessToken[0].user_id], function (err, resultsFromMade) {
                                                if (err) {
                                                    var response = constants.responseErrors.UPDATION_ERROR;
                                                    return callback(response)
                                                }
                                                else {
                                                    var sql = "UPDATE `user` SET `total_transactions`=?,`payment_received`=? WHERE  `user_id`=? LIMIT 1";
                                                    connection.query(sql, [opponentTransactions, opponentPaymentReceive, userId], function (err, resultsFromReceived) {
                                                        if (err) {
                                                            var response = constants.responseErrors.UPDATION_ERROR;
                                                            return callback(response)
                                                        }
                                                        else {
                                                            callback()
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                        else {
                                            var response = constants.responseErrors.UPDATION_ERROR;
                                            return callback(response)
                                        }
                                    });
                            },function (callback) {
                                if(rideId !=0){
                                    func.getUserRideDetails(resultsFromAccessToken[0].user_id, userId, function(err, responseFromRide){
                                        if(err){
                                            var response = constants.responseErrors.UPDATION_ERROR;
                                            return callback(response)
                                        }
                                        else{
                                            if (responseFromRide[0].is_ride_completed_by_driver == constants.rideCompletedByDriverOrRider.COMPLETED) {
                                                var sql = "UPDATE `user_ride` SET `ride_status`=?, `ride_running_status`=?, `driver_rating`=?,";
                                                sql += " `is_ride_completed_by_rider`=?,`rider_pickup_latitude`=?, `rider_pickup_longitude`=?,`driver_start_latitude`=?,";
                                                sql += " `driver_start_longitude`=?,`is_set_pickup_location_by_rider`=?  WHERE `id` = ?  LIMIT 1";
                                                connection.query(sql, [constants.userRideStatus.NEW_OR_COMPLETED_RIDE, constants.userRideCompleteStatus.COMPLETED,
                                                    rating,constants.rideCompletedByDriverOrRider.COMPLETED, constants.defaultUserLocationValue.LATITUDE,
                                                    constants.defaultUserLocationValue.LONGIUDE,constants.defaultUserLocationValue.LATITUDE,
                                                    constants.defaultUserLocationValue.LONGIUDE,constants.userRideStatus.NEW_OR_COMPLETED_RIDE, rideId], function (err, result) {
                                                    var response = constants.responseErrors.UPDATION_ERROR;
                                                    console.log(err);
                                                    if (err) return callback(response);
                                                    callback();
                                                });
                                            }
                                            //Else update rider ride completion status
                                            else {
                                                var sql = "UPDATE `user_ride` SET `ride_status`=?, `driver_rating`=?, `is_ride_completed_by_rider`=?";
                                                sql +=   " WHERE `id` = ?  LIMIT 1";
                                                connection.query(sql, [constants.userRideStatus.PAYMENT_OR_RATING,
                                                    rating, constants.rideCompletedByDriverOrRider.COMPLETED, rideId], function (err, result) {
                                                    var response = constants.responseErrors.UPDATION_ERROR;
                                                    console.log(err);
                                                    if (err) return callback(response);
                                                    callback();
                                                });
                                            }
                                        }
                                    });
                                }
                                else{
                                    callback();
                                }

                            }], function (err, callbackResponse) {
                            if (err) {
                                res.jsonp(err);
                            }
                            else {
                                var response = constants.responseSuccess.PAYMENT_MADE_SUCCESSFULLY;
                                res.jsonp(response);
                            }
                        });

                    }
                }
            );
                }
        ])
        }

    });
}
/*
 * -----------------------------------------------------------------------------
 * Verify promo code for payment
 * Input : promo code
 * Output : log or error message
 * -----------------------------------------------------------------------------
 */
exports.verifyPromoCode = function (req, res) {
    var promoCode = req.body.promocode;
    var manValues = [promoCode];
    console.log(req.body)
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["value"];
        func.authenticatePromoAmountAndReturnExtraData(promoCode, extraData, res, function (resultsFromPromoCode) {
            verifyPromoCodeResult(resultsFromPromoCode, res);
        });
    });
};
function verifyPromoCodeResult(resultsFromPromoCode, res) {
    var response = {"amount": (resultsFromPromoCode[0].value * 100)};
    res.jsonp(response);
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get user invitation
 * Input : access token, fb ids
 * Output :log message
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.updateUserInvitationsFromFacebookThroughAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    var fbIds = req.body.fbids;
    var manValues = [accessToken];
    console.log(req.body);
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["default_trip"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            updateUserInvitationsFromFacebookThroughAccessTokenResult(resultsFromAccessToken, fbIds, res, req);
        });
    });
};
function updateUserInvitationsFromFacebookThroughAccessTokenResult(resultsFromAccessToken, fbIds, res, req) {
    var fbIdsArray = fbIds.split(',');
    var fbUserCount = fbIdsArray.length;
    var jsonDataInvitedFriends = '';
    for (var j = 0; j < fbUserCount; j++) {
        var image = "http://graph.facebook.com/" + fbIdsArray[j] + "/picture?width=225&height=225";
        jsonDataInvitedFriends += "  (" + resultsFromAccessToken[0].user_id + "," + fbIdsArray[j] + ",'" + image + "'" + "),";
    }
    jsonDataInvitedFriends = jsonDataInvitedFriends.substring(0, jsonDataInvitedFriends.length - 1);
    var sql = "INSERT INTO `fb_invitation`(`user_id`,`fb_id`,`image`) VALUES  " + jsonDataInvitedFriends + "";
    connection.query(sql, function (err, result) {
        console.log(err);
        if (!err) {
            var response = constants.responseSuccess.FACEBOOK_INVITATION_SENT_SUCCESSFULLY;
            res.jsonp(response);
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 *  Get user logout from app
 * Input : access token
 * Output : log or error message
 * -----------------------------------------------------------------------------
 */
exports.getUserLogoutFromApp = function (req, res) {
    var accessToken = req.body.accesstoken;
    var manValues = [accessToken];
    console.log(req.body)
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ['user_type'];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getUserLogoutFromAppResult(resultsFromAccessToken, res);
        });
    });
};
function getUserLogoutFromAppResult(resultsFromAccessToken, res) {
    if (resultsFromAccessToken[0].user_type == constants.userDeviceType.ANDROID) {
        var sql = "UPDATE `user` SET `android_device_token`=? WHERE `user_id`=? LIMIT 1";
    }
    else {
        var sql = "UPDATE `user` SET `ios_device_token`=? WHERE `user_id`=? LIMIT 1";
    }
    connection.query(sql, ['', resultsFromAccessToken[0].user_id], function (err, resultsFromUsers) {
        if (!err) {
            var response = constants.responseSuccess.USER_LOGOUT_SUCCESSFULLY;
            res.jsonp(response);
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Block or unblock user through access token
 * Input : access token
 * Output : log or error message
 * -----------------------------------------------------------------------------
 */
exports.blockOrUnblockUserThroughAccessToken = function (req, res) {
    var accessToken = req.body.access_token;
    var blockedId = req.body.blocked_id;
    var flag = parseInt(req.body.flag);
    var manValues = [accessToken, blockedId, flag];
    console.log(req.body)
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = [];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            blockOrUnblockUserThroughAccessTokenResult(resultsFromAccessToken, flag, blockedId, res);
        });
    });
};
function blockOrUnblockUserThroughAccessTokenResult(resultsFromAccessToken, flag, blockedId, res) {
    if (flag === constants.userBlockedStatus.BLOCKED) {
        var sql = "INSERT INTO `blocked_user`(`user_id`,`blocked_id`) VALUES (?,?)";
        connection.query(sql, [resultsFromAccessToken[0].user_id, blockedId], function (err, resultsFromBlock) {
            if (!err) {
                var response = constants.responseSuccess.USER_BLOCKED_SUCCESSFULLY;
                res.jsonp(response);
            }
            else {
                var response = constants.responseSuccess.UPDATION_ERROR;
                res.jsonp(response);
            }
        });
    }
    else if (flag === constants.userBlockedStatus.UN_BLOCK) {
        var sql = "DELETE FROM `blocked_user` WHERE `user_id`=? AND `blocked_id`=? LIMIT 1";
        connection.query(sql, [resultsFromAccessToken[0].user_id, blockedId], function (err, resultsFromBlock) {
            if (!err) {
                var response = constants.responseSuccess.USER_UN_BLOCK_SUCCESSFULLY;
                res.jsonp(response);
            }
            else {
                var response = constants.responseSuccess.UPDATION_ERROR;
                res.jsonp(response);
            }
        });
    }
}
