/**
 * Created by clicklabs on 11/18/14.
 */
var func = require('./functions');
var constants = require('./constants');
var md5 = require('MD5');
var async = require('async');
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get data for dashboard
 * Input : access token
 * Output :user's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getDataForDashboard = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var accessToken = req.body.accesstoken;
    var type = req.body.type;
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["user_type"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getDataForDashboardResult(resultsFromAccessToken, type, res, req);
        });
    });
};
function getDataForDashboardResult(resultsFromAccessToken, type, res, req) {
    if (type == constants.userProfileType.RIDER) {
        var sql = "SELECT  COUNT(*) AS total , MONTH(`register_timestamp`) MONTH, COUNT(*) COUNT FROM user WHERE YEAR(`register_timestamp`)= YEAR(CURDATE()) && `is_deleted`=? &&";
        sql += " `profile`= ? GROUP BY MONTH(`register_timestamp`)"
        connection.query(sql, [constants.userDeleteStatus.NOT_DELETED, constants.userProfileType.RIDER], function (err, resultFromDashboard) {
            var dashboardLen = resultFromDashboard.length;
            var graphArray = new Array();
            var sum = constants.defaultUserSettingValue.DISABLE;
            for (var i = 1; i <= 12; i++) {
                var flag = constants.defaultUserSettingValue.DISABLE;
                for (var k = 0; k < dashboardLen; k++) {
                    if (i == resultFromDashboard[k].MONTH) {
                        graphArray.push(resultFromDashboard[k].COUNT);
                        sum += resultFromDashboard[k].total;
                        flag = constants.defaultUserSettingValue.ENABLE;
                        break;
                    }
                }
                if (flag == constants.defaultUserSettingValue.DISABLE) {
                    graphArray.push(constants.defaultUserSettingValue.DISABLE);
                }
            }
            var response = {"graph": graphArray};
            res.jsonp(response);
        });
    }
    else if (type == constants.userProfileType.DRIVER) {
        var sql = "SELECT  COUNT(*) AS total , MONTH(`register_timestamp`) MONTH, COUNT(*) COUNT FROM user WHERE YEAR(`register_timestamp`)= YEAR(CURDATE())";
        sql += " && `is_deleted`=? && `profile`= ? GROUP BY MONTH(`register_timestamp`)";
        connection.query(sql, [constants.userDeleteStatus.NOT_DELETED, constants.userProfileType.DRIVER], function (err, resultFromDashboard) {
            var dashboardLen = resultFromDashboard.length;
            var graphArray = new Array();
            var sum = constants.defaultUserSettingValue.DISABLE;
            for (var i = 1; i <= 12; i++) {
                var flag = constants.defaultUserSettingValue.DISABLE;
                for (var k = 0; k < dashboardLen; k++) {
                    if (i == resultFromDashboard[k].MONTH) {
                        graphArray.push(resultFromDashboard[k].COUNT);
                        sum += resultFromDashboard[k].total;
                        flag = constants.defaultUserSettingValue.ENABLE;
                        break;
                    }
                }
                if (flag == constants.defaultUserSettingValue.DISABLE) {
                    graphArray.push(constants.defaultUserSettingValue.DISABLE);
                }
            }
            var response = {"graph": graphArray};
            res.jsonp(response);
        });
    }
    else {
        var sql = "SELECT  COUNT(*) AS total , MONTH(`register_timestamp`) MONTH, COUNT(*) COUNT FROM user WHERE YEAR(`register_timestamp`)= YEAR(CURDATE()) &&";
        sql += " `is_deleted`=? GROUP BY MONTH(`register_timestamp`)"
        connection.query(sql, [constants.userDeleteStatus.NOT_DELETED], function (err, resultFromDashboard) {
            var dashboardLen = resultFromDashboard.length;
            var graphArray = new Array();
            var sum = constants.defaultUserSettingValue.DISABLE;
            for (var i = 1; i <= 12; i++) {
                var flag = constants.defaultUserSettingValue.DISABLE;
                for (var k = 0; k < dashboardLen; k++) {
                    if (i == resultFromDashboard[k].MONTH) {
                        graphArray.push(resultFromDashboard[k].COUNT);
                        sum += resultFromDashboard[k].total;
                        flag = constants.defaultUserSettingValue.ENABLE;
                        break;
                    }
                }
                if (flag == constants.defaultUserSettingValue.DISABLE) {
                    graphArray.push(constants.defaultUserSettingValue.DISABLE);
                }
            }
            var sql = "SELECT COUNT(t.id) as trip FROM trip t"
            connection.query(sql, [], function (err, resultFromTrip) {
                if (resultFromTrip.length == 0) {
                    var tripCount = 0;
                }
                else {
                    tripCount = resultFromTrip[0].trip
                }
                var sql = "SELECT COUNT(m.id) as matches FROM mutual_hytched_user m"
                connection.query(sql, [], function (err, resultFromMatched) {
                    if (resultFromMatched.length == 0) {
                        var matches = 0;
                    }
                    else {
                        matches = resultFromMatched[0].matches
                    }
                    var sql = "SELECT ROUND(SUM(p.amount),2) as total_payment FROM payment p"
                    connection.query(sql, [], function (err, resultFromPayments) {
                        if (resultFromPayments.length == 0) {
                            var payment = 0;
                        }
                        else {
                            payment = resultFromPayments[0].total_payment/100;
                        }
                        var response = {"graph": graphArray, "user": sum, "trip": resultFromTrip[0].trip, "matches": resultFromMatched[0].matches,
                            "total_payment": payment};
                        res.jsonp(response);
                    })
                });
            });
        });
    }
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get data for all user
 * Input : access token, type
 * Output :user's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getDataForAllUsers = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var accessToken = req.body.accesstoken;
    var type = parseInt(req.body.type);
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["user_type"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getDataForAllUsersResult(resultsFromAccessToken, type, res, req);
        });
    });
};
function getDataForAllUsersResult(resultsFromAccessToken, type, res, req) {
    if (type == constants.userProfileType.DRIVER) {
        var profile = constants.userProfileType.DRIVER;
    }
    else if (type === constants.userProfileType.RIDER) {
        var profile = constants.userProfileType.RIDER;
    }
    else {
        var profile = constants.userProfileType.RIDER + ',' + constants.userProfileType.DRIVER + ',' + constants.userProfileType.BOTH;
    }
    var sql = "SELECT `user_id`,`first_name`,`last_name`,`email`,`age`,`gender`,CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image) END as image,";
    sql += "`total_hytches`,`total_dytches`,`total_matches`,`profile` FROM `user` WHERE `is_deleted`=?  && profile IN(" + profile + ")";
    connection.query(sql, [0], function (err, resultFromUsers) {
        var totalUsers = resultFromUsers.length;
        if (totalUsers > 0) {
            for (var i = 0; i < totalUsers; i++) {
                resultFromUsers[i].gender = resultFromUsers[i].gender.charAt(0).toUpperCase() + resultFromUsers[i].gender.slice(1);
                if (resultFromUsers[i].profile == constants.userProfileType.DRIVER) {
                    resultFromUsers[i].profile = 'Driver';
                }
                else if (resultFromUsers[i].profile == constants.userProfileType.RIDER) {
                    resultFromUsers[i].profile = 'Rider';
                }
                else {
                    resultFromUsers[i].profile = 'Both';
                }
            }
        }
        var response = {"data": resultFromUsers};
        res.jsonp(response);
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get data for user
 * Input : user_id
 * Output :user's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getDataForUserThroughUserId = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var userId = req.body.userid;
    console.log(req.body);
    var manValues = [userId];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["user_type"];
        getDataForUserThroughUserIdResult(userId, res, req);
    });
};
function getDataForUserThroughUserIdResult(userId, res, req) {
    var sql = "SELECT `user_id`,`first_name`,`last_name`,`email`,`age`,`gender`,`image`,`total_hytches`,`total_dytches`,";
    sql += "`total_matches`,`about`,DATE_FORMAT(register_timestamp,'%b %d %Y %H:%i')as register_timestamp,`total_transactions`,(`payment_received`/100) as payment_received,";
    sql += "`work`,(`payment_made`/100) as payment_made  FROM `user` WHERE `user_id`=? && `is_deleted`=? LIMIT 1 ";
    connection.query(sql, [userId, constants.userDeleteStatus.NOT_DELETED], function (err, resultFromUsers) {
        console.log(err);
        console.log(resultFromUsers);
        if (resultFromUsers.length > 0) {
            resultFromUsers[0].gender = resultFromUsers[0].gender.charAt(0).toUpperCase() + resultFromUsers[0].gender.slice(1);
            var sql = "SELECT `start_trip_string`,`end_trip_string` FROM `trip` WHERE `user_id`=?  ORDER BY id LIMIT 1"
            connection.query(sql, [userId], function (err, resultFromTrips) {
                var sql = "SELECT `name`,`image` FROM `user_interest` WHERE `user_id`=? "
                connection.query(sql, [userId], function (err, resultFromInterest) {
                    var sql = "SELECT CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image)  END as image FROM `user_image` WHERE `user_id`=? "
                    connection.query(sql, [userId], function (err, resultFromImages) {
                        var sql = "SELECT `image`,`fb_name` FROM `fb_user` WHERE `user_id`=? "
                        connection.query(sql, [userId], function (err, resultFromFacebook) {
                            var sql = "SELECT  DISTINCT `image`,`fb_name` FROM `fb_invitation` WHERE `user_id`=? "
                            connection.query(sql, [userId], function (err, resultFromInvitation) {
                                var response = {"details": resultFromUsers, "interest": resultFromInterest, "user_images": resultFromImages, "fb_friends": resultFromFacebook, "fb_invites": resultFromInvitation, "user_trips": resultFromTrips};
                                res.jsonp(response);
                            });
                        });
                    });
                });
            });
        }
        else {
            var response = constants.responseErrors.USER_DOES_NOT_EXIST;
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get data for all user in pagination for card view
 * Input : access token, offset, type, profile
 * Output :user's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getDataForAllUsersInPagination = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var accessToken = req.body.accesstoken;
    var offset = req.body.offset;
    var type = req.body.type;
    var profile = req.body.profile;
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["user_type"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getDataForAllUsersInPaginationResult(resultsFromAccessToken, offset, type, profile, res, req);
        });
    });
};
function getDataForAllUsersInPaginationResult(resultsFromAccessToken, offset, type, profile, res, req) {
    var sqlTotal = "SELECT `user_id` FROM `user` WHERE `is_deleted`=?  "
    if (type == 0) {
        if (profile == 0) {
            var sql = "SELECT `user_id`,`register_timestamp` FROM `user` WHERE `is_deleted`=? ORDER BY `register_timestamp` DESC LIMIT " + offset + ""
            var sqlTotal = "SELECT `user_id` FROM `user` WHERE `is_deleted`=? "
        }
        else if (profile === constants.userProfileType.DRIVER) {
            var sql = "SELECT `user_id`,`register_timestamp` FROM `user` WHERE `is_deleted`=? && `profile` = " + constants.userProfileType.DRIVER + " ORDER BY `register_timestamp` DESC LIMIT " + offset + ""
            var sqlTotal = "SELECT `user_id` FROM `user` WHERE `is_deleted`=? && `profile`=" + constants.userProfileType.DRIVER + "";
        }
        else if (profile == constants.userProfileType.RIDER) {
            var sql = "SELECT `user_id`,`register_timestamp` FROM `user` WHERE `is_deleted`=? && `profile` = " + constants.userProfileType.RIDER + " ORDER BY `register_timestamp` DESC LIMIT " + offset + ""
            var sqlTotal = "SELECT `user_id` FROM `user` WHERE `is_deleted`=? && `profile`=" + constants.userProfileType.RIDER + "";
        }
        connection.query(sql, [0], function (err, resultFromUsers) {
            var totalUsers = resultFromUsers.length;
            if (totalUsers > 0) {
                var lastUser = totalUsers - 1;
                var lastTimestamp = resultFromUsers[lastUser].register_timestamp;
                if (profile == 0) {
                    var opponentProfile = constants.userProfileType.RIDER + ',' + constants.userProfileType.DRIVER + ',' + constants.userProfileType.BOTH;
                }
                else if (profile == constants.userProfileType.RIDER) {
                    var opponentProfile = constants.userProfileType.RIDER;
                }
                else if (profile == constants.userProfileType.DRIVER) {
                    var opponentProfile = constants.userProfileType.DRIVER;
                }
                var sql = "SELECT `user_id`,`first_name`,`last_name`,`email`,`age`,`gender`,CASE WHEN image LIKE 'http%' THEN image";
                sql += " ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image) END as image,`total_hytches`,`total_dytches`,`total_matches`,`profile`,";
                sql += "`register_timestamp` FROM `user` WHERE `profile` IN(" + opponentProfile + ")  && `is_deleted`=? && `register_timestamp` < ?  ORDER BY `register_timestamp` DESC LIMIT 6"
                connection.query(sql, [constants.userDeleteStatus.NOT_DELETED, lastTimestamp], function (err, resultFromUsersSorting) {
                    var totalUsersSorted = resultFromUsersSorting.length;
                    if (totalUsersSorted > 0) {
                        for (var i = 0; i < totalUsersSorted; i++) {
                            if (resultFromUsersSorting[i].profile == 1) {
                                resultFromUsersSorting[i].profile = 'Driver';
                            }
                            else if (resultFromUsersSorting[i].profile == 2) {
                                resultFromUsersSorting[i].profile = 'Rider';
                            }
                            else {
                                resultFromUsersSorting[i].profile = 'Driver, Rider';
                            }
                        }
                        connection.query(sqlTotal, [0], function (err, resultUsersLength) {
                            var response = {"data": resultFromUsersSorting, "total": Math.ceil(resultUsersLength.length / 6)};
                            res.jsonp(response);
                        });
                    }
                });
            }
        });
    }
    else {
        if (profile == 0) {
            var opponentProfile = constants.userProfileType.RIDER + ',' + constants.userProfileType.DRIVER + ',' + constants.userProfileType.BOTH;
        }
        else if (profile == constants.userProfileType.RIDER) {
            var opponentProfile = constants.userProfileType.RIDER;
        }
        else if (profile == constants.userProfileType.DRIVER) {
            var opponentProfile = constants.userProfileType.DRIVER;
        }
        var sql = "SELECT `user_id`,`first_name`,`last_name`,`email`,`age`,`gender`,CASE WHEN image LIKE 'http%' THEN image";
        sql += " ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image) END as image,`total_hytches`,`total_dytches`,`total_matches`,`profile`,";
        sql += "`register_timestamp` FROM `user` WHERE `profile` IN(" + opponentProfile + ")  && `is_deleted`=?  ORDER BY `register_timestamp` DESC LIMIT 6"
        connection.query(sql, [constants.userDeleteStatus.NOT_DELETED], function (err, resultFromUsersSorting) {
            var totalUsersSorted = resultFromUsersSorting.length;
            if (totalUsersSorted > 0) {
                for (var i = 0; i < totalUsersSorted; i++) {
                    if (resultFromUsersSorting[i].profile == constants.userProfileType.DRIVER) {
                        resultFromUsersSorting[i].profile = 'Driver';
                    }
                    else if (resultFromUsersSorting[i].profile == constants.userProfileType.RIDER) {
                        resultFromUsersSorting[i].profile = 'Rider';
                    }
                    else {
                        resultFromUsersSorting[i].profile = 'Driver, Rider';
                    }
                }
                connection.query(sqlTotal, [0], function (err, resultUsersLength) {
                    var response = {"data": resultFromUsersSorting, "total": Math.ceil(resultUsersLength.length / 6)};
                    res.jsonp(response);
                });
            }
        });
    }
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get Latest feed of user
 * Input : user_id
 * Output :user's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getLatestFeedOfUserThroughUserId = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var userId = req.body.userid;
    console.log(req.body);
    var manValues = [userId];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["first_name"];
        getLatestFeedOfUserThroughUserIdResult(userId, res, req);
    });
};
function getLatestFeedOfUserThroughUserIdResult(userId, res, req) {
    var sql = "SELECT `from_id`,to_id,`from_name`,`to_name`,(`amount`/100),`created_at` FROM `payment` WHERE from_id = ? ORDER BY `id` DESC LIMIT 1 "
    connection.query(sql, [userId], function (err, resultFromPaymentMade) {
        var sql = "SELECT `from_id`,to_id,`from_name`,`to_name`,(`amount`/100),`created_at` FROM `payment` WHERE to_id = ? ORDER BY `id` DESC LIMIT 1  "
        connection.query(sql, [userId], function (err, resultFromPaymentReceived) {
            var response = {"payment_made": resultFromPaymentMade, "payment_received": resultFromPaymentReceived};
            res.jsonp(response);
        });
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Admin login
 * Input : email, password
 * Output :user's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getLoginForAdmin = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var email = req.body.email;
    var password = req.body.password;
    var manValues = [email, password];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        getLoginForAdminResult(email, password, res, req);
    });
};
function getLoginForAdminResult(email, password, res, req) {
    console.log(email);
    var userPassword = md5(password);
    console.log(userPassword);
    var sql = "SELECT `user_id`,`access_token` FROM `user` WHERE `email`=? && `password`=? && `is_admin`=? LIMIT 1 "
    connection.query(sql, [email, userPassword, constants.userProfileType.ADMIN], function (err, resultFromUsers) {
        if (resultFromUsers.length > 0) {
            var response = {"user_id": resultFromUsers[0].user_id, 'access_token': resultFromUsers[0].access_token}
            res.jsonp(response);
        }
        else {
            var response = constants.responseErrors.INCORRECT_USER_NAME_OR_PASSWORD
            res.jsonp(response);
        }
    });
}
/* ------------------------------------------------------
 * Update user password from access token
 * Input: old password, new password
 * Output: log or error message
 * ------------------------------------------------------
 */
exports.updateUserPasswordFromAccessToken = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var accessToken = req.body.accesstoken;
    var oldPassword = req.body.oldpassword;
    var newPassword = req.body.newpassword;
    oldPassword = md5(oldPassword);
    newPassword = md5(newPassword);
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["password"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromToken) {
            var userId = resultsFromToken[0].user_id;
            var dbPassword = resultsFromToken[0].password;
            var count = dbPassword.localeCompare(oldPassword);
            if (count == 0) {
                var sql1 = "UPDATE `user` SET `password`=? WHERE `user_id`=? LIMIT 1";
                connection.query(sql1, [newPassword, userId], function (err, updateUser) {
                    if (err) {
                        var response = constants.responseErrors.UPDATION_ERROR;
                        res.jsonp(response);
                    }
                    else {
                        var response = constants.responseSuccess.PASSWORD_CHANGE_SUCCESSFULLY;
                        res.jsonp(response);
                    }
                });
            }
            else {
                var response = constants.responseErrors.INCORRECT_OLD_PASSWORD;
                res.jsonp(response);
            }
        });
    });
};
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get Latest feed of user
 * Input : user_id
 * Output :graph's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getLatestFeedForDashboardThroughAccessToken = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var accessToken = req.body.accesstoken;
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["first_name"];
        getLatestFeedForDashboardThroughAccessTokenResult(res, req);
    });
};
function getLatestFeedForDashboardThroughAccessTokenResult(res, req) {
    var sql = "SELECT `from_name` as f_name,`to_name` as t_name,(`amount`/100),`from_id`,`to_id`,`created_at` as timestamp FROM `payment` ORDER BY id DESC LIMIT 2 "
    connection.query(sql, function (err, resultFromPaymentMade) {
//        if (resultFromPaymentMade.length > 0) {
//            resultFromPaymentMade[0].key = 1;
//            resultFromPaymentMade[1].key = 1;
//        }
        var sql = "SELECT u.first_name as f_name, t.name as t_name,t.user_id as from_id, t.created_at as timestamp FROM `trip` t INNER JOIN user u on t.user_id= u.user_id  ORDER BY id  DESC LIMIT 2"
        connection.query(sql, function (err, resultFromTrip) {
            resultFromTrip[0].key = 2;
            resultFromTrip[1].key = 2;
            var sql = "SELECT `user_name` as f_name, `hytched_name` as t_name,`user_id` as from_id, `hytched_id` as to_id, `created_at` as timestamp FROM `mutual_hytched_user`  ORDER BY id DESC LIMIT 3"
            connection.query(sql, function (err, resultForMutualHytch) {
                var newMutualResult = [];
                var mutualLen = resultForMutualHytch.length;
                for (var i = 0; i < mutualLen; i++) {
                    if (i != 1) {
                        newMutualResult.push(resultForMutualHytch[i]);
                    }
                }
                newMutualResult[0].key = 3;
                newMutualResult[1].key = 3;
                var sql = "SELECT `first_name` as f_name,`last_name` as t_name,`user_id` as from_id, `register_timestamp` as timestamp FROM `user` ORDER BY user_id DESC LIMIT 2 "
                connection.query(sql, function (err, resultFromUsers) {
                    resultFromUsers[0].key = 4;
                    resultFromUsers[1].key = 4;
                    var mergedArray = new Array();
                    for (var i = 0; i < 2; i++) {
                        if (resultFromPaymentMade[i]) {
                            mergedArray.push(resultFromPaymentMade[i]);
                        }
                        mergedArray.push(resultFromTrip[i]);
                        mergedArray.push(newMutualResult[i]);
                        mergedArray.push(resultFromUsers[i]);
                    }
                    var response = {"data": func.sortByKeyDesc(mergedArray, 'timestamp') };
                    res.jsonp(response);
                });
            });
        });
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get graph data for trips
 * Input : access token, type
 * Output :user's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getGraphForAllTrips = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var accessToken = req.body.accesstoken;
    var type = req.body.type;
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["first_name"];
        getGraphForAllTripsResult(type, res, req);
    });
};
function getGraphForAllTripsResult(type, res, req) {
    if (type == constants.showGraphStatus.DAILY) {
        var sql = "SELECT DATE_FORMAT(created_at,'%b %d %Y')as datatimestamp , COUNT(*) COUNT FROM trip WHERE created_at >NOW() - INTERVAL 1 MONTH GROUP BY DAY(created_at) ORDER BY created_at"
    }
    else if (type == constants.showGraphStatus.MONTHLY) {
        var sql = "SELECT MONTH(`created_at`) MONTH, COUNT(*) COUNT FROM trip WHERE YEAR(`created_at`)= YEAR(CURDATE()) GROUP BY MONTH(`created_at`)"
    }
    else {
        var sql = "SELECT YEAR(`created_at`) AS year, COUNT(*) COUNT FROM trip  GROUP BY YEAR(`created_at`)"
    }
    connection.query(sql, [], function (err, resultFromDashboard) {
        var dashboardLen = resultFromDashboard.length;
        if (dashboardLen > 0) {
            if (type == constants.showGraphStatus.DAILY) {
                var response = {"graph": resultFromDashboard};
                res.jsonp(response);
            }
            else if (type == constants.showGraphStatus.MONTHLY) {
                var graphArray = new Array();
                var sum = constants.defaultUserSettingValue.DISABLE;
                for (var i = 1; i <= 12; i++) {
                    var flag = 0;
                    for (var k = 0; k < dashboardLen; k++) {
                        if (i == resultFromDashboard[k].MONTH) {
                            graphArray.push(resultFromDashboard[k].COUNT);
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 0) {
                        graphArray.push(constants.defaultUserSettingValue.DISABLE);
                    }
                }
                var response = {"graph": graphArray};
                res.jsonp(response);
            }
            else {
                var response = {"graph": resultFromDashboard};
                res.jsonp(response);
            }
        }
        else {
            var response = {"graph": []};
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get data for all trips
 * Input : access token, type
 * Output :trip's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getDataForAllTrips = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var accessToken = req.body.accesstoken;
    var type = req.body.type;
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = [];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getDataForAllTripsResult(resultsFromAccessToken, type, res, req);
        });
    });
};
function getDataForAllTripsResult(resultsFromAccessToken, type, res, req) {
    var sql = "SELECT CONCAT(u.first_name,' ',u.last_name) AS user_name,t.id,t.user_id,t.name,";
    sql += "t.start_trip_string,t.end_trip_string,t.profile,t.distance,DATE_FORMAT(t.created_at,'%b %d %Y %H:%i')as timestamp FROM user u INNER JOIN trip t on u.user_id= t.user_id"
    connection.query(sql, [], function (err, resultFromTrip) {
        var sql = "SELECT COUNT(*) as totaluser FROM `user` WHERE `is_deleted`=?"
        connection.query(sql, [0], function (err, resultFromUsers) {
            var totalTrips = resultFromTrip.length;
            if (totalTrips > 0) {
                var tripCount = constants.defaultUserSettingValue.DISABLE;
                var driver = constants.defaultUserSettingValue.DISABLE;
                var rider = constants.defaultUserSettingValue.DISABLE;
                var totalDistance = constants.defaultUserSettingValue.DISABLE;
                for (var i = 0; i < totalTrips; i++) {
                    ++tripCount;
                    totalDistance += resultFromTrip[i].distance
                    if (resultFromTrip[i].profile == constants.userProfileType.DRIVER) {
                        resultFromTrip[i].profile = 'Driver';
                        ++driver;
                    }
                    else if (resultFromTrip[i].profile == constants.userProfileType.RIDER) {
                        resultFromTrip[i].profile = 'Rider';
                        ++rider;
                    }
                    else {
                        resultFromTrip[i].profile = 'Both';
                    }
                }
                console.log(rider);
                console.log(driver);
                var averageTrips = Math.round((tripCount / resultFromUsers[0].totaluser), 1);
                var averageDistance = Math.round((totalDistance / tripCount), 1)
                if (driver == constants.defaultUserSettingValue.DISABLE) {
                    averageRider = rider;
                }
                else {
                    console.log(rider / driver);
                    var averageRider = Math.round((rider / driver), 1);
                }
                if (rider == constants.defaultUserSettingValue.DISABLE) {
                    averageDriver = driver;
                }
                else {
                    var averageDriver = Math.round((driver / rider), 1);
                }
            }
            else {
                averageTrips = 0
            }
            var response = {"data": resultFromTrip, "average_trips": averageTrips, "average_distance": averageDistance, "average_rider": averageRider, "average_driver": averageDriver };
            res.jsonp(response);
        });
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get trip data through trip id
 * Input : trip id
 * Output :trip's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getTripDataThroughTripId = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var tripid = req.body.tripid;
    console.log(req.body);
    var manValues = [tripid];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = [];
        getTripDataThroughTripIdResult(tripid, res, req);
    });
};
function getTripDataThroughTripIdResult(tripid, res, req) {
    var sql = "SELECT CONCAT(u.first_name,' ',u.last_name) AS user_name, ";
    sql += "CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image)";
    sql += " END as image,t.user_id,t.name,t.start_trip_string,t.end_trip_string,t.profile,t.distance,t.route_string,";
    sql += "DATE_FORMAT(t.created_at,'%b %d %Y')as timestamp FROM user u INNER JOIN trip t on u.user_id= t.user_id WHERE t.id=?";
    connection.query(sql, [tripid], function (err, resultFromTrip) {
        var totalTrips = resultFromTrip.length;
        if (totalTrips > 0) {
            for (var i = 0; i < totalTrips; i++) {
                if (resultFromTrip[i].profile == constants.userProfileType.DRIVER) {
                    resultFromTrip[i].profile = 'Driver';
                }
                else if (resultFromTrip[i].profile == constants.userProfileType.RIDER) {
                    resultFromTrip[i].profile = 'Rider';
                }
                else {
                    resultFromTrip[i].profile = 'Both';
                }
            }
        }
        var sql = "SELECT m.hytched_id, CASE WHEN image LIKE 'http%' THEN image";
        sql += " ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image) END as image,";
        sql += " u.fb_id, CONCAT(u.first_name, '#', u.last_name) AS user_name FROM mutual_hytched_user m INNER JOIN user u";
        sql += " ON m.hytched_id= u.user_id WHERE (m.user_id= ? && m.trip_id = ?) || (m.opponent_trip_id= ? && m.hytched_id = ?)  GROUP BY u.fb_id"
        connection.query(sql, [resultFromTrip[0].user_id, tripid, resultFromTrip[0].user_id, tripid ], function (err, resultFromMutualHytch) {
            var hytchLen = resultFromMutualHytch.length;
            if (hytchLen > 0) {
                var str = resultFromTrip[0].user_id + ',' + resultFromMutualHytch[0].hytched_id;
                console.log(str);
                var sql = "SELECT `fb_id`,`name`,`image` FROM `user_interest` WHERE user_id IN(" + str + ") GROUP BY fb_id HAVING COUNT(*) = 2"
                connection.query(sql, [], function (err, resultFromMutualInterest) {
                    var sql = "SELECT `fb_id`,`fb_name`,`image` FROM `fb_user` WHERE user_id IN(" + str + ") GROUP BY fb_id HAVING COUNT(*) = 2"
                    connection.query(sql, [], function (err, resultFromMutualFriends) {
                        var response = {"trip": resultFromTrip, "mutual_hytch": resultFromMutualHytch, "mutual_interest": resultFromMutualInterest, "mutual_friends": resultFromMutualFriends, "match_count": hytchLen};
                        res.jsonp(response);
                    });
                });
            }
            else {
                var response = {"trip": resultFromTrip, "mutual_hytch": [], "mutual_interest": [], "mutual_friends": [], "match_count": 0};
                res.jsonp(response);
            }
        });
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get mutual interest and friends
 * Input : user_id comma separated of both user and opponent respectively
 * Output :data's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getMutualInterestsAndFriends = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var userIds = req.body.userid;
    console.log(req.body);
    var manValues = [userIds];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = [];
        getMutualInterestsAndFriendsResult(userIds, res, req);
    });
};
function getMutualInterestsAndFriendsResult(userIds, res, req) {
    var sql = "SELECT `fb_id`,`name`, `image` FROM `user_interest` WHERE user_id IN(" + userIds + ") GROUP BY fb_id HAVING COUNT(*) = 2"
    connection.query(sql, [], function (err, resultFromMutualIntrest) {
        console.log(err);
        var sql = "SELECT `fb_id`,`fb_name`,`image` FROM `fb_user` WHERE user_id IN(" + userIds + ") GROUP BY fb_id HAVING COUNT(*) = 2"
        connection.query(sql, [], function (err, resultFromMutualFriends) {
            console.log(err);
            var response = {"mutual_interest": resultFromMutualIntrest, "mutual_friends": resultFromMutualFriends};
            res.jsonp(response);
        });
    });
}
/*
 * -----------------------------------------------------------------------------
 * Sending password through mail to user
 * Input : email id
 * Output : log message
 * -----------------------------------------------------------------------------
 */
exports.forgotPassword = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var email = req.body.email;
    var manValues = [email];
    console.log(req.body);
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        forgotPasswordResult(email, res);
    });
};
function forgotPasswordResult(email, res) {
    var sql = "SELECT `user_id`,`first_name` FROM `user` WHERE email=? && is_admin=? && is_deleted=? LIMIT 1";
    connection.query(sql, [email, constants.userProfileType.ADMIN, constants.userDeleteStatus.NOT_DELETED], function (err, responseFromUser) {
        if (responseFromUser.length > 0) {
            var timestamp = new Date();
            var password = func.generateRandomString();
            var to = email;
            var subject = "[Hytch] Password Reset";
            var message = "<body>" +
                "<p>Hello " + responseFromUser[0].first_name + ",</p>" +
                "<p>We have received a password reset request for your account.</p><p>Your new password is : " + password + " </p>" +
                "<p>You can change your password by logging into your account.</p><p>Thank you.</p>" +
                "<p> Your Hytch Team</p></body>";
            func.sendHtmlContent(to, message, subject, function (responseFromSendMessage) {
                if (!responseFromSendMessage) {
                    var response = constants.responseErrors.UPDATION_ERROR;
                    res.jsonp(response);
                }
                else {
                    var encryptedPassword = md5(password);
                    console.log(password);
                    console.log(encryptedPassword);
                    var sql = "UPDATE `user` SET `password`=? WHERE `email`=? LIMIT 1";
                    connection.query(sql, [encryptedPassword, email], function (err, response) {
                    });
                    var response = constants.responseSuccess.PASSWORD_RESET_SUCCESSFULLY;
                    res.jsonp(response);
                }
            });
        }
        else {
            var response = constants.responseErrors.USER_NOT_REGISTERED;
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get graph  data for payment
 * Input : access token, type
 * Output :graph's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getGraphForAllPayments = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var accessToken = req.body.accesstoken;
    var type = req.body.type;
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["first_name"];
        getGraphForAllPaymentsResult(type, res, req);
    });
};
function getGraphForAllPaymentsResult(type, res, req) {
    if (type == constants.showGraphStatus.DAILY) {
        var sql = "SELECT DATE_FORMAT(created_at,'%b %d %Y')as datatimestamp , SUM(`amount`/100) SUM FROM payment WHERE created_at >NOW() - INTERVAL 1 MONTH GROUP BY DAY(created_at) ORDER BY created_at"
    }
    else if (type == constants.showGraphStatus.MONTHLY) {
        var sql = "SELECT MONTH(`created_at`) MONTH, SUM(`amount`/100) SUM FROM payment WHERE YEAR(`created_at`)= YEAR(CURDATE()) GROUP BY MONTH(`created_at`)"
    }
    else {
        var sql = "SELECT YEAR(`created_at`) AS year, SUM(`amount`/100) SUM FROM payment  GROUP BY YEAR(`created_at`)"
    }
    connection.query(sql, [], function (err, resultFromDashboard) {
        console.log(err);
        var dashboardLen = resultFromDashboard.length;
        if (dashboardLen > 0) {
            if (type == constants.showGraphStatus.DAILY) {
                var response = {"graph": resultFromDashboard};
                res.jsonp(response);
            }
            else if (type == constants.showGraphStatus.MONTHLY) {
                var graphArray = new Array();
                var sum = 0;
                for (var i = 1; i <= 12; i++) {
                    var flag = 0;
                    for (var k = 0; k < dashboardLen; k++) {
                        if (i == resultFromDashboard[k].MONTH) {
                            graphArray.push(resultFromDashboard[k].SUM);
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 0) {
                        graphArray.push(0);
                    }
                }
                var response = {"graph": graphArray};
                res.jsonp(response);
            }
            else {
                var response = {"graph": resultFromDashboard};
                res.jsonp(response);
            }
        }
        else {
            var response = {"graph": []};
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get data for all payment
 * Input : access token
 * Output :payment's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getDataForAllPayments = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var accessToken = req.body.accesstoken;
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = [];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getDataForAllPaymentsResult(resultsFromAccessToken, res, req);
        });
    });
};
function getDataForAllPaymentsResult(resultsFromAccessToken, res, req) {
    var sql = "SELECT p.from_name,p.to_name,(p.amount/100) as amount ,p.status,DATE_FORMAT(p.created_at,'%b %d %Y %H:%i')as timestamp,";
    sql += " p.promocode as promo_code, c.card,c.card_number FROM user_card c INNER JOIN payment p on c.customer_id= p.customer_id"
    connection.query(sql, [], function (err, resultFromPayments) {
        var totalPayments = resultFromPayments.length;
        if (totalPayments > 0) {
            var response = {"data": resultFromPayments};
            res.jsonp(response)
        }
        else {
            var response = {"data": []};
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get user data for maps
 * Input : access token
 * Output :payment's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getUserDataForMaps = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var accessToken = req.body.accesstoken;
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = [];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getUserDataForMapsResult(resultsFromAccessToken, res, req);
        });
    });
};
function getUserDataForMapsResult(resultsFromAccessToken, res, req) {
    var sql = "SELECT `user_id`, CONCAT(`first_name`, ' ',`last_name`) as user_name,CASE WHEN image";
    sql += " LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image) END as image";
    sql += ",`profile`,`current_latitude`,`current_longitude`,`email`,`age` FROM `user` WHERE `is_deleted`=? ";
    connection.query(sql, [constants.userDeleteStatus.NOT_DELETED], function (err, resultFromUsers) {
        var totalUsers = resultFromUsers.length;
        if (totalUsers > 0) {
            for (var i = 0; i < totalUsers; i++) {
                if (resultFromUsers[i].profile == constants.userProfileType.DRIVER) {
                    resultFromUsers[i].profile = 'Driver';
                }
                else if (resultFromUsers[i].profile == constants.userProfileType.RIDER) {
                    resultFromUsers[i].profile = 'Rider';
                }
                else {
                    resultFromUsers[i].profile = 'Both';
                }
            }
            var response = {"data": resultFromUsers};
            res.jsonp(response)
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Send mail to user
 * Input : to, subject, message body
 * Output :payment's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.sendMailToUser = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var to = req.body.to;
    var subject = req.body.subject;
    var messageBody = req.body.messagebody;
    console.log(req.body);
    var manValues = [to, subject, messageBody];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        sendMailToUserResult(to, subject, messageBody, res, req);
    });
};
function sendMailToUserResult(to, subject, messageBody, res, req) {
    func.sendHtmlContent(to, messageBody, subject, function (responseFromSendMessage) {
        if (responseFromSendMessage) {
            var response = constants.responseSuccess.MAIL_SENT_SUCCESSFULLY;
            res.jsonp(response);
        }
        else {
            var response = constants.responseErrors.UPDATION_ERROR;
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get all promo code
 * Input : access token
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getAllPromoCode = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var accessToken = req.body.accesstoken;
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        getAllPromoCodeResult(accessToken, res, req);
    });
};
function getAllPromoCodeResult(accessToken, res, req) {
    var sql = "SELECT `id`,`name`,`location`,`value`, DATE_FORMAT(valid_from,'%Y-%m-%d')as valid_from , DATE_FORMAT(valid_upto,'%Y-%m-%d') as valid_upto FROM `promocode`";
    connection.query(sql, [], function (err, resultsFromPromoCode) {
        if (resultsFromPromoCode.length > 0) {
            var response = {
                "data": resultsFromPromoCode
            };
            res.jsonp(response);
        }
        else {
            var response = {
                "data": []
            };
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get promo code details through promo code id
 * Input : id
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getPromoCodeDetailsThroughId = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var id = req.body.id;
    console.log(req.body);
    var manValues = [id];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        getPromoCodeDetailsThroughIdResult(id, res, req);
    });
};
function getPromoCodeDetailsThroughIdResult(id, res, req) {
    var sql = "SELECT `id`,`name`,`location`,`value`, DATE_FORMAT(valid_from,'%Y-%m-%d')as valid_from,";
    sql += " DATE_FORMAT(valid_upto,'%Y-%m-%d') as valid_upto FROM `promocode` WHERE `id` = ? LIMIT 1";
    connection.query(sql, [id], function (err, resultsFromPromoCode) {
        if (resultsFromPromoCode.length > 0) {
            var response = {
                "data": resultsFromPromoCode
            };
            res.jsonp(response);
        }
        else {
            var response = {
                "data": []
            };
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Update promo code details through promo code id
 * Input : id
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.createOrUpdatePromoCodeDetailsThroughId = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var id = req.body.id;
    var name = req.body.name;
    var value = req.body.value;
    var location = req.body.location;
    var valid_from = req.body.validfrom;
    var valid_upto = req.body.validupto;
    var type = req.body.type;
    console.log(req.body);
    var manValues = [name, value, valid_from, valid_upto];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        createOrUpdatePromoCodeDetailsThroughIdResult(id, name, value, location, valid_from, valid_upto, type, res, req);
    });
};
function createOrUpdatePromoCodeDetailsThroughIdResult(id, name, value, location, valid_from, valid_upto, type, res, req) {
    if (type == 0) {
        var sql = "UPDATE `promocode` SET `name`=?,`location`=?, `value`=?,`valid_from`=?, `valid_upto`=?  WHERE `id`=? LIMIT 1";
        connection.query(sql, [name, location, value, valid_from, valid_upto, id], function (err, resultsFromPromoCode) {
            if (!err) {
                var response = constants.responseSuccess.PROMO_CODE_UPDATED_SUCCESSFULLY;
                res.jsonp(response);
            }
            else {
                var response = constants.responseErrors.UPDATION_ERROR;
                res.jsonp(response);
            }
        });
    }
    else {
        console.log('HELLL')
        var timestamp = new Date();
        var sql = "INSERT INTO `promocode`(`name`,`location`,`value`,`valid_from`,`valid_upto`,`created_at`) VALUES (?,?,?,?,?,?)";
        connection.query(sql, [name, location, value, valid_from, valid_upto, timestamp], function (err, resultsFromPromoCode) {
            console.log(err);
            if (!err) {
                var response = constants.responseSuccess.PROMO_CODE_CREATED_SUCCESSFULLY;
                res.jsonp(response);
            }
            else {
                console.log(err);
                var response = constants.responseErrors.UPDATION_ERROR;
                res.jsonp(response);
            }
        });
    }
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get promo code details through promo code id
 * Input : id
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.deletePromoCodeDetailsThroughId = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var id = req.body.id;
    console.log(req.body);
    var manValues = [id];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        deletePromoCodeDetailsThroughIdResult(id, res, req);
    });
};
function deletePromoCodeDetailsThroughIdResult(id, res, req) {
    var sql = "DELETE FROM `promocode` WHERE `id` = ? LIMIT 1";
    connection.query(sql, [id], function (err, responseFromPromoCode) {
        if (!err) {
            var response = constants.responseSuccess.PROMO_CODE_UPDATED_SUCCESSFULLY;
            res.jsonp(response);
        }
        else {
            var response = constants.responseErrors.UPDATION_ERROR;
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get  all wait listed user
 * Input : access token
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getAllWaitListedUserThroughAccessToken = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var id = req.body.id;
    console.log(req.body);
    var accessToken = req.body.access_token
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["user_type"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getAllWaitListedUserThroughAccessTokenResult(resultsFromAccessToken, res, req);
        });
    });
};
function getAllWaitListedUserThroughAccessTokenResult(resultsFromAccessToken, res, req) {
    var sql = "SELECT `id`,`name`,`email`,`phone`,`wait_code`,DATE_FORMAT(created_at,'%Y-%m-%d')as timestamp FROM `wait_list_user` WHERE `wait_status`=? ORDER BY `id`";
    connection.query(sql, [constants.defaultUserSettingValue.DISABLE], function (err, responseFromWaitUser) {
        if (err) {
            var response = constants.responseSuccess.PROMO_CODE_UPDATED_SUCCESSFULLY;
            res.jsonp(response);
        }
        else {
            var recordsLength = responseFromWaitUser.length;
            if(recordsLength > 0){
            var waitListArray = [];
            console.log(responseFromWaitUser);
            var check = 0;
            for (var i = 0; i < recordsLength; i++) {
                (function (i) {
                    var sql = "SELECT COUNT(*) as front_user FROM `wait_list_user` WHERE `id` <= ? AND `wait_status` = ?";
                    connection.query(sql, [responseFromWaitUser[i].id, constants.defaultUserSettingValue.DISABLE], function (err, responseFromWaitList) {
                        if (responseFromWaitUser[i].phone == '') {
                            var mode = 'Application';
                        }
                        else {
                            mode = 'Web';
                        }
                        waitListArray.push({id: responseFromWaitList[0].front_user, name: responseFromWaitUser[i].name, email: responseFromWaitUser[i].email, phone: responseFromWaitUser[i].phone,
                            wait_code: responseFromWaitUser[i].wait_code, registration_mode: mode, registration_date: responseFromWaitUser[i].timestamp})
                        ++check;
                        if (check == recordsLength) {
                            var response = {data: waitListArray}
                            res.jsonp(response)
                        }
                    });
                })(i);
            }
            }
            else{
                var response = {data: []}
                res.jsonp(response)
            }
        }
    });
}
exports.addOrEditCreditCard = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    console.log(req.body);
    var accessToken = req.body.access_token;
    var stripeToken = req.body.stripe_token;
    var type = req.body.type;
    var updateId = req.body.update_id;
    var manValues = [accessToken, stripeToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["user_type"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            addOrEditCreditCardResult(resultsFromAccessToken, stripeToken, type, updateId, res);
        });
    });
}
function addOrEditCreditCardResult(resultsFromAccessToken, stripeToken, type, updateId, res) {
    var stripe = require("stripe")(config.get('stripeSettings.key'));
    stripe.customers.create({
        card: stripeToken

    }, function (err, customer) {
        if (err) {
            var response = constants.responseErrors.STRIPE_TOKEN_ERROR;
            res.jsonp(response);
        }
        else {
            console.log(customer);
            var id = customer.id;
            var last4 = customer['sources']['data'][0].last4
            console.log(customer['sources']['data'][0].card)
            if (type == constants.userPaymentCardStatus.ADD_NEW_CARD) {
                var sql = "INSERT INTO admin_card(`customer_id`,`card_number`,`card`,`default`) values(?,?,?,?)"
                connection.query(sql, [id, last4, 'visa', constants.defaultUserSettingValue.ENABLE], function (err, resultsFromCards) {
                    if (err) {
                        var response = constants.responseErrors.UPDATION_ERROR;
                        res.jsonp(response);
                    }
                    else {
                        var response = constants.responseSuccess.CARD_UPDATED_SUCCESSFULLY;
                        res.jsonp(response);
                    }
                });
            }
            else {
                var sql = "UPDATE `admin_card` SET `customer_id`=?,`card_number`=?, `card`=?,`default`=? WHERE `id`=? LIMIT 1";
                connection.query(sql, [id, last4, 'visa', constants.defaultUserSettingValue.ENABLE, updateId], function (err, resultsFromCards) {
                    console.log(err);
                    if (!err) {
                        var response = constants.responseSuccess.CARD_UPDATED_SUCCESSFULLY;
                        res.jsonp(response);
                    }
                    else {
                        var response = constants.responseErrors.UPDATION_ERROR;
                        res.jsonp(response);
                    }
                });
            }
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get Admin card details
 * Input : access token
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getAdminCardDetails = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var id = req.body.id;
    console.log(req.body);
    var accessToken = req.body.access_token;
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["user_type"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getAdminCardDetailsResult(resultsFromAccessToken, res);
        });
    });
};
function getAdminCardDetailsResult(resultsFromAccessToken, res) {
    var sql = "SELECT * FROM `admin_card` WHERE `default` = ? LIMIT 1";
    connection.query(sql, [constants.defaultUserSettingValue.ENABLE], function (err, responseFromAdminCard) {
        if (!err) {
            var response = {data: responseFromAdminCard}
            res.jsonp(response);
        }
        else {
            var response = constants.responseErrors.UPDATION_ERROR;
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Clear user wait list
 * Input : access token
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.clearUserWaitList = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    console.log(req.body);
    var accessToken = req.body.access_token;
    var requestedParams = req.body
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["user_type"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            clearUserWaitListResult(resultsFromAccessToken, requestedParams, res);
        });
    });
};
function clearUserWaitListResult(resultsFromAccessToken, requestedParams, res) {
    if (requestedParams.type == constants.waitListClearStatus.CLEARED_OFFSET) {
        var startIndex = requestedParams.start_index - 1;
        var limit = requestedParams.limit;
        var sql = "SELECT id FROM `wait_list_user` WHERE `wait_status`= ? LIMIT " + startIndex + "," + limit + "";
        connection.query(sql, [constants.defaultUserSettingValue.DISABLE], function (err, resultsFromWaitList) {
            if (err) {
                var response = constants.responseErrors.UPDATION_ERROR;
                res.jsonp(response);
            }
            else {
                if (resultsFromWaitList.length == 0) {
                    var response = constants.responseErrors.UPDATION_ERROR;
                    res.jsonp(response);
                }
                else if (resultsFromWaitList.length != limit) {
                    var response = constants.responseErrors.UPDATION_ERROR;
                    res.jsonp(response);
                }
                else {
                    var sql = "UPDATE wait_list_user SET `wait_status`= ?, `wait_code`=? WHERE id IN  ( SELECT id FROM (SELECT id FROM wait_list_user WHERE `wait_status`= ? LIMIT " + startIndex + "," + limit + ")temp);";
                    console.log(sql);
                    connection.query(sql, [constants.defaultUserSettingValue.ENABLE, '',constants.defaultUserSettingValue.DISABLE], function (err, resultsFromWaitList) {
                        console.log(err);
                        if (!err) {
                            var response = constants.responseSuccess.WAIT_LIST_CLEARED_SUCCESSFULLY;
                            res.jsonp(response);
                        }
                        else {
                            var response = constants.responseErrors.UPDATION_ERROR;
                            res.jsonp(response);
                        }
                    });
                }
            }
        });
    }
    else if (requestedParams.type == constants.waitListClearStatus.CLEARED_ALL) {
        var sql = "UPDATE `wait_list_user` SET `wait_status`=?, `wait_code`=? WHERE `wait_status`=? ";
        connection.query(sql, [constants.defaultUserSettingValue.ENABLE,'', constants.defaultUserSettingValue.DISABLE], function (err, resultsFromWaitList) {
            console.log(err);
            if (!err) {
                var response = constants.responseSuccess.WAIT_LIST_CLEARED_SUCCESSFULLY;
                res.jsonp(response);
            }
            else {
                var response = constants.responseErrors.UPDATION_ERROR;
                res.jsonp(response);
            }
        });
    }
}


/*
 * -----------------------------------------------------------------------------
 * Get all ride history
 * Input : access token
 * Output :
 * -----------------------------------------------------------------------------
 */
exports.getAllRideHistoryThroughAccessToken = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    console.log(req.body);
    var accessToken = req.body.access_token;
    var requestedParams = req.body
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["user_type"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getAllRideHistoryThroughAccessTokenResult(resultsFromAccessToken, requestedParams, res);
        });
    });
};
function getAllRideHistoryThroughAccessTokenResult(resultsFromAccessToken, requestedParams, res) {
    var sql = "SELECT r.id,r.rider_id,r.driver_id,r.ride_start_location_string,r.ride_end_location_string,(r.amount/100) as amount,r.distance,r.rider_rating,r.driver_rating,";
    sql += " DATE_FORMAT(r.ride_start_date_time,'%b %d %Y %H:%i')as ride_start_date_time , DATE_FORMAT(r.ride_end_date_time,'%b %d %Y %H:%i')as ride_end_date_time ,";
        sql += " CONCAT(u.first_name ,' ', u.last_name) as rider_name, r.is_ride_completed_by_rider, r.is_ride_completed_by_driver, r.ride_running_status FROM user_ride r INNER JOIN user u ON r.rider_id = u.user_id   ORDER BY `ride_end_date_time` DESC";
    connection.query(sql, [resultsFromAccessToken[0].user_id, resultsFromAccessToken[0].user_id,
        constants.rideCompletedByDriverOrRider.COMPLETED, constants.rideCompletedByDriverOrRider.COMPLETED],
        function (err, resultsFromUserRide) {
            if (err) {
                var response = constants.responseErrors.UPDATION_ERROR;
                return callback(response, null);
            }
            else {
                var rideIdArray = new Array();
                var opponentIdArray = new Array();
                var profileArray = new Array();
                var opponentRating = new Array();
                var tripHistory = [];
                var totalRides = resultsFromUserRide.length;
                if (totalRides > 0) {
                    for (var i = 0; i < totalRides; i++) {
                        rideIdArray.push(resultsFromUserRide[i].id);
                            opponentIdArray.push(resultsFromUserRide[i].driver_id);
                    }
                    var rideIdString = rideIdArray.toString(',');
                    var opponentIdString = opponentIdArray.toString(',');
                    var asyncResponse = [];
                    async.parallel([
                        function (callback) {
                            var sql = "SELECT c.card_number,c.card, p.ride_id, p.promocode FROM user_card c INNER JOIN payment p";
                            sql += "  ON p.customer_id = c.customer_id WHERE ride_id IN (" + rideIdString + ") ORDER BY FIELD(`ride_id`," + rideIdString + ")";
                            connection.query(sql, [],
                                function (err, responseFromCard) {
                                    console.log(err);
                                    if (err) return callback(err);
                                    asyncResponse.card = responseFromCard;
                                    callback();
                                });
                        },
                        function (callback) {
                            var sql = "SELECT `user_id`,`first_name`,`last_name`,CASE WHEN image LIKE 'http%' THEN image ELSE ";
                            sql += " CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image) END as image FROM `user`";
                            sql += "  WHERE user_id IN(" + opponentIdString + ") ORDER BY FIELD(`user_id`," + opponentIdString + ")";
                            connection.query(sql, [],
                                function (err, resultFromUser) {
                                    console.log(err);
                                    if (err) return callback(err);
                                    asyncResponse.user = resultFromUser;
                                    callback();
                                });
                        }], function (err, callbackResponse) {
                        if (err) {
                            console.log(err)
                            var response = constants.responseErrors.UPDATION_ERROR;
                            res.jsonp(response);
                        }
                        else {
                            var cardLength = asyncResponse.card.length;
                            var opponentDetailsLength = asyncResponse.user.length;
                            var opponentUserLength = asyncResponse.user.length;
                            for (var i = 0; i < totalRides; i++) {
                                var isCardSet = 0;
                                for (var j = 0; j < cardLength; j++) {
                                    if (rideIdArray[i] == asyncResponse.card[j].ride_id) {
                                        var cardNumber = asyncResponse.card[j].card_number;
                                        var card = asyncResponse.card[j].card;
                                        isCardSet = 1;
                                        break;
                                    }
                                    var promoCode = asyncResponse.card[j].promocode;
                                }
                                if (isCardSet == 0) {
                                    var cardNumber = '';
                                    var card = '';
                                }

                                for (var k = 0; k < opponentDetailsLength; k++) {
                                    if (opponentIdArray[i] == asyncResponse.user[k].user_id) {
                                        var userImage = asyncResponse.user[k].image;
                                        var driverName = asyncResponse.user[k].first_name + ' ' + asyncResponse.user[k].last_name;
                                    }
                                }
                                tripHistory.push({"ride_start_location_string": resultsFromUserRide[i].ride_start_location_string,
                                    "ride_end_location_string": resultsFromUserRide[i].ride_end_location_string, "amount": resultsFromUserRide[i].amount,
                                    distance: resultsFromUserRide[i].distance, "card_number": cardNumber, card: card, "driver_name": driverName,rider_name: resultsFromUserRide[i].rider_name,
                                    user_image: userImage, opponent_rating: opponentRating[i], profile: profileArray[i],
                                    "ride_start_date_time": resultsFromUserRide[i].ride_start_date_time, "ride_end_date_time": resultsFromUserRide[i].ride_end_date_time,
                                    "is_ride_completed_by_rider": resultsFromUserRide[i].is_ride_completed_by_rider, "is_ride_completed_by_driver": resultsFromUserRide[i].is_ride_completed_by_driver,
                                    "ride_running_status": resultsFromUserRide[i].ride_running_status,"promo_code": promoCode
                                })
                            }
                            var response = {data: tripHistory}
                           res.jsonp(response);
                        }
                    });
                }
                else {
                    var response = {data: tripHistory}
                    res.jsonp(response);
                }
            }
        });
}