var func = require('./functions');
var request = require('request');
var constants = require('./constants');
var async = require('async');
var client = require('twilio')(config.get('twilioSettings.accountSid'), config.get('twilioSettings.authToken'));
var md5 = require('MD5');
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Register or Login through facebook
 * Input : first name, last name, email, password, device token,login
 * Output :user's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.registerOrLoginThroughFacebook = function (req, res) {
    var fbId = req.body.fbid;
    var fbAccessToken = req.body.fbaccesstoken;
    var firstName = req.body.firstname;
    var lastName = req.body.lastname;
    var email = req.body.email;
    var gender = req.body.gender;
    var age = req.body.age;
    var deviceToken = req.body.devicetoken;
    var userType = parseInt(req.body.usertype);
    var work = req.body.work;
    var loginType = parseInt(req.body.login_type);
    var waitCode = req.body.wait_code;
    console.log(req.body);
    var manValues = [fbId, fbAccessToken, firstName, lastName, email];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        registerOrLoginThroughFacebookResult(fbId, firstName, lastName, email, gender, age, deviceToken, userType, fbAccessToken, work, loginType, waitCode, res, req);
    });
};
function registerOrLoginThroughFacebookResult(fbId, firstName, lastName, email, gender, age, deviceToken, userType, fbAccessToken, work, loginType, waitCode, res, req) {
    if (loginType === constants.userSignUp.VIA_FACEBOOK) {
        authenticateFbUser(fbId, fbAccessToken, firstName, lastName, email, gender, age, work, res, function (resultsFromAuthentication) {
            if (resultsFromAuthentication.length > 0) {
                fbId = resultsFromAuthentication[0].fb_id;
                var userId = resultsFromAuthentication[0].user_id;
                var accessToken = resultsFromAuthentication[0].access_token;
                firstName = resultsFromAuthentication[0].first_name;
                lastName = resultsFromAuthentication[0].last_name;
                email = resultsFromAuthentication[0].email;
                var image = resultsFromAuthentication[0].image;
                var defaultTrip = resultsFromAuthentication[0].default_trip;
                var newMatches = resultsFromAuthentication[0].new_matches;
                var messages = resultsFromAuthentication[0].messages;
                var locationSharing = resultsFromAuthentication[0].location_sharing;
                var pushNotification = resultsFromAuthentication[0].push_notifications;
                var profileRating = resultsFromAuthentication[0].profile_rating;
                var defaultTripName = resultsFromAuthentication[0].default_trip_name;
                var userName = firstName + ' ' + lastName;
                updateUserParams(deviceToken, userId, resultsFromAuthentication[0].user_type);
                if (defaultTrip > 0) {
                    var flag = constants.defaultValueSet.AFTER_SIGN_UP_TO_ZERO;
                }
                else {
                    flag = constants.defaultValueSet.AT_SIGN_UP_TO_ONE;
                }
                //
                var userExtraInformation = []
                async.parallel([
                    function (callback) {
                        getUserMissedPushData(userId, function (err, responseFromMissedPush) {
                            var response = constants.responseErrors.UPDATION_ERROR;
                            if (err) return callback(response);
                            userExtraInformation.push_data = responseFromMissedPush;
                            callback();
                        });
                    },
                    function (callback) {
                        getUserCurrentRideData(userId, function (err, resultsFromRide) {
                            var response = constants.responseErrors.UPDATION_ERROR;
                            if (err) return callback(response);
                            userExtraInformation.ride_data = resultsFromRide;
                            callback();
                        });
                    },
                    function (callback) {
                        func.getAppVersion(function (err, resultsFromVersion) {
                            if (err) return callback(err);
                            userExtraInformation.version = resultsFromVersion;
                            callback();
                        });
                    }], function (err, callbackResponse) {
                    if (err) {
                        res.jsonp(err);
                    }
                    else {
                        var response = {"fb_id": fbId, "user_id": userId, "access_token": accessToken, "first_name": firstName, "last_name": lastName,
                            "email": email, "image": image, "flag": flag, "default_trip": defaultTrip, "new_matches": newMatches, "messages": messages,
                            "location_sharing": locationSharing, "push_notifications": pushNotification, "profile_rating": profileRating, "default_trip_name": defaultTripName,
                            new_user: constants.defaultValueSet.AFTER_SIGN_UP_TO_ZERO, missed_push: userExtraInformation.push_data, current_running_rides: userExtraInformation.ride_data,
                            version: userExtraInformation.version};
                        res.jsonp(response);
                    }
                });
            }
            else {
                authenticateUserWaitList(firstName, lastName, email, loginType, waitCode, function (err, responseFromWaiList) {
                    if (err) {
                    }
                    else if (responseFromWaiList.front_user != constants.defaultUserSettingValue.DISABLE) {
                        func.getAppVersion(function (err, resultsFromVersion) {
                            if (err) {
                                return callback(err, null);
                            }
                            else {
                                var response = {data: responseFromWaiList, new_user: constants.defaultValueSet.AT_SIGN_UP_TO_ONE, version: resultsFromVersion};
                                res.jsonp(response);
                            }
                        });
                    }
                    else {
                        registerNewUser(fbId, firstName, lastName, email, gender, age, deviceToken, userType, fbAccessToken, work, res, function (err, responseFromInsertion) {
                            if (err) {
                                res.jsonp(err)
                            }
                            else {
                                res.jsonp(responseFromInsertion)
                            }
                        });
                    }
                });
            }
        });
    }
    else if (loginType === constants.userSignUp.VIA_WAIT_LIST_CODE) {
        authenticateUserWaitList(firstName, lastName, email, loginType, waitCode, function (err, responseFromWaiList) {
            if (err) {
                res.jsonp(err);
            }
            else if (responseFromWaiList.front_user != constants.defaultUserSettingValue.DISABLE) {
                func.getAppVersion(function (err, resultsFromVersion) {
                    if (err) {
                        return callback(err, null);
                    }
                    else {
                        var response = {data: responseFromWaiList, new_user: constants.defaultValueSet.AT_SIGN_UP_TO_ONE, version: resultsFromVersion};
                        res.jsonp(response);
                    }
                });
            }
            else {
                registerNewUser(fbId, firstName, lastName, email, gender, age, deviceToken, userType, fbAccessToken, work, res, function (err, responseFromInsertion) {
                    if (err) {
                        res.jsonp(err)
                    }
                    else {
                        res.jsonp(responseFromInsertion)
                    }
                });
            }
        });
    }
}
/*
 * -----------------------------------------------------------------------------
 * Access token login
 * Input : access token, device token
 * Output : User data
 * -----------------------------------------------------------------------------
 */
exports.loginFromAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    var deviceToken = req.body.devicetoken;
    var manValues = [accessToken];
    console.log(req.body);
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["fb_id", "access_token", "first_name", "last_name", "email",
            "CASE WHEN `image` LIKE 'http%' THEN `image` ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',`image`) END as image", "default_trip", "user_type"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (results) {
            loginFromAccessTokenResult(accessToken, deviceToken, results, res);
        });
    });
};
function loginFromAccessTokenResult(accessToken, deviceToken, result, res) {
    var userId = result[0].user_id;
    updateUserParams(deviceToken, userId, result[0].user_type);
    var accessToken = result[0].access_token;
    var firstName = result[0].first_name;
    var lastName = result[0].last_name;
    var email = result[0].email;
    var fbId = result[0].fb_id;
    var image = result[0].image;
    var defaultTrip = result[0].default_trip;
    var userName = firstName + ' ' + lastName;
    //
    var userExtraInformation = []
    async.parallel([
        function (callback) {
            getUserMissedPushData(userId, function (err, responseFromMissedPush) {
                if (err) return callback(err);
                userExtraInformation.push_data = responseFromMissedPush;
                callback();
            });
        },
        function (callback) {
            getUserCurrentRideData(userId, function (err, resultsFromRide) {
                if (err) return callback(err);
                userExtraInformation.ride_data = resultsFromRide;
                callback();
            });
        },
        function (callback) {
            func.getAppVersion(function (err, resultsFromVersion) {
                if (err) return callback(err);
                userExtraInformation.version = resultsFromVersion;
                callback();
            });
        }], function (err, callbackResponse) {
        if (err) {
            var response = constants.responseErrors.UPDATION_ERROR;
            res.jsonp(response);
        }
        else {
            if (defaultTrip > 0) {
                var flag = constants.defaultValueSet.AFTER_SIGN_UP_TO_ZERO;
            }
            else {
                var flag = constants.defaultValueSet.AT_SIGN_UP_TO_ONE;
            }
            var response = {"user_id": userId, "fb_id": fbId, "access_token": accessToken, "first_name": firstName, "last_name": lastName, "email": email,
                "image": image, "flag": flag, "default_trip": defaultTrip, missed_push: userExtraInformation.push_data, current_running_rides: userExtraInformation.ride_data,
                version: userExtraInformation.version};
            res.jsonp(response);
        }
    });
};
/*
 * -----------------------------------------------------------------------------
 * Checking availability of email and username
 * Input : email
 * Output :  log 0 or log 2
 * -----------------------------------------------------------------------------
 */
function checkEmailAndUsernameAvailability(userEmail, flag, callback) {
    if (flag === constants.userRegisterThrough.APP) {
        var sql = "SELECT `user_id`,`is_deleted` FROM `user` WHERE `email`=? LIMIT 1";
    }
    else if (flag === constants.userRegisterThrough.WEB) {
        var sql = "SELECT `id`,`is_deleted` FROM `wait_list_user` WHERE `email`=? LIMIT 1";
    }
    connection.query(sql, [userEmail], function (err, results) {
        if (results.length > 0) {
            if (results[0].is_deleted == 0) {
                return callback(results, null);
            }
            else {
                return callback(null, results);
            }
        }
        else {
            return callback(null, results);
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Update device token of  an user
 * Input : user_id
 * Output : updated user params
 * -----------------------------------------------------------------------------
 */
function updateUserParams(deviceToken, userId, type) {
    var loginTime = new Date();
    if (type == constants.userDeviceType.ANDROID) {
        var sql = "UPDATE `user` SET `android_device_token`=?,`last_login_timestamp`=? WHERE `user_id`=? LIMIT 1";
    }
    else {
        var sql = "UPDATE `user` SET `ios_device_token`=?,`last_login_timestamp`=? WHERE `user_id`=? LIMIT 1";
    }
    connection.query(sql, [deviceToken, loginTime, userId], function (err, result) {
    });
}
/*
 * -----------------------------------------------------------------------------
 * Fb user registered or not
 * INPUT : userFbId
 * OUTPUT : Fb user registered or not
 * -----------------------------------------------------------------------------
 */
function authenticateFbUser(userFbId, fbAccessToken, firstName, lastName, email, gender, age, work, res, callback) {
    var urls = "https://graph.facebook.com/" + userFbId + "?fields=updated_time&access_token=" + fbAccessToken;
    request(urls, function (error, response, body) {
        if (!error && response.statusCode === constants.errorResponseCode.WHILE_FACEBOOK_AUTHENTICATION) {
            var output = JSON.parse(body);
            if (output['error']) {
                var response = constants.responseErrors.UN_AUTHORIZED_ACCESS;
                res.jsonp(response);
            }
            else {
                if (work != null || work != '') {
                    work = work.replace(/\\/g, '')
                    var output = JSON.parse(work);
                    var workLen = output.length;
                    var workData = ''
                    for (var i = 0; i < workLen; i++) {
                        var flag = 0;
                        if (output[i]['employer']) {
                            workData += output[i]['employer']['name'];
                        }
                        if (output[i]['location']) {
                            flag = 1;
                            workData += ', ' + output[i]['location']['name'] + '@';
                        }
                        if (flag == 0) {
                            workData += '@';
                        }
                    }
                    workData = workData.substring(0, workData.length - 1);
                }
                else {
                    workData = '';
                }
                var sql = "UPDATE `user` SET `first_name`=?,`last_name`=?,`email`=?,`age`=?, `work`=? WHERE `fb_id`=?  && `is_deleted` =? LIMIT 1";
                connection.query(sql, [firstName, lastName, email, age, workData, userFbId, 0], function (err, result) {
                    var sql = "SELECT `user_id`,`fb_id`,`access_token`,`image`,`default_trip`,`default_trip_name`,`user_type`,`new_matches`,`messages`,"
                    sql += "`location_sharing`,`push_notifications`,`profile_rating`,`is_deleted` FROM `user` WHERE `fb_id`=?  ORDER BY  user_id DESC LIMIT 1 ";
                    connection.query(sql, [userFbId], function (err, results) {
                        if (results.length > 0) {
                            if (results[0].is_deleted == 0) {
                                results[0].first_name = firstName;
                                results[0].last_name = lastName;
                                results[0].email = email;
                                return callback(results);
                            }
                            else {
                                return callback([]);
                            }
                        }
                        else {
                            return callback([]);
                        }
                    });
                });
            }
        }
        else {
            var response = constants.responseErrors.UN_AUTHORIZED_ACCESS;
            res.jsonp(response);
        }
    });
}
/* ------------------------------------------------------
 * Update user password from access token
 * Input: old password, new password
 * Output: log or error message
 * ------------------------------------------------------
 */
exports.updateUserPasswordFromAccessToken = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var accessToken = req.body.accesstoken;
    var oldPassword = req.body.oldpassword;
    var newPassword = req.body.newpassword;
    oldPassword = func.encrypt(oldPassword);
    newPassword = func.encrypt(newPassword);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["password"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromToken) {
            var userId = resultsFromToken[0].user_id;
            var dbPassword = resultsFromToken[0].password;
            var count = dbPassword.localeCompare(oldPassword);
            if (count == 0) {
                var sql1 = "UPDATE `user` SET `password`=? WHERE `user_id`=? LIMIT 1";
                connection.query(sql1, [newPassword, userId], function (err, updateUser) {
                    if (err) {
                        var response = constants.responseErrors.UPDATION_ERROR;
                        res.jsonp(response);
                    }
                    else {
                        var response = constants.responseErrors.PASSWORD_CHANGED_SUCCESSFULLY;
                        res.jsonp(response);
                    }
                });
            }
            else {
                var response = constants.responseErrors.INCORRECT_OLD_PASSWORD;
                res.jsonp(response);
            }
        });
    });
};
/*
 * -----------------------------------------------------------------------------
 * Getting registered  friends of fb
 * Input : fbId, fbAccessToken
 * Output :
 * -----------------------------------------------------------------------------

 */
function getFbFriendsFromFbIdAndFbAccessTokenTest(fbId, fbAccessToken, userId) {
    var request = require('request');
    var urls = "https://graph.facebook.com/" + fbId + "/friends?access_token=" + fbAccessToken;
    request(urls, function (error, response, body) {
        if (!error && response.statusCode === constants.errorResponseCode.WHILE_FACEBOOK_AUTHENTICATION) {
            var output = JSON.parse(body);
            var dbFbIds = [];
            var fbFriends = [];
            var fbFriendsName = [];
            var fbFriendLength = (output['data']).length;
            for (var j = 0; j < fbFriendLength; j++) {
                fbFriends.push(parseInt(output['data'][j].id));
            }
            var str = fbFriends.toString(',')
            if (str.length == 0) {
                str = 0;
            }
            var sql = "SELECT `user_id` FROM `user` WHERE `fb_id` IN(" + str + ")ORDER BY FIELD(`fb_id`," + str + ")";
            connection.query(sql, function (err, responseFromUser) {
                var userCount = responseFromUser.length;
                var userIdArray = new Array();
                for (var i = 0; i < userCount; i++) {
                    userIdArray.push(responseFromUser[i].user_id);
                }
                var sql = "SELECT `chat_id`,`sender_id`,`receiver_id` FROM `conversation` WHERE (`sender_id`=? ) || (`receiver_id`=? ) ";
                connection.query(sql, [ userId, userId], function (err, resultsFromConversations) {
                    var conversationLength = resultsFromConversations.length;
                    var chatUserArray = new Array();
                    if (conversationLength > 0) {
                        for (var i = 0; i < conversationLength; i++) {
                            if (resultsFromConversations[i].sender_id != userId) {
                                chatUserArray.push(resultsFromConversations[i].sender_id);
                            }
                            else if (resultsFromConversations[i].receiver_id != userId) {
                                chatUserArray.push(resultsFromConversations[i].receiver_id);
                            }
                        }
                        var newUserIds = chatUserArray.diff(userIdArray);
                    }
                    else {
                        newUserIds = userIdArray;
                    }
                    var newUserCount = newUserIds.length;
                    var jsonData = '';
                    for (var j = 0; j < newUserCount; j++) {
                        jsonData += "  (" + userId + "," + newUserIds[j] + ", NOW() " + "),";
                    }
                    jsonData = jsonData.substring(0, jsonData.length - 1);
                    var sqlConversation = "INSERT INTO `conversation`(`sender_id`,`receiver_id`,`created_at`) VALUES " + jsonData + "";
                    connection.query(sqlConversation, function (err, resultFromConversation) {
                    });
                });
            });
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Array difference
 * Input : arrayA, arrayB
 * Output :
 * -----------------------------------------------------------------------------
 */
Array.prototype.diff = function (a) {
    return this.filter(function (i) {
        return a.indexOf(i) < 0;
    });
};
/*
 * -----------------------------------------------------------------------------
 * Get user facebook data
 * Input : fbId, fbAccessToken
 * Output :
 * -----------------------------------------------------------------------------

 */
function getUserFacebookData(fbId, fbAccessToken, userId) {
    var request = require('request');
    var sql = "DELETE  FROM `fb_user` WHERE `user_id` =? ";
    connection.query(sql, [ userId], function (err, resultsFromFbUsers) {
        var sql = "DELETE  FROM `user_interest` WHERE `user_id` =? ";
        connection.query(sql, [ userId], function (err, resultsFromFbUsers) {
            var urls = "https://graph.facebook.com/" + fbId + "?fields=likes{name,picture},friends&access_token=" + fbAccessToken;
            request(urls, function (error, response, body) {
                if (!error && response.statusCode === constants.errorResponseCode.WHILE_FACEBOOK_AUTHENTICATION) {
                    var output = JSON.parse(body);
                    var dbFbIds = [];
                    var fbFriends = [];
                    var fbFriendsName = [];
                    var fbUserCount = output['friends']['data'].length;
                    var likesCount = output['likes']['data'].length;
                    var fbIdArray = new Array();
                    var jsonDataFriends = '';
                    for (var j = 0; j < fbUserCount; j++) {
                        jsonDataFriends += "  (" + userId + "," + output['friends']['data'][j].id + ",'" + output['friends']['data'][j].name + "'" + "),";
                    }
                    jsonDataFriends = jsonDataFriends.substring(0, jsonDataFriends.length - 1);
                    var sql = "INSERT INTO `fb_user`(`user_id`,`fb_id`,`fb_name`) VALUES  " + jsonDataFriends + "";
                    connection.query(sql, function (err, result) {
                    });
                    var jsonDataLikes = '';
                    for (var j = 0; j < likesCount; j++) {
                        var name = output['likes']['data'][j].name.replace(/['"]+/g, '');
                        jsonDataLikes += '  (' + userId + ',' + output['likes']['data'][j].id + ',"' + name + '"' + '),';
                    }
                    jsonDataLikes = jsonDataLikes.substring(0, jsonDataLikes.length - 1);
                    var sql = 'INSERT INTO `user_interest`(`user_id`,`fb_id`,`name`) VALUES  ' + jsonDataLikes + '';
                    connection.query(sql, function (err, result) {
                    });
                }
            });
        });
    });
}
/*
 * -----------------------------------------------------------------------------
 * Get user profile data
 * Input : access token
 * Output : User data
 * -----------------------------------------------------------------------------
 */
exports.getUserProfileDataFromAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    var fbAccessToken = req.body.fbaccesstoken;
    var manValues = [accessToken];
    console.log(req.body)
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["gender", "about", "fb_id"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getUserProfileDataFromAccessTokenResult(resultsFromAccessToken, fbAccessToken, res, req);
        });
    });
};
function getUserProfileDataFromAccessTokenResult(resultsFromAccessToken, fbAccessToken, res, req) {
    var sql = "DELETE  FROM `fb_user` WHERE `user_id` =? ";
    connection.query(sql, [ resultsFromAccessToken[0].user_id], function (err, resultsFromFbUsers) {
        var sql = "DELETE  FROM `user_interest` WHERE `user_id` =? ";
        connection.query(sql, [ resultsFromAccessToken[0].user_id], function (err, resultsFromFbUsers) {
            var sql = "SELECT `id`,CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image) END as image" +
                " FROM `user_image` WHERE `user_id`=?";
            connection.query(sql, [resultsFromAccessToken[0].user_id], function (err, resultsImages) {
                var finalArray = [];
                var fbId = resultsFromAccessToken[0].fb_id;
                var urls = "https://graph.facebook.com/" + fbId + "?fields=likes{name,picture},friends&access_token=" + fbAccessToken;
                request(urls, function (error, response, body) {
                    if (!error && response.statusCode == constants.errorResponseCode.WHILE_FACEBOOK_AUTHENTICATION) {
                        var output = JSON.parse(body);
                        var fbFriends = [];
                        var fbLikes = [];
                        var fbIdArray = new Array();
                        var jsonDataFriends = '';
                        if (output['friends']) {
                            var fbUserCount = output['friends']['data'].length;
                            for (var j = 0; j < fbUserCount; j++) {
                                fbFriends.push({"id": output['friends']['data'][j].id, "name": output['friends']['data'][j].name,
                                    "image": "http://graph.facebook.com/" + output['friends']['data'][j].id + "/picture?width=225&height=225" });
                                var image = "http://graph.facebook.com/" + output['friends']['data'][j].id + "/picture?width=225&height=225";
                                jsonDataFriends += "  (" + resultsFromAccessToken[0].user_id + "," + output['friends']['data'][j].id + ",'" + output['friends']['data'][j].name + "','" + image + "'" + "),";
                            }
                            jsonDataFriends = jsonDataFriends.substring(0, jsonDataFriends.length - 1);
                            var sql = "INSERT INTO `fb_user`(`user_id`,`fb_id`,`fb_name`,`image`) VALUES  " + jsonDataFriends + "";
                            connection.query(sql, function (err, result) {
                                console.log(err);
                            });
                        }
                        var jsonDataLikes = '';
                        if (output['likes']) {
                            var likesCount = output['likes']['data'].length;
                            for (var j = 0; j < likesCount; j++) {
                                fbLikes.push({"id": output['likes']['data'][j].id, "name": output['likes']['data'][j].name,
                                    "image": "http://graph.facebook.com/" + output['likes']['data'][j].id + "/picture?width=225&height=225"});
                                var name = output['likes']['data'][j].name.replace(/['"]+/g, '');
                                var image = "http://graph.facebook.com/" + output['likes']['data'][j].id + "/picture?width=225&height=225";
                                jsonDataLikes += '  (' + resultsFromAccessToken[0].user_id + ',' + output['likes']['data'][j].id + ',"' + name + '"' + ',"' + image + '"' + '),';
                            }
                            jsonDataLikes = jsonDataLikes.substring(0, jsonDataLikes.length - 1);
                            var sql = 'INSERT INTO `user_interest`(`user_id`,`fb_id`,`name`,`image`) VALUES  ' + jsonDataLikes + '';
                            connection.query(sql, function (err, result) {
                                console.log(err);
                            });
                        }
                    }
                    func.getCardDetailsOfUserThroughUserId(resultsFromAccessToken[0].user_id, res, function (resultsFromCard) {
                        res.jsonp({"gender": resultsFromAccessToken[0].gender, "about": resultsFromAccessToken[0].about, "images": resultsImages,
                            "friends": fbFriends, "interests": fbLikes, "cards": resultsFromCard});
                    });
                });
            });
        });
    });
}
/*
 * -----------------------------------------------------------------------------
 * edit user profile data
 * Input : access token
 * Output : User data
 * -----------------------------------------------------------------------------
 */
exports.editUserProfileDataFromAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    var gender = req.body.gender;
    var about = req.body.about;
    var fileStatus = req.body.filestatus;
    var newFileStatus = req.body.newfilestatus;
    var imageId = req.body.imageid;
    var removeIds = req.body.removeids;
    var isProfilePictureChanged = req.body.profilepic
    var manValues = [accessToken];
    console.log(req.body);
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["gender", "about"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            editUserProfileDataFromAccessTokenResult(resultsFromAccessToken, gender, about, req.files, fileStatus, imageId, newFileStatus, removeIds, isProfilePictureChanged, res, req);
        });
    });
};
function editUserProfileDataFromAccessTokenResult(resultsFromAccessToken, gender, about, files, fileStatus, imageId, newFileStatus, removeIds, isProfilePictureChanged, res, req) {
    var sql = "UPDATE `user` SET `gender`=?, `about`=? WHERE `user_id` =? LIMIT 1";
    connection.query(sql, [gender, about, resultsFromAccessToken[0].user_id], function (err, result) {
    })
    var flag = 0;
    var flagInsert = 0;
    if (removeIds) {
        var sql = "DELETE  FROM `user_image` WHERE `id` IN(" + removeIds + ") ";
        connection.query(sql, function (err, resultsFromFbUsers) {
        });
    }
    if (fileStatus > 0) {
        var counter = 0;
        func.uploadMultipleAttachmentToS3Bucket(files.userimage, fileStatus, 'user_image', function (fileName) {
            var imageIdArray = imageId.split(',');
            var idLength = imageIdArray.length;
            var sql = "UPDATE `user_image` SET `image`=? WHERE `id` =? LIMIT 1";
            for (var i = 0; i < idLength; i++) {
                (function (i) {
                    connection.query(sql, [fileName[i], imageIdArray[i]], function (err, result) {
                        ++counter;
                        if (counter == idLength) {
                            flag = 1;
                            if (flag == 1 && flagInsert == 1) {
                                var sql = "SELECT `id`,CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image)  END as image FROM `user_image` WHERE `user_id`=?";
                                connection.query(sql, [resultsFromAccessToken[0].user_id], function (err, resultsImages) {
                                    res.jsonp(resultsImages);
                                    if (isProfilePictureChanged == 1) {
                                        var sql = "UPDATE `user` SET `image`=? WHERE `user_id`=? LIMIT 1";
                                        connection.query(sql, [resultsImages[0].image, resultsFromAccessToken[0].user_id], function (err, result) {
                                        });
                                    }
                                });
                            }
                        }
                    });
                })(i);
            }
        });
    }
    else {
        flag = 1;
        if (flag == 1 && flagInsert == 1) {
            var sql = "SELECT `id`,CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image)  END as image " +
                "FROM `user_image` WHERE `user_id`=?";
            connection.query(sql, [resultsFromAccessToken[0].user_id], function (err, resultsImages) {
                res.jsonp(resultsImages);
                if (isProfilePictureChanged == 1) {
                    var sql = "UPDATE `user` SET `image`=? WHERE `user_id`=? LIMIT 1";
                    connection.query(sql, [resultsImages[0].image, resultsFromAccessToken[0].user_id], function (err, result) {
                    });
                }
            });
        }
    }
    if (newFileStatus > 0) {
        func.uploadMultipleAttachmentToS3Bucket(files.newuserimage, newFileStatus, 'user_image', function (newFileName) {
            var newImagesCount = newFileName.length;
            var jsonData = '';
            for (var i = 0; i < newImagesCount; i++) {
                jsonData += "  (" + resultsFromAccessToken[0].user_id + ",'" + newFileName[i] + "'," + "NOW() " + "),";
            }
            jsonData = jsonData.substring(0, jsonData.length - 1);
            var sql = "INSERT INTO `user_image`(`user_id`,`image`,`created_at`) VALUES " + jsonData + "";
            connection.query(sql, function (err, resultFromIndividuals) {
                flagInsert = 1;
                if (flag == 1 && flagInsert == 1) {
                    var sql = "SELECT `id`,CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image)  END as image" +
                        " FROM `user_image` WHERE `user_id`=?";
                    connection.query(sql, [resultsFromAccessToken[0].user_id], function (err, resultsImages) {
                        res.jsonp(resultsImages);
                        if (isProfilePictureChanged == 1) {
                            var sql = "UPDATE `user` SET `image`=? WHERE `user_id`=? LIMIT 1";
                            connection.query(sql, [resultsImages[0].image, resultsFromAccessToken[0].user_id], function (err, result) {
                            });
                        }
                    });
                }
            });
        });
    }
    else {
        flagInsert = 1;
        if (flag == 1 && flagInsert == 1) {
            var sql = "SELECT `id`,CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image)  END as image" +
                " FROM `user_image` WHERE `user_id`=?";
            connection.query(sql, [resultsFromAccessToken[0].user_id], function (err, resultsImages) {
                res.jsonp(resultsImages);
                if (isProfilePictureChanged == 1) {
                    var sql = "UPDATE `user` SET `image`=? WHERE `user_id`=? LIMIT 1";
                    connection.query(sql, [resultsImages[0].image, resultsFromAccessToken[0].user_id], function (err, result) {
                    });
                }
            });
        }
    }
}
function getFbProfileImages(fbId, fbAccessToken, callback) {
    var picArray = new Array();
    var urls = "https://graph.facebook.com/" + fbId + "/albums?access_token=" + fbAccessToken;
    request(urls, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var output = JSON.parse(body);
            var count = output['data'].length;
            if (count == 0) {
                picArray.push("https://graph.facebook.com/" + fbId + "/picture?width=225&height=225")
                return callback(picArray)
            }
            sendQueries();
            function sendQueries() {
                var i = 0;

                function next() {
                    if (i < count) {
                        if (output['data'][i].name == 'Profile Pictures') {
                            var profilePicId = output['data'][i].id;
                            var urls = "https://graph.facebook.com/" + profilePicId + "/photos?access_token=" + fbAccessToken;
                            request(urls, function (error, response, body) {
                                if (!error && response.statusCode == 200) {
                                    i++;
                                    var outputPictures = JSON.parse(body);
                                    var picsLength = outputPictures['data'].length;
                                    if (picsLength == 0) {
                                        picArray.push("https://graph.facebook.com/" + fbId + "/picture?width=225&height=225")
                                    }
                                    var s = 0;
                                    for (var j = 0; j < picsLength; j++) {
                                        ++s;
                                        if (s > 6) {
                                            break;
                                        }
                                        picArray.push(outputPictures['data'][j].source)
                                    }
                                    return callback(picArray);
                                }
                                else {
                                    i++;
                                    return callback([]);
                                }
                            });
                        }
                        else {
                            i++;
                            if (i == count) {
                                return callback([]);
                            }
                            next();
                        }
                    }
                }

                next();
            }
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Authenticate wait list user
 * Input : email
 * Output :
 * -----------------------------------------------------------------------------
 */
function authenticateUserWaitList(firstName, lastName, email, flag, waitCode, callback) {
    var userName = firstName + ' ' + lastName;
    if (flag == constants.userSignUp.VIA_FACEBOOK) {
        getDataOfWaitListUser(email, constants.getWaitListUserData.THROUGH_EMAIL, function (err, responseFromWaitList) {
            if (err) {
                var response = constants.responseErrors.FETCHING_DATA;
                return callback(response, null);
            }
            else {
                if (responseFromWaitList.length == 0) {
                    generateAndVerifyWaitCode(function (err, waitCode) {
                        if (err) {
                            var response = constants.responseErrors.UPDATION_ERROR;
                            res.jsonp(response);
                        }
                        else {
                    var sql = "INSERT INTO `wait_list_user`(`name`,`email`,`wait_code`) VALUES (?,?,?)";
                    connection.query(sql, [userName, email, waitCode], function (err, responseFromWaitListInsertion) {
                        getWaitListOfUser(responseFromWaitListInsertion.insertId, function (err, responseCallback) {
                            if (err) {
                                return callback(err, null);
                            }
                            else {
                                var subject = 'Hytch : Welcome';
                                var message = "\tHi " + userName + ", <br> <br> <br>";
                                message +=
                                    "Thanks for Registering with Hytch. <br><br> <br>" +
                                        "Wait Listing for your email ID " + email + " is " + responseCallback.front_user + "  <br>" +
                                        "Your Unique Referral Code is " + waitCode + ". Enter this Code in Hytch iOS or Android Application to check your updated Wait listing Number.<br> <br>" +
                                        "We would be rolling out Complete Version for the Application soon. Would keep you informed on that. <br> <br> <br>" +
                                        "For any queries, contact info@hytch.me  <br> <br> <br>";
                                message += "Thanks  <br> <br>";
                                message += "Hytch Team";
                                func.sendHtmlContent(email, message, subject, function (err, responseFromSendMessage) {
                                });
                                return callback(null, responseCallback);
                            }
                        });
                    });
                }
            });
                }
                else {
                    getWaitListOfUser(responseFromWaitList[0].id, function (err, responseCallback) {
                        if (err) {
                            return callback(err, null);
                        }
                        else {
                            if(responseFromWaitList[0].wait_status == constants.defaultValueSet.AT_SIGN_UP_TO_ONE){
                                var waitListResponse = {front_user: constants.defaultValueSet.AFTER_SIGN_UP_TO_ZERO, behind_user:responseCallback.behind_user}
                                return callback(null, waitListResponse);
                            }
                            return callback(null, responseCallback);
                        }
                    });
                }
            }
        });
    }
    else if (flag == constants.userSignUp.VIA_WAIT_LIST_CODE) {
        finalResponse = []
        waitListUserDataArray = [];
        var isClearedWaitList = 0;
        async.series([
            function (callback) {
                getDataOfWaitListUser(email, constants.getWaitListUserData.THROUGH_EMAIL, function (err, responseFromWaitListUser) {
                    if (err) return callback(err);
                    waitListUserDataArray.email_data = responseFromWaitListUser;
                    getWaitListOfUser(responseFromWaitListUser[0].id, function (err, responseFromWaitListByEmail) {
                        finalResponse.email_wait_list = responseFromWaitListByEmail;
                        if(responseFromWaitListUser[0].wait_status == constants.defaultValueSet.AT_SIGN_UP_TO_ONE){
                            isClearedWaitList = constants.defaultValueSet.AT_SIGN_UP_TO_ONE;
                        }
                        callback();
                    });

                });
            },
            function (callback) {
                if(!isClearedWaitList){
                getDataOfWaitListUser(waitCode, constants.getWaitListUserData.THROUGH_WAIT_CODE, function (err, responseFromWaitListUser) {
                    if (err) return callback(err);
                    waitListUserDataArray.code_data = responseFromWaitListUser;
                    getWaitListOfUser(responseFromWaitListUser[0].id, function (err, responseFromWaitListByCode) {
                        finalResponse.code_wait_list = responseFromWaitListByCode;
                        callback();
                    });
                });
                }
                else{
                    callback();
                }
            }], function (err, localsdata) {
            if (err) {
                return callback(err, null);
            }
            else {
                if(isClearedWaitList){
                    var waitListResponse = {front_user: constants.defaultValueSet.AFTER_SIGN_UP_TO_ZERO, behind_user: finalResponse.email_wait_list.behind_user}
                    return callback(null, waitListResponse);
                }
                else if (finalResponse.code_wait_list['front_user'] < finalResponse.email_wait_list['front_user']) {
                    var oldUserName = waitListUserDataArray.email_data[0]['name'];
                    var oldWorkLatitude = waitListUserDataArray.email_data[0]['work_address_latitude'];
                    var oldWorkLongitude = waitListUserDataArray.email_data[0]['work_address_longitude'];
                    var oldWorkLocationString = waitListUserDataArray.email_data[0]['work_address_location_string'];
                    var oldHomeLatitude = waitListUserDataArray.email_data[0]['home_address_latitude'];
                    var oldHomeLongitude = waitListUserDataArray.email_data[0]['home_address_longitude'];
                    var oldHomeLocationString = waitListUserDataArray.email_data[0]['home_address_location_string'];
                    var newUpdatedId = waitListUserDataArray.code_data[0]['id'];
                    var sql = "UPDATE `wait_list_user` SET `name`=?, `email`=?, `work_address_latitude`=?,`work_address_longitude`=?,`work_address_location_string`=?,";
                    sql += "`home_address_latitude`=?,`home_address_longitude`=?,`home_address_location_string`=? WHERE `id`=? LIMIT 1";
                    connection.query(sql, [userName, email, oldWorkLatitude, oldWorkLongitude, oldWorkLocationString, oldHomeLatitude, oldHomeLongitude, oldHomeLocationString, newUpdatedId ], function (err, result) {
                        if (err) {
                            var response = constants.responseErrors.FETCHING_DATA;
                            return callback(response, null);
                        }
                        else {
                            return callback(null, finalResponse.code_wait_list);
                        }
                    });
                }
                else {
                    return callback(null, finalResponse.email_wait_list);
                }
            }
        });
    }
}
function getWaitListOfUser(userId, callback) {
    var finalResponse = {};
    async.series([
        function (callback) {
            var sql = "SELECT COUNT(*) as front_user FROM `wait_list_user` WHERE `id` <= ? AND `wait_status` = ?";
            connection.query(sql, [userId, constants.defaultUserSettingValue.DISABLE], function (err, responseFromWaitList) {
                if (err) return callback(err);
                finalResponse.front_user = responseFromWaitList[0].front_user;
                callback();
            });
        },
        function (callback) {
            var sql = "SELECT COUNT(*) as behind_user FROM `wait_list_user` WHERE `id` > ? AND `wait_status` = ?";
            connection.query(sql, [userId, constants.defaultUserSettingValue.DISABLE], function (err, responseFromWaitList) {
                if (err) return callback(err);
                finalResponse.behind_user = responseFromWaitList[0].behind_user;
                callback();
            });
        }], function (err, localsdata) {
        if (err) {
            return callback(err, null);
        }
        else {
            return callback(null, finalResponse);
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Get data of wait list user
 * Input : id or email
 * Output :
 * -----------------------------------------------------------------------------
 */
function getDataOfWaitListUser(conditionalData, flag, callback) {
    if (flag == constants.getWaitListUserData.THROUGH_EMAIL) {
        var sql = "SELECT * FROM  `wait_list_user` WHERE `email`=? LIMIT 1";
        connection.query(sql, [conditionalData], function (err, responseFromWaitList) {
            if (err) {
                console.log(err);
                var response = constants.responseErrors.FETCHING_DATA;
                return callback(response, null);
            }
            else {
                return callback(null, responseFromWaitList);
            }
        });
    }
    else {
        var sql = "SELECT * FROM  `wait_list_user` WHERE `wait_code`=? LIMIT 1";
        connection.query(sql, [conditionalData], function (err, responseFromWaitList) {
            if (err) {
                console.log(err);
                var response = constants.responseErrors.FETCHING_DATA;
                return callback(response, null);
            }
            else {
                if (responseFromWaitList.length > 0) {
                    return callback(null, responseFromWaitList);
                }
                else {
                    var response = constants.responseErrors.INVALID_REFERRAL_CODE;
                    return callback(response, null);
                }
            }
        });
    }
}
/*
 * -----------------------------------------------------------------------------
 * Register new user details
 * Input fbId, firstName, lastName, email, gender, age, deviceToken, userType, fbAccessToken, work
 * Output :
 * -----------------------------------------------------------------------------
 */
function registerNewUser(fbId, firstName, lastName, email, gender, age, deviceToken, userType, fbAccessToken, work, res, callback) {
    checkEmailAndUsernameAvailability(email, constants.userRegisterThrough.APP, function (err, responseOfCheckEmail) {
        if (err) {
            var response = constants.responseErrors.EMAIL_ALREADY_REGISTERED;
            res.jsonp(response);
        }
        else {
            var timestamp = new Date();
            var accessToken = (new Buffer(email + timestamp).toString('base64'));
            var image = "http://graph.facebook.com/" + fbId + "/picture?width=250&height=250";
            if (work != null || work != '') {
                work = work.replace(/\\/g, '')
                var output = JSON.parse(work);
                var workLen = output.length;
                var workData = ''
                for (var i = 0; i < workLen; i++) {
                    var flag = 0;
                    if (output[i]['employer']) {
                        workData += output[i]['employer']['name'];
                    }
                    if (output[i]['location']) {
                        flag = 1;
                        workData += ', ' + output[i]['location']['name'] + '@';
                    }
                    if (flag == 0) {
                        workData += '@';
                    }
                }
                workData = workData.substring(0, workData.length - 1);
            }
            else {
                workData = '';
            }
            if (userType === constants.userDeviceType.ANDROID) {
                var sql = "INSERT INTO `user`(`fb_id`,`first_name`,`last_name`,`email`,`gender`,`age`,`image`,`android_device_token`,`access_token`,`user_type`,`work`,";
                sql += "`register_timestamp`,`last_login_timestamp`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            }
            else if (userType === constants.userDeviceType.IOS) {
                var sql = "INSERT INTO `user`(`fb_id`,`first_name`,`last_name`,`email`,`gender`,`age`,`image`,`ios_device_token`,`access_token`,`user_type`,`work`,";
                sql += "`register_timestamp`,`last_login_timestamp`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            }
            connection.query(sql, [fbId, firstName, lastName, email, gender, age, image, deviceToken, accessToken, userType, workData, timestamp, timestamp],
                function (err, resultFromInsertion) {
                    if (err) {
                        var response = constants.responseErrors.UPDATION_ERROR;
                        return callback(response, null);
                    }
                    else {
                        func.getAppVersion(function (err, resultsFromVersion) {
                            if (err) {
                                return callback(err, null);
                            }
                            else {
                                var userId = resultFromInsertion.insertId;
                                var defaultValue = constants.defaultValueSet.AT_SIGN_UP_TO_ONE;
                                var response = {"fb_id": fbId, "user_id": userId, "access_token": accessToken, "first_name": firstName, "last_name": lastName, "email": email, "image": image,
                                    "flag": defaultValue, "default_trip": "", "new_matches": defaultValue, "messages": defaultValue, "location_sharing": defaultValue,
                                    "push_notifications": defaultValue, "profile_rating": defaultValue, "default_trip_name": '', new_user: constants.defaultValueSet.AFTER_SIGN_UP_TO_ZERO,
                                    missed_push: [], current_running_rides: [], version: resultsFromVersion};
                                getFbProfileImages(fbId, fbAccessToken, function (resultFromPhotos) {
                                    var profileImageCount = resultFromPhotos.length;
                                    var jsonDataImages = '';
                                    for (var j = 0; j < profileImageCount; j++) {
                                        jsonDataImages += '  (' + userId + ',"' + resultFromPhotos[j] + '",' + " NOW() " + '),';
                                    }
                                    jsonDataImages = jsonDataImages.substring(0, jsonDataImages.length - 1);
                                    var sql = 'INSERT INTO `user_image`(`user_id`,`image`,`created_at`) VALUES  ' + jsonDataImages + '';
                                    connection.query(sql, function (err, result) {
                                    });
                                    var sql = "UPDATE `user` SET `image`=? WHERE `user_id`=? LIMIT 1";
                                    connection.query(sql, [resultFromPhotos[0], userId], function (err, result) {
                                    });
                                    return callback(err, response);
                                });
                            }
                        });
                    }
                });
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Register new user for website
 * Input :
 * Output :
 * -----------------------------------------------------------------------------
 */
exports.registerNewUserFromWebsite = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var name = req.body.name;
    var email = req.body.email;
    var phone = req.body.phone;
    var workAddressLatitude = req.body.work_address_latitude;
    var workAddressLongitude = req.body.work_address_longitude;
    var workAddressLocation = req.body.work_address_location_string;
    var homeAddressLatitude = req.body.home_address_latitude;
    var homeAddressLongitude = req.body.home_address_longitude;
    var homeAddressLocation = req.body.home_address_location_string;
    console.log(req.body);
    var manValues = [name, email, phone];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        registerNewUserFromWebsiteResult(name, email, phone, workAddressLatitude, workAddressLongitude, workAddressLocation, homeAddressLatitude, homeAddressLongitude, homeAddressLocation, res, req);
    });
};
function registerNewUserFromWebsiteResult(name, email, phone, workAddressLatitude, workAddressLongitude, workAddressLocation, homeAddressLatitude, homeAddressLongitude, homeAddressLocation, res, req) {
    generateAndVerifyWaitCode(function (err, waitCode) {
        if (err) {
            var response = constants.responseErrors.UPDATION_ERROR;
            res.jsonp(response);
        }
        else {
            checkEmailAndUsernameAvailability(email, constants.userRegisterThrough.WEB, function (err, responseOfCheckEmail) {
                if (err) {
                    console.log(err)
                    getWaitListOfUser(err[0].id, function (err, responseCallback) {
                        if (err) {
                            res.jsonp(err);
                        }
                        else {
                            var response = {data: responseCallback, new_user: constants.defaultValueSet.AFTER_SIGN_UP_TO_ZERO};
                            res.jsonp(response);
                        }
                    });
                }
                else {
                    var sql = "INSERT INTO  `wait_list_user` (`name`, `email`, `phone`, `work_address_latitude`,`work_address_longitude`,`work_address_location_string`,";
                    sql += "`home_address_latitude`,`home_address_longitude`,`home_address_location_string`,`wait_code`) VALUES(?,?,?,?,?,?,?,?,?,?)";
                    connection.query(sql, [name, email, phone, workAddressLatitude, workAddressLongitude, workAddressLocation,
                        homeAddressLatitude, homeAddressLongitude, homeAddressLocation , waitCode],
                        function (err, resultFromInsertion) {
                            if (err) {
                                console.log(err);
                                var response = constants.responseErrors.UPDATION_ERROR;
                                res.jsonp(response);
                            }
                            else {
                                getWaitListOfUser(resultFromInsertion.insertId, function (err, responseCallback) {
                                    if (err) {
                                        res.jsonp(err);
                                    }
                                    else {
                                        var response = {data: responseCallback, new_user: constants.defaultValueSet.AT_SIGN_UP_TO_ONE};
                                        res.jsonp(response);
                                        var messagePhone = "\tHi " + name + ",\n";
                                        messagePhone +=
                                            "Thanks for Registering with Hytch. \n" +
                                                "Wait Listing for your email ID " + email + " is " + responseCallback.front_user + "  \n" +
                                                "Your Unique Referral Code is " + waitCode + ". Enter this Code in Hytch iOS or Android Application to check your updated Wait listing Number.\n";
                                        sendTextMessageToRegisteredPhone(phone, messagePhone, function (err, responseFromSendMessage) {
                                            if (!err) {
                                                var subject = 'Hytch : Welcome';
                                                var message = "\tHi " + name + ", <br> <br> <br>";
                                                message +=
                                                    "Thanks for Registering with Hytch. <br><br> <br>" +
                                                        "Wait Listing for your email ID " + email + " is " + responseCallback.front_user + "  <br>" +
                                                        "Your Unique Referral Code is " + waitCode + ". Enter this Code in Hytch iOS or Android Application to check your updated Wait listing Number.<br> <br>" +
                                                        "We would be rolling out Complete Version for the Application soon. Would keep you informed on that. <br> <br> <br>" +
                                                        "For any queries, contact info@hytch.me  <br> <br> <br>";
                                                message += "Thanks  <br> <br>";
                                                message += "Hytch Team";
                                                func.sendHtmlContent(email, message, subject, function (err, responseFromSendMessage) {
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                }
            });
        }
    });
}
function sendTextMessageToRegisteredPhone(phone, messagePhone, callback) {
    client.messages.create({
        to: phone, // Any number Twilio can deliver to
        from: config.get('twilioSettings.fromNumber'),
        body: messagePhone // body of the SMS message
    }, function (err, message) {
        if (err) {
            console.log(err);
            var response = constants.responseErrors.TWILIO_ERROR;
            return callback(response, null);
        }
        else {
            return callback(null, 1);
        }
    });
}
function generateAndVerifyWaitCode(callback) {
    var waitCode = func.generateRandomInteger();
    getDataOfWaitListUser(waitCode, constants.getWaitListUserData.THROUGH_WAIT_CODE, function (err, responseFromWaitListUser) {
        if (err) {
            callback(null, waitCode)
        }
        else {
            generateAndVerifyWaitCode(function (err, response) {
            });
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Get user missed push data
 * Input : userId
 * Output :
 * -----------------------------------------------------------------------------
 */
function getUserMissedPushData(userId, callback) {
    var sql = "SELECT * FROM `rider_pickup_location_missed_push` WHERE `rider_id`=? AND `is_deleted`=? LIMIT 1";
    connection.query(sql, [userId, constants.userDeleteStatus.NOT_DELETED], function (err, responseFromMissedPush) {
        if (err) return callback(err, null);
        callback(null, responseFromMissedPush);
    });
}
/*
 * -----------------------------------------------------------------------------
 * Get user current ride data
 * Input : userId
 * Output :
 * -----------------------------------------------------------------------------
 */
function getUserCurrentRideData(userId, callback) {
    var sql = "SELECT `id`,`rider_id`,`driver_id` FROM `user_ride` WHERE (`rider_id`=? || `driver_id`=?) AND `ride_running_status`=?  ORDER BY id DESC LIMIT 1";
    connection.query(sql, [userId, userId, constants.userRideCompleteStatus.RUNNING], function (err, responseFromRide) {
        if (err) {
            var response = constants.responseErrors.UPDATION_ERROR;
            return callback(response, null);
        }
        else {
            if (responseFromRide.length > 0) {
                if (userId == responseFromRide[0].driver_id) {
                    var opponentId = responseFromRide[0].rider_id;
                }
                else {
                    var opponentId = responseFromRide[0].driver_id;
                }
                func.getConversationDetailsBetweenTwoUser(userId, opponentId, function (err, resultsFromConversationUsers) {
                    if (err) return callback(err, null);
                    callback(null, [resultsFromConversationUsers]);
                });
            }
            else {
                callback(null, []);
            }
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Create admin
 * Input : id
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.createAdmin = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var requestedParams = req.body;
    console.log(req.body);
    var manValues = [requestedParams.email, requestedParams.password];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        createAdminResult(requestedParams, req, res);
    });
};
function createAdminResult(requestedParams, req, res) {
    checkEmailAndUsernameAvailability(requestedParams.email, constants.userRegisterThrough.APP, function (err, responseOfCheckEmail) {
        var hash = md5(requestedParams.password);
        var timestamp = new Date();
        if (err) {
            var sql = "UPDATE `user` SET `password`=?, `is_admin`=?, `last_login_timestamp`=? WHERE `email`=? LIMIT 1";
            connection.query(sql, [hash, constants.userProfileType.ADMIN, timestamp, requestedParams.email], function (err, resultsFromUsers) {
                console.log(err);
                if (!err) {
                    var response = {email: requestedParams.email};
                    res.jsonp(response);
                }
            });
        }
        else {
            var accessToken = (new Buffer(requestedParams.email + timestamp)
                .toString('base64'));
            var image = constants.defaultImage.USER;
            var sql = "INSERT INTO `user`(`first_name`,`password`,`email`,`access_token`,`is_admin`,`image`,`register_timestamp`,`last_login_timestamp`) VALUES (?,?,?,?,?,?,?,?)";
            connection.query(sql, [requestedParams.name, hash, requestedParams.email, accessToken, constants.userProfileType.ADMIN, image, timestamp, timestamp], function (err, resultsFromPromoCode) {
                console.log(err);
                if (!err) {
                    var response = {email: requestedParams.email};
                    res.jsonp(response);
                }
            });
        }
    });
}