/**
 * Created by clicklabs on 9/22/14.
 */
var func = require('./functions');
var constants = require('./constants');
var request = require('request');
var dist = require('geolib');
var async = require('async');
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Create new trip
 * Input : access token,trip name,latitude,longitude,route id,route string, profile, age
 * Output :user's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.createNewUserTripThroughAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    var tripName = req.body.tripname;
    var startLatitude = req.body.startlatitude;
    var startLongitude = req.body.startlongitude;
    var endLatitude = req.body.endlatitude;
    var endLongitude = req.body.endlongitude;
    var profile = req.body.profile;
    var routeId = req.body.routeid;
    var routeString = req.body.routestring;
    var defaultTrip = req.body.defaulttrip;
    var startTripString = req.body.starttripstring;
    var endTripString = req.body.endtripstring;
    var minAge = req.body.minage;
    var maxAge = req.body.maxage;
    var routeData = req.body.routedata;
    var pickRadiusFromStarting = req.body.pickradiusfromstarting;
    var dropRadiusFromEnding = req.body.dropradiusfromending;
    var routeRadius = req.body.routeradius;
    var showGender = req.body.showgender;
    var distance = req.body.distance;
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["age"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            createNewUserTripThroughAccessTokenResult(resultsFromAccessToken, tripName, startLatitude, startLongitude, endLatitude, endLongitude,
                profile, minAge, maxAge, defaultTrip, routeId, routeString, routeData, startTripString, endTripString, pickRadiusFromStarting,
                dropRadiusFromEnding, routeRadius, showGender, distance, res, req);
        });
    });
};
function createNewUserTripThroughAccessTokenResult(resultsFromAccessToken, tripName, startLatitude, startLongitude, endLatitude, endLongitude, profile, minAge, maxAge, defaultTrip, routeId, routeString, routeData, startTripString, endTripString, pickRadiusFromStarting, dropRadiusFromEnding, routeRadius, showGender, distance, res, req) {
    var timestamp = new Date();
    var userId = resultsFromAccessToken[0].user_id;
    var useAge = resultsFromAccessToken[0].age;
    if (defaultTrip == 0) {
        var sql = "INSERT INTO `trip`(`user_id`,`user_age`,`name`,`start_latitude`,`start_longitude`,`end_latitude`,`end_longitude`,`profile`,`min_age`,`max_age`,`route_id`,";
        sql += "`route_string`,`default`,`route_data`,`start_trip_string`,`end_trip_string`,`pick_radius_from_starting`,`drop_radius_from_ending`,`pick_radius_from_route`";
        sql += ",`show_gender`,`distance`,`created_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        connection.query(sql, [userId, useAge, tripName, startLatitude, startLongitude, endLatitude, endLongitude, profile, minAge, maxAge,
            routeId, routeString, defaultTrip, routeData, startTripString, endTripString, pickRadiusFromStarting, dropRadiusFromEnding,
            routeRadius, showGender, distance, timestamp], function (err, resultFromInsertion) {
            if (err) {
                var response = constants.responseErrors.UNABLE_TO_CREATE_NEW_TRIP;
                res.jsonp(response);
            }
            else {
                var response = {"trip_id": resultFromInsertion.insertId};
                res.jsonp(response);
            }
        });
    } else {
        var sql = "INSERT INTO `trip`(`user_id`,`user_age`,`name`,`start_latitude`,`start_longitude`,`end_latitude`,`end_longitude`,`profile`,`min_age`,`max_age`,`route_id`"
        sql += ",`route_string`,`default`,`route_data`,`start_trip_string`,`end_trip_string`,`distance`,`created_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        connection.query(sql, [userId, useAge, tripName, startLatitude, startLongitude, endLatitude, endLongitude, profile, minAge, maxAge, routeId, routeString,
            defaultTrip, routeData, startTripString, endTripString, distance, timestamp], function (err, resultFromInsertion) {
            if (err) {
                console.log(err);
                var response = constants.responseErrors.UNABLE_TO_CREATE_NEW_TRIP;
                res.jsonp(response);
            }
            else {
                var response = {"trip_id": resultFromInsertion.insertId};
                res.jsonp(response);
                if (defaultTrip == constants.defaultValueSet.AT_SIGN_UP_TO_ONE) {
                    var sql = "UPDATE `user` SET `default_trip`=?,`default_trip_name`=?, `profile`=?  WHERE `user_id`=? LIMIT 1";
                    connection.query(sql, [resultFromInsertion.insertId, tripName, profile, userId], function (err, result) {
                    });
                }
            }
        });
    }
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * get All matches of user default trip
 * Input : access token
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getAllMatchesOfUserDefaultTrip = function (req, res) {
    var accessToken = req.body.accesstoken;
    var fbAccessToken = req.body.fbaccesstoken;
    var manValues = [accessToken];
    console.log(req.body);
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["fb_id"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getAllMatchesOfUserDefaultTripResult(resultsFromAccessToken, fbAccessToken, res, req);
        });
    });
};
function getAllMatchesOfUserDefaultTripResult(resultsFromAccessToken, fbAccessToken, res, req) {
    var userId = resultsFromAccessToken[0].user_id;
    var defaultValue = constants.defaultUserSettingValue.ENABLE;
    var sql = "SELECT t.id,t.user_id,t.start_latitude,t.start_longitude,t.end_latitude,t.end_longitude,t.profile,t.min_age,t.max_age,t.route_id,t.route_string," +
        "t.pick_radius_from_starting,t.drop_radius_from_ending,t.pick_radius_from_route,t.route_data,t.show_gender FROM trip t INNER JOIN user u" +
        " ON t.user_id = u.user_id WHERE t.user_id=? && t.default=? && u.new_matches=? LIMIT 1";
    connection.query(sql, [userId, 1, defaultValue], function (err, responseFromUserTrip) {
        if (err) {
            var response = constants.responseErrors.FETCHING_DATA;
            res.jsonp(response);
        }
        else {
            if (responseFromUserTrip.length > 0) {
                var userPreferenceMinAge = responseFromUserTrip[0].min_age;
                var userPreferenceMaxAge = responseFromUserTrip[0].max_age;
                var userStartingLatitude = responseFromUserTrip[0].start_latitude;
                var userStartingLongitude = responseFromUserTrip[0].start_longitude;
                var userEndingLatitude = responseFromUserTrip[0].end_latitude;
                var userEndingLongitude = responseFromUserTrip[0].end_longitude;
                var userPickRadius = responseFromUserTrip[0].pick_radius_from_starting;
                var userDropRadius = responseFromUserTrip[0].drop_radius_from_ending;
                var userRouteRadius = responseFromUserTrip[0].pick_radius_from_route;
                var userRouteData = responseFromUserTrip[0].route_data;
                var userProfile = responseFromUserTrip[0].profile;
                var userShowGender = responseFromUserTrip[0].show_gender;
                var sql = "SELECT * FROM `hytched_user` WHERE `trip_id`=? && `user_id`=?";
                connection.query(sql, [responseFromUserTrip[0].id, resultsFromAccessToken[0].user_id], function (err, resultsFromHytchedUsers) {
                    var hytchedUsers = resultsFromHytchedUsers.length;
                    var hytchedArray = new Array();
                    var mutualHytchedArray = new Array();
                    if (hytchedUsers > 0) {
                        for (var i = 0; i < hytchedUsers; i++) {
                            hytchedArray.push(resultsFromHytchedUsers[i].hytched_id);
                        }
                    }
                    var sql = "SELECT * FROM `mutual_hytched_user` WHERE (`trip_id`=? && `user_id`=?)";
                    connection.query(sql, [responseFromUserTrip[0].id, resultsFromAccessToken[0].user_id, responseFromUserTrip[0].id,
                        resultsFromAccessToken[0].user_id], function (err, resultsFromMutualHytchedUsers) {
                        var mutaulHytchedUsers = resultsFromMutualHytchedUsers.length;
                        if (mutaulHytchedUsers > 0) {
                            for (var i = 0; i < mutaulHytchedUsers; i++) {
                                hytchedArray.push(resultsFromMutualHytchedUsers[i].hytched_id);
                            }
                        }
                        var sql = "SELECT `dytched_id` FROM `dytched_user` WHERE `trip_id`=? && `user_id`=?";
                        connection.query(sql, [responseFromUserTrip[0].id, resultsFromAccessToken[0].user_id], function (err, resultsFromDytchedUsers) {
                            var dytchedUsers = resultsFromDytchedUsers.length;
                            var dytchedArray = new Array();
                            if (dytchedUsers > 0) {
                                for (var i = 0; i < dytchedUsers; i++) {
                                    dytchedArray.push(resultsFromDytchedUsers[i].dytched_id);
                                }
                            }
                            var sql = "SELECT `blocked_id` FROM `blocked_user` WHERE `user_id`=?";
                            connection.query(sql, [resultsFromAccessToken[0].user_id], function (err, resultsFromBlockedUsers) {
                                var blockedUsers = resultsFromBlockedUsers.length;
                                var blockedArray = new Array();
                                if (blockedUsers > 0) {
                                    for (var i = 0; i < blockedUsers; i++) {
                                        blockedArray.push(resultsFromBlockedUsers[i].blocked_id);
                                    }
                                }
                            var mergedArray = hytchedArray.concat(dytchedArray);
                            var finalMergedArray =  mergedArray.concat(blockedArray);
                                finalMergedArray.push(resultsFromAccessToken[0].user_id);
                                finalMergedArray.filter(Boolean);
                            var str = finalMergedArray.toString(',');
                            if (str.length == 0) {
                                str = '0';
                            }
                            if (userProfile == constants.userProfileType.RIDER) {
                                var opponentProfile = constants.userProfileType.DRIVER + ',' + constants.userProfileType.BOTH;
                            }
                            else if (userProfile == constants.userProfileType.DRIVER) {
                                var opponentProfile = constants.userProfileType.RIDER + ',' + constants.userProfileType.BOTH;
                            }
                            else {
                                var opponentProfile = constants.userProfileType.RIDER + ',' + constants.userProfileType.DRIVER + ',' + constants.userProfileType.BOTH;
                            }
                            if (userShowGender == 'BOTH') {
                                var sql = "SELECT u.fb_id,u.first_name,CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image) END as image";
                                sql += ",u.gender,u.about,t.user_id,t.id,t.start_latitude,t.start_longitude,t.end_latitude,t.end_longitude,t.profile,t.min_age,t.max_age,t.route_id,";
                                sql += "t.route_string,t.route_data,t.user_age FROM  trip t INNER JOIN user u ON t.user_id = u.user_id WHERE t.user_id NOT IN (" + str + ")";
                                sql += " && t.profile IN (" + opponentProfile + ") && `default`=?  && `is_deleted`=? &&";
                                sql += " ( t.user_age BETWEEN " + userPreferenceMinAge + " AND " + userPreferenceMaxAge + " || t.user_age = ? )";
                            }
                            else {
                                var sql = "SELECT u.fb_id,u.first_name,CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image) END as image,";
                                sql += "u.gender,u.about,t.user_id,t.id,t.start_latitude,t.start_longitude,t.end_latitude,t.end_longitude,t.profile,t.min_age,t.max_age,t.route_id,";
                                sql += "t.route_string,t.route_data,t.user_age FROM  trip t INNER JOIN user u ON t.user_id = u.user_id WHERE t.user_id NOT IN (" + str + ") && ";
                                sql += "t.profile IN (" + opponentProfile + ") && `default`=?  && u.is_deleted=?  && u.gender= '" + userShowGender + "' && ";
                                sql += "( t.user_age BETWEEN " + userPreferenceMinAge + " AND " + userPreferenceMaxAge + " || t.user_age = ? ) ";
                            }
                            connection.query(sql, [constants.defaultUserSettingValue.ENABLE, constants.defaultUserSettingValue.DISABLE, constants.defaultUserSettingValue.DISABLE], function (err, responseFromOtherTrip) {
                                var tripUserCount = responseFromOtherTrip.length;
                                if (tripUserCount > 0) {
                                    var filteredArray = [];
                                    var flag = 0;
                                    var userIdArray = new Array();
                                    var pickRadiusArray = new Array();
                                    var userArray = new Array();
                                    var tripArray = new Array();
                                    var nameArray = new Array();
                                    var imageArray = new Array();
                                    var fbIdArray = new Array();
                                    var genderArray = new Array();
                                    var ageArray = new Array();
                                    var profileArray = new Array();
                                    var aboutArray = new Array();
                                    //Alternate
                                    var alternateUserArray = new Array();
                                    var alternateTripArray = new Array();
                                    var alternateNameArray = new Array();
                                    var alternateImageArray = new Array();
                                    var alternateFbIdArray = new Array();
                                    var startLatArray = new Array();
                                    var startLongArray = new Array();
                                    var alternateRouteData = new Array();
                                    var alternateGenderArray = new Array();
                                    var alternateAgeArray = new Array();
                                    var alternateProfileArray = new Array();
                                    var alternateAboutArray = new Array();
                                    userIdArray.push(resultsFromAccessToken[0].user_id);
                                    if ((userStartingLatitude != constants.defaultUserLocationValue.LATITUDE && userStartingLongitude != constants.defaultUserLocationValue.LONGITUDE) &&
                                        (userEndingLatitude == constants.defaultUserLocationValue.LATITUDE && userEndingLongitude == constants.defaultUserLocationValue.LONGITUDE)) {
                                        for (var i = 0; i < tripUserCount; i++) {
                                            (function (i) {
                                                var startLat = responseFromOtherTrip[i].start_latitude;
                                                var startLong = responseFromOtherTrip[i].start_longitude;
                                                var pickRadius = func.calculateDistanceBetweenPath(userStartingLatitude, userStartingLongitude, startLat, startLong);
                                                if ((pickRadius >= 0 && pickRadius <= userPickRadius)) {
                                                    fbIdArray.push(responseFromOtherTrip[i].fbid);
                                                    userArray.push(responseFromOtherTrip[i].user_id);
                                                    tripArray.push(responseFromOtherTrip[i].id);
                                                    nameArray.push(responseFromOtherTrip[i].first_name);
                                                    imageArray.push(responseFromOtherTrip[i].image);
                                                    pickRadiusArray.push(pickRadius);
                                                    genderArray.push(responseFromOtherTrip[i].gender);
                                                    ageArray.push(responseFromOtherTrip[i].user_age);
                                                    profileArray.push(responseFromOtherTrip[i].profile);
                                                    aboutArray.push(responseFromOtherTrip[i].about);
                                                }
                                            })(i)
                                        }
                                    }
                                    else if ((userStartingLatitude == constants.defaultUserSettingValue.DISABLE && userStartingLongitude == constants.defaultUserSettingValue.DISABLE) &&
                                        (userEndingLatitude != constants.defaultUserLocationValue.LATITUDE && userEndingLongitude != constants.defaultUserLocationValue.LONGITUDE)) {
                                        for (var i = 0; i < tripUserCount; i++) {
                                            (function (i) {
                                                var endLat = responseFromOtherTrip[i].end_latitude;
                                                var endLong = responseFromOtherTrip[i].end_longitude
                                                var dropRadius = func.calculateDistanceBetweenPath(userEndingLatitude, userEndingLongitude, endLat, endLong);
                                                if ((dropRadius >= 0 && dropRadius <= userDropRadius)) {
                                                    fbIdArray.push(responseFromOtherTrip[i].fbid);
                                                    userArray.push(responseFromOtherTrip[i].user_id);
                                                    tripArray.push(responseFromOtherTrip[i].id);
                                                    nameArray.push(responseFromOtherTrip[i].first_name);
                                                    imageArray.push(responseFromOtherTrip[i].image);
                                                    pickRadiusArray.push(dropRadius);
                                                    genderArray.push(responseFromOtherTrip[i].gender);
                                                    ageArray.push(responseFromOtherTrip[i].user_age);
                                                    profileArray.push(responseFromOtherTrip[i].profile);
                                                    aboutArray.push(responseFromOtherTrip[i].about);
                                                }
                                            })(i)
                                        }
                                    }
                                    else {
                                        for (var i = 0; i < tripUserCount; i++) {
                                            (function (i) {
                                                var startLat = responseFromOtherTrip[i].start_latitude;
                                                var startLong = responseFromOtherTrip[i].start_longitude;
                                                var endLat = responseFromOtherTrip[i].end_latitude;
                                                var endLong = responseFromOtherTrip[i].end_longitude
                                                var pickRadius = func.calculateDistanceBetweenPath(userStartingLatitude, userStartingLongitude, startLat, startLong);
                                                var dropRadius = func.calculateDistanceBetweenPath(userEndingLatitude, userEndingLongitude, endLat, endLong);
//                                                console.log(userStartingLatitude + '  ' + userStartingLongitude + '  ' + startLat + '  ' + startLong);
//                                                console.log(userEndingLatitude + '  ' + userEndingLongitude + '  ' + endLat + '  ' + endLong);
//                                                console.log(dropRadius);
//                                                console.log(userDropRadius);
//                                                console.log(pickRadius);
//                                                console.log(userPickRadius);
                                                if ((dropRadius >= 0 && dropRadius <= userDropRadius)) {
                                                    if ((pickRadius >= 0 && pickRadius <= userPickRadius)) {
                                                        fbIdArray.push(responseFromOtherTrip[i].fb_id);
                                                        userArray.push(responseFromOtherTrip[i].user_id);
                                                        tripArray.push(responseFromOtherTrip[i].id);
                                                        nameArray.push(responseFromOtherTrip[i].first_name);
                                                        imageArray.push(responseFromOtherTrip[i].image);
                                                        pickRadiusArray.push(pickRadius);
                                                        genderArray.push(responseFromOtherTrip[i].gender);
                                                        ageArray.push(responseFromOtherTrip[i].user_age);
                                                        profileArray.push(responseFromOtherTrip[i].profile);
                                                        aboutArray.push(responseFromOtherTrip[i].about);
                                                    }
                                                    else {
                                                        alternateFbIdArray.push(responseFromOtherTrip[i].fbid);
                                                        alternateUserArray.push(responseFromOtherTrip[i].user_id);
                                                        alternateTripArray.push(responseFromOtherTrip[i].id);
                                                        alternateNameArray.push(responseFromOtherTrip[i].first_name);
                                                        alternateImageArray.push(responseFromOtherTrip[i].image);
                                                        startLatArray.push(responseFromOtherTrip[i].start_latitude);
                                                        startLongArray.push(responseFromOtherTrip[i].start_longitude);
                                                        alternateRouteData.push(responseFromOtherTrip[i].route_data);
                                                        alternateGenderArray.push(responseFromOtherTrip[i].gender);
                                                        alternateAgeArray.push(responseFromOtherTrip[i].user_age);
                                                        alternateProfileArray.push(responseFromOtherTrip[i].profile);
                                                        alternateAboutArray.push(responseFromOtherTrip[i].about);
                                                    }
                                                }
                                            })(i)
                                        }
                                        //Fetching route lie as starting path
                                        var alternateLength = alternateFbIdArray.length;
                                        for (var p = 0; p < alternateLength; p++) {
                                            (function (p) {
                                                var userRouteLatLongArray = alternateRouteData[p].split('&');
                                                var userRouteLatLongArrayLength = userRouteLatLongArray.length;
                                                for (var k = 0; k < userRouteLatLongArrayLength; k++) {
                                                    var userSplitedArray = userRouteLatLongArray[k].split(',');
                                                    var latitude = userSplitedArray[0];
                                                    var longitude = userSplitedArray[1];
                                                    var distance = calculateDistance(userStartingLatitude, userStartingLongitude, latitude, longitude);
                                                    if (distance >= 0 && distance <= userPickRadius) {
                                                        fbIdArray.push(alternateFbIdArray[p]);
                                                        userArray.push(alternateUserArray[p]);
                                                        tripArray.push(alternateTripArray[p]);
                                                        nameArray.push(alternateNameArray[p]);
                                                        imageArray.push(alternateImageArray[p]);
                                                        genderArray.push(alternateGenderArray[p]);
                                                        pickRadiusArray.push(0);
                                                        ageArray.push(alternateAgeArray[p]);
                                                        profileArray.push(alternateProfileArray[p]);
                                                        aboutArray.push(alternateAboutArray[p]);
                                                    }
                                                }
                                            })(p);
                                        }
                                        var routeLatLongArray = userRouteData.split('&');
                                        var routeLatLongArrayLength = routeLatLongArray.length;
                                        for (var m = 0; m < alternateLength; m++) {
                                            (function (m) {
                                                for (var s = 0; s < routeLatLongArrayLength; s++) {
                                                    var splitedArray = routeLatLongArray[s].split(',');
                                                    var latitude = splitedArray[0];
                                                    var longitude = splitedArray[1];
                                                    var distance = calculateDistance(startLatArray[m], startLongArray[m], latitude, longitude);
                                                    if (distance >= 0 && distance <= userRouteRadius) {
                                                        fbIdArray.push(alternateFbIdArray[m]);
                                                        userArray.push(alternateUserArray[m]);
                                                        tripArray.push(alternateTripArray[m]);
                                                        nameArray.push(alternateNameArray[m]);
                                                        imageArray.push(alternateImageArray[m]);
                                                        genderArray.push(alternateGenderArray[m])
                                                        pickRadiusArray.push(0);
                                                        ageArray.push(alternateAgeArray[m]);
                                                        profileArray.push(alternateProfileArray[m]);
                                                        aboutArray.push(alternateAboutArray[m]);
                                                    }
                                                }
                                            })(m);
                                        }
                                    }
                                    var filterUser = fbIdArray.length
                                    if (filterUser > 0) {
                                        var string = userArray.toString(',');
                                        var sql = "SELECT `id`, CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image)  END as image," +
                                            "`user_id` FROM `user_image` WHERE `user_id` IN (" + string + ") ";
                                        connection.query(sql, function (err, resultsFromImages) {
                                            var userImagesLength = resultsFromImages.length;
                                            for (var k = 0; k < filterUser; k++) {
                                                (function (k) {
                                                    var imagesArray = [];
                                                    var urls = "https://graph.facebook.com/v2.1/" + fbIdArray[k] + "?fields=context&access_token=" + fbAccessToken + "";
                                                    console.log(urls);
                                                    request(urls, function (error, response, body) {
                                                        if (!error && response.statusCode === constants.errorResponseCode.WHILE_FACEBOOK_AUTHENTICATION) {
                                                            var output = JSON.parse(body);
                                                            var mutualFriendsArray = [];
                                                            var mutualLikesArray = [];
                                                            if (output['context']['mutual_likes']) {
                                                                var fbLikesLength = (output['context']['mutual_likes']['data']).length;
                                                                for (var j = 0; j < fbLikesLength; j++) {
                                                                    mutualLikesArray.push({"id": output['context']['mutual_likes']['data'][j].id,
                                                                        "name": output['context']['mutual_likes']['data'][j].name,
                                                                        "image": "http://graph.facebook.com/" + output['context']['mutual_likes']['data'][j].id + "/picture?width=225&height=225"});
                                                                }
                                                            }
                                                            else {
                                                                mutualLikesArray = [];
                                                            }
                                                            if (output['context']['mutual_friends']) {
                                                                var fbFriendLength = (output['context']['mutual_friends']['data']).length;
                                                                for (var l = 0; l < fbFriendLength; l++) {
                                                                    mutualFriendsArray.push({"id": output['context']['mutual_friends']['data'][l].id,
                                                                        "name": output['context']['mutual_friends']['data'][l].name,
                                                                        "image": "http://graph.facebook.com/" + output['context']['mutual_friends']['data'][l].id + "/picture?width=225&height=225"});
                                                                }
                                                            }
                                                            else {
                                                                mutualFriendsArray = [];
                                                            }
                                                            for (var s = 0; s < userImagesLength; s++) {
                                                                if (userArray[k] == resultsFromImages[s].user_id) {
                                                                    imagesArray.push({"id": resultsFromImages[s].id, "image": resultsFromImages[s].image})
                                                                }
                                                            }
                                                            if (profileArray[k] == constants.userProfileType.DRIVER) {
                                                                var userProfile = 'Driver'
                                                            }
                                                            else if (profileArray[k] == constants.userProfileType.RIDER) {
                                                                var userProfile = 'Rider'
                                                            }
                                                            else if (profileArray[k] == constants.userProfileType.BOTH) {
                                                                var userProfile = 'Both'
                                                            }
                                                            filteredArray.push({"user_id": userArray[k], "name": nameArray[k], "image": imageArray[k],
                                                                "gender": genderArray[k], "trip_id": tripArray[k], "miles": pickRadiusArray[k],
                                                                "mutual_friends": mutualFriendsArray, "mutual_likes": mutualLikesArray,
                                                                "image_array": imagesArray, "age": ageArray[k], "profile": userProfile, "about": aboutArray[k]});
                                                        }
                                                        ++flag;
                                                        if (flag == filterUser) {
                                                            var filterLength = filteredArray.length
                                                            var unique = {};
                                                            var distinct = [];
                                                            for (var i = 0; i < filterLength; i++) {
                                                                if (typeof(unique[filteredArray[i].user_id]) == "undefined") {
                                                                    distinct.push(filteredArray[i]);
                                                                }
                                                                unique[filteredArray[i].user_id] = 0;
                                                            }
                                                            var response = {"data": distinct};
                                                            res.jsonp(response);
                                                        }
                                                    });
                                                })(k);
                                            }
                                        });
                                    }
                                    else {
                                        var response = {"data": filteredArray};
                                        res.jsonp(response);
                                    }
                                }
                                else {
                                    var response = {"data": []};
                                    res.jsonp(response);
                                }
                            });
                            });
                        });
                    });
                });
            }
            else {
                var response = {"data": []};
                res.jsonp(response);
            }
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Hytch or dytch user based on matches
 * Input : acccestoken
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.hytchOrDytchUserBasedOnUserMatches = function (req, res) {
    var accessToken = req.body.accesstoken;
    var tripId = req.body.tripid;
    var userTripId = req.body.usertripid;
    var userId = req.body.userid;
    var userName = req.body.username;
    var type = parseInt(req.body.type);
    console.log(req.body);
    var manValues = [accessToken, tripId, userId];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["CASE WHEN `image` LIKE 'http%' THEN `image` ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',`image`) END as image"
            , "first_name", "total_hytches", "total_dytches", "total_matches"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            hytchOrDytchUserBasedOnUserMatchesResult(resultsFromAccessToken, tripId, userTripId, userId, userName, type, res, req);
        });
    });
};
function hytchOrDytchUserBasedOnUserMatchesResult(resultsFromAccessToken, tripId, userTripId, userId, userName, type, res, req) {
    var timestamp = new Date();
    if (type === constants.userHytchOrDytchStaus.HYTCH) {
        var sql = "SELECT h.user_id, u.first_name, CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image) END as image " +
            ",u.ios_device_token, u.android_device_token, u.push_notifications FROM hytched_user h INNER JOIN user u ON h.user_id = u.user_id WHERE `trip_id`=? && `hytched_id`=? LIMIT 1";
        connection.query(sql, [ tripId, resultsFromAccessToken[0].user_id], function (err, resultsFromHytchedUsers) {
            if (resultsFromHytchedUsers.length > 0) {
                var ratingUserArray = new Array();
                ratingUserArray.push(resultsFromAccessToken[0].user_id)
                ratingUserArray.push(resultsFromHytchedUsers[0].user_id);
                var str = ratingUserArray.toString(',');
                GetConversationDataBetweenTwoPersons(resultsFromAccessToken[0].user_id, resultsFromHytchedUsers[0].user_id, function (responseFromChatId) {
                    var sql = "SELECT AVG(rate) AS rating, rated_id FROM user_rating WHERE rated_id IN  (" + str + ") GROUP BY rated_id ORDER BY FIELD(rated_id," + str + ")";
                    connection.query(sql, function (err, resultsFromRatings) {
                        var sql = "SELECT  `rate`,`rated_id` FROM `user_rating` WHERE  `user_id` =? && `rated_id`=? LIMIT 1";
                        connection.query(sql, [resultsFromAccessToken[0].user_id, resultsFromHytchedUsers[0].user_id], function (err, resultsFromUserRatings) {
                            if (resultsFromUserRatings.length > 0) {
                                var individualRate = resultsFromUserRatings[0].rate;
                            }
                            else {
                                individualRate = 0;
                            }
                            var rateLength = resultsFromRatings.length;
                            var opponentRatedFlag = 0;
                            var userRatedFlag = 0;
                            for (var l = 0; l < rateLength; l++) {
                                if (resultsFromRatings[l].rated_id == resultsFromAccessToken[0].user_id) {
                                    var userRating = resultsFromRatings[l].rating
                                    userRatedFlag = 1;
                                }
                                else {
                                    var opponentRating = resultsFromRatings[l].rating;
                                    var opponentImage = resultsFromHytchedUsers[0].image;
                                    var opponentName = resultsFromHytchedUsers[0].first_name;
                                    var opponentId = resultsFromRatings[l].rated_id;
                                    opponentRatedFlag = 1;
                                }
                            }
                            if (userRatedFlag == 0) {
                                userRating = 0;
                            }
                            if (opponentRatedFlag == 0) {
                                var opponentRating = '0';
                                var opponentImage = resultsFromHytchedUsers[0].image;
                                var opponentName = resultsFromHytchedUsers[0].first_name;
                                var opponentId = resultsFromHytchedUsers[0].user_id;
                                var response = {"status": 1, "user_id": resultsFromAccessToken[0].user_id, "user_name": resultsFromAccessToken[0].first_name,
                                    "user_image": resultsFromAccessToken[0].image, "user_rating": userRating, "opponent_id": opponentId, "opponent_name": opponentName,
                                    "opponent_image": opponentImage, "opponent_rating": opponentRating,
                                    "individual_rate": individualRate, "chat_id": responseFromChatId};
                                res.jsonp(response);
                            }
                            else {
                                var response = {"status": 1, "user_id": resultsFromAccessToken[0].user_id, "user_name": resultsFromAccessToken[0].first_name,
                                    "user_image": resultsFromAccessToken[0].image, "user_rating": userRating, "opponent_id": opponentId,
                                    "opponent_name": opponentName, "opponent_image": opponentImage, "opponent_rating": opponentRating,
                                    "individual_rate": individualRate, "chat_id": responseFromChatId};
                                res.jsonp(response);
                            }
                            var tripArray = [tripId, userTripId];
                            var opponentTripArray = [userTripId, tripId];
                            var userArray = [userId, resultsFromAccessToken[0].user_id];
                            var usernameArray = [userName, resultsFromAccessToken[0].first_name];
                            var hytchedNameArray = [resultsFromAccessToken[0].first_name, userName]
                            var hytchedArray = [resultsFromAccessToken[0].user_id, userId];
                            var jsonData = '';
                            for (var i = 0; i < 2; i++) {
                                jsonData += "  (" + tripArray[i] + "," + opponentTripArray[i] + "," + userArray[i] + ",'" + usernameArray[i] + "','" + hytchedNameArray[i] + "','" + hytchedArray[i] + "', NOW() " + "),";
                            }
                            jsonData = jsonData.substring(0, jsonData.length - 1);
                            //
                            async.series([
                                function (callback) {
                                    var sql = "INSERT INTO `mutual_hytched_user`(`trip_id`,`opponent_trip_id`,`user_id`,`user_name`,`hytched_name`,`hytched_id`,`created_at`)  VALUES  " + jsonData + "";
                                    connection.query(sql, function (err, resultFromMutulHytched) {
                                        if (err) return callback(err);
                                        callback();
                                    });
                                },
                                function (callback) {
                                    var sql = "INSERT INTO `hytched_user`(`trip_id`,`user_id`,`hytched_id`,`user_name`,`hytched_name`,`opponent_trip_id`,`created_at`) VALUES (?,?,?,?,?,?,?)";
                                    connection.query(sql, [userTripId, resultsFromAccessToken[0].user_id, userId, resultsFromAccessToken[0].first_name, userName, tripId, timestamp], function (err, resultfromInsertion) {
                                        if (err) return callback(err);
                                        callback();
                                    });
                                },
                                function (callback) {
                                    var totalHytches = resultsFromAccessToken[0].total_hytches + 1;
                                    var totalMatches = resultsFromAccessToken[0].total_matches + 1;
                                    var sql = "UPDATE `user` SET `total_hytches`=?, `total_matches`=? WHERE `user_id` =? LIMIT 1";
                                    connection.query(sql, [totalHytches, totalMatches, resultsFromAccessToken[0].user_id ], function (err, result) {
                                        if (err) return callback(err);
                                        callback();
                                    });
                                }], function (err, callbackResponse) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    var androidTokenArray = new Array();
                                    var iosTokenArray = new Array();
                                    if (resultsFromHytchedUsers[0].android_device_token != '') {
                                        androidTokenArray.push(resultsFromHytchedUsers[0].android_device_token);
                                    }
                                    if (resultsFromHytchedUsers[0].ios_device_token != '' || resultsFromHytchedUsers[0].ios_device_token != '(null)') {
                                        iosTokenArray.push(resultsFromHytchedUsers[0].ios_device_token);
                                    }
                                    var iosUnread = [];
                                    var sql = "SELECT  `rate`,`rated_id` FROM `user_rating` WHERE  `user_id` =? && `rated_id`=? LIMIT 1";
                                    connection.query(sql, [resultsFromAccessToken[0].user_id, userId], function (err, resultsFromUserRatings) {
                                        if (resultsFromUserRatings.length > 0) {
                                            var opponentRating = resultsFromUserRatings[0].rate;
                                        }
                                        else {
                                            var opponentRating = 0;
                                        }

                                        func.verifyUserForBlocking(userId, resultsFromAccessToken[0].user_id, function (err, response) {
                                            if (err) {
                                            }
                                            else {
                                    var message = constants.pushMessageToUser.FOR_MUTAL_HYTCH + resultsFromAccessToken[0].first_name
                                    var androidMessage = ({"chat": message, "sender_id": resultsFromAccessToken[0].user_id, "sender_name": resultsFromAccessToken[0].first_name,
                                        "sender_image": resultsFromAccessToken[0].image, "type": constants.pushNotificationsType.MUTUAL_HYTCH, "badge": 1, "opponent_rating": opponentRating, "chat_id": responseFromChatId})
                                    var iosSendMessage = ({"sender_id": resultsFromAccessToken[0].user_id, "sender_name": resultsFromAccessToken[0].first_name, "type": constants.pushNotificationsType.MUTUAL_HYTCH,
                                        "sender_image": resultsFromAccessToken[0].image, "opponent_rating": opponentRating, "chat_id": responseFromChatId})
                                    if ((androidTokenArray.length > 0) && (resultsFromHytchedUsers[0].push_notifications == 1)) {
                                        func.androidPushNotification(androidTokenArray, androidMessage);
                                    }
                                    if ((iosTokenArray.length > 0) && (resultsFromHytchedUsers[0].push_notifications == 1)) {
                                        iosUnread.push(1);
                                        func.iosPushNotification(iosTokenArray, message, iosSendMessage, iosUnread);
                                    }
                                            }
                                        });
                                    });
                                }

                            });
                            //

                        });
                    });
                });
            }
            else {
                var sql = "INSERT INTO `hytched_user`(`trip_id`,`user_id`,`user_name`,`hytched_name`,`hytched_id`,`opponent_trip_id`,`created_at`) VALUES (?,?,?,?,?,?,?)";
                connection.query(sql, [userTripId, resultsFromAccessToken[0].user_id, resultsFromAccessToken[0].first_name, userName, userId, tripId, timestamp],
                    function (err, resultFromInsertion) {
                        console.log(err);
                        if (!err) {
                            var response = constants.responseSuccess.USER_HYTCHED_SUCCESSFULLY;
                            res.jsonp(response);
                            var totalHytches = resultsFromAccessToken[0].total_hytches + 1;
                            var sql = "UPDATE `user` SET `total_hytches`=? WHERE `user_id` =? LIMIT 1";
                            connection.query(sql, [totalHytches, resultsFromAccessToken[0].user_id ], function (err, result) {
                            });
                        }
                    });
            }
        });
    }
    else if (type === constants.userHytchOrDytchStaus.DYTCH) {
        var sql = "INSERT INTO `dytched_user`(`trip_id`,`user_id`,`user_name`,`dytched_name`,`dytched_id`,`opponent_trip_id`,`created_at`) VALUES (?,?,?,?,?,?,?)";
        connection.query(sql, [userTripId, resultsFromAccessToken[0].user_id, resultsFromAccessToken[0].first_name, userName, userId, tripId, timestamp], function (err, resultFromInsertion) {
            if (!err) {
                var response = constants.responseSuccess.USER_DYTCHED_SUCCESSFULLY;
                res.jsonp(response);
                var totalDytches = resultsFromAccessToken[0].total_dytches + 1;
                var sql = "UPDATE `user` SET `total_dytches`=? WHERE `user_id` =? LIMIT 1";
                connection.query(sql, [totalDytches, resultsFromAccessToken[0].user_id], function (err, result) {
                });
            }
        });
    }
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Edit or delete trip
 * Input : access token, trip name, latitude, longitude,route id,route string, profile, age, trip id, type
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.editOrDeleteTripThroughAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    var tripName = req.body.tripname;
    var startLatitude = req.body.startlatitude;
    var startLongitude = req.body.startlongitude;
    var endLatitude = req.body.endlatitude;
    var endLongitude = req.body.endlongitude;
    var profile = req.body.profile;
    var routeId = req.body.routeid;
    var routeString = req.body.routestring;
    var minAge = req.body.minage;
    var maxAge = req.body.maxage;
    var routeData = req.body.routedata;
    var startTripString = req.body.starttripstring;
    var endTripString = req.body.endtripstring;
    var tripId = req.body.tripid;
    var pickRadiusFromStarting = req.body.pickradiusfromstarting;
    var dropRadiusFromEnding = req.body.dropradiusfromending;
    var routeRadius = req.body.routeradius;
    var showGender = req.body.showgender;
    var distance = req.body.distance;
    var type = parseInt(req.body.type);
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["age"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            editOrDeleteTripThroughAccessTokenResult(resultsFromAccessToken, tripName, startLatitude, startLongitude, endLatitude, endLongitude,
                profile, minAge, maxAge, routeId, routeString, routeData, tripId, type, startTripString, endTripString, pickRadiusFromStarting,
                dropRadiusFromEnding, routeRadius, showGender, distance, res, req);
        });
    });
};
function editOrDeleteTripThroughAccessTokenResult(resultsFromAccessToken, tripName, startLatitude, startLongitude, endLatitude, endLongitude, profile, minAge, maxAge, routeId, routeString, routeData, tripId, type, startTripString, endTripString, pickRadiusFromStarting, dropRadiusFromEnding, routeRadius, showGender, distance, res, req) {
    if (type === constants.tripManipulationStatus.EDIT) {
        var sql = "SELECT `id`,`start_latitude`,`start_longitude`,`end_latitude`,`end_longitude` FROM `trip` WHERE `user_id`=? && `id`=?  LIMIT 1";
        connection.query(sql, [resultsFromAccessToken[0].user_id, tripId], function (err, responseFromUserTrip) {
            var newStartLatitude = parseFloat(startLatitude).toFixed(2);
            console.log(newStartLatitude);
            if ((responseFromUserTrip[0].start_latitude != startLatitude) || (responseFromUserTrip[0].start_longitude != startLongitude) ||
                (responseFromUserTrip[0].end_latitude != endLatitude) || (responseFromUserTrip[0].end_longitude != endLongitude)) {
                var sql = "DELETE FROM `dytched_user` WHERE (`opponent_trip_id`=? || `trip_id`=?) LIMIT 1";
                connection.query(sql, [tripId, tripId], function (err, resultFromDeletion) {
                });
                var sql = "DELETE FROM `hytched_user` WHERE (`opponent_trip_id`=? || `trip_id`=?) LIMIT 1";
                connection.query(sql, [tripId, tripId], function (err, resultFromDeletion) {
                });
                var sql = "DELETE FROM `mutual_hytched_user` WHERE (`opponent_trip_id`=? || `trip_id`=?)";
                connection.query(sql, [tripId, tripId], function (err, resultFromDeletion) {
                });
            }
            var sql = "UPDATE `trip` SET `name`=?,`start_latitude`=?,`start_longitude`=?,`end_latitude`=?,`end_longitude`=?,`profile`=?,`min_age`=?,`max_age`=?,`route_id`=?,";
            sql += "`route_string`=?,`route_data`=?, `start_trip_string`=?, `end_trip_string`=? ,`pick_radius_from_starting`=?,`drop_radius_from_ending`=?,`pick_radius_from_route`=?,";
            sql += " `show_gender`=?, `distance`=? WHERE `id`=? LIMIT 1";
            connection.query(sql, [ tripName, startLatitude, startLongitude, endLatitude, endLongitude, profile, minAge, maxAge, routeId, routeString, routeData,
                startTripString, endTripString, pickRadiusFromStarting, dropRadiusFromEnding, routeRadius, showGender, distance, tripId], function (err, resultFromInsertion) {
                if (err) {
                    var response = constants.responseErrors.UPDATION_ERROR;
                    res.jsonp(response);
                }
                else {
                    var response = constants.responseSuccess.SETTINGS_CHANGED;
                    res.jsonp(response);
                    var sql = "UPDATE `user` SET  `profile`=?  WHERE `user_id`=? LIMIT 1";
                    connection.query(sql, [ profile, resultsFromAccessToken[0].user_id], function (err, result) {
                    });
                }
            });
        });
    }
    else if (type === constants.tripManipulationStatus.DELETE) {
        var sql = "DELETE FROM `trip` WHERE `id`=? LIMIT 1";
        connection.query(sql, [tripId], function (err, resultFromDeletion) {
            var response = constants.responseSuccess.SETTINGS_CHANGED;
            res.jsonp(response);
        });
    }
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get all user trips
 * Input : acccestoken
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getAllUserTripsThroughAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["age"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            getAllUserTripsThroughAccessTokenResult(resultsFromAccessToken, res, req);
        });
    });
};
function getAllUserTripsThroughAccessTokenResult(resultsFromAccessToken, res, req) {
    //
    var tripDetails = {};
    async.series([
        function (callback) {
            var sql = "SELECT `name` AS trip_name,`id`,`start_latitude`,`start_longitude`,`end_latitude`,`end_longitude`,`profile`,`min_age`,`max_age`,`route_id`,";
            sql += "`route_string`,`start_trip_string`,`end_trip_string`,`pick_radius_from_starting`,`drop_radius_from_ending`,`default`,`distance`,`pick_radius_from_route`,";
            sql += "`show_gender` FROM `trip` WHERE `user_id`=? ";
            connection.query(sql, [resultsFromAccessToken[0].user_id], function (err, responseFromUserTrip) {
                if (err) return callback(err);
                tripDetails.trip = responseFromUserTrip;
                callback();
            });
        },
        function (callback) {
            func.getUserRideHistoryThroughAccessToken(resultsFromAccessToken, function (err, responseFromUserRide) {
                if (err) return callback(err);
                tripDetails.ride = responseFromUserRide;
                callback();
            });
        }], function (err, callbackResponse) {
        if (err) {
            var response = constants.responseErrors.UPDATION_ERROR;
            res.jsonp(response);
        }
        else {
            res.jsonp(tripDetails);
        }
    });
    //
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Calculate distance between two lat longs
 * Input : lat longs
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function calculateDistance(lat1, long1, lat2, long2) {
    var distance = dist.getDistance(
        {latitude: lat1, longitude: long1},
        {latitude: lat2, longitude: long2}
    );
    distance = ((0.000621371) * distance);
    distance = Math.round(distance);
    return distance;
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get conversation data between two persons
 * Input : user id, opponent id
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function GetConversationDataBetweenTwoPersons(userId, opponentId, callback) {
    var timestamp = new Date();
    var sql = "SELECT `chat_id` FROM `conversation` WHERE (`sender_id`=? && `receiver_id`=?) || (`receiver_id`=? && `sender_id`=? ) LIMIT 1";
    connection.query(sql, [ userId, opponentId, userId, opponentId], function (err, resultsFromConversation) {
        if (resultsFromConversation.length == 0) {
            var sqlConversation = "INSERT INTO `conversation`(`sender_id`,`receiver_id`,`sender_last_message`,`receiver_last_message`,`created_at`) VALUES (?,?,?,?,?)";
            connection.query(sqlConversation, [userId, opponentId, constants.defaultMessage.FOR_CONVERSATION_LIST, constants.defaultMessage.FOR_CONVERSATION_LIST, timestamp], function (err, resultFromConversationInsert) {
                return callback(resultFromConversationInsert.insertId);
            });
        }
        else {
            return callback(resultsFromConversation[0].chat_id);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Authenticate trip name
 * Input : access token, trip name
 * Output : log or error message
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.authenticateTripNameFromAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    var tripName = req.body.tripname;
    console.log(req.body);
    var manValues = [accessToken, tripName];
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ["age"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            authenticateTripNameFromAccessTokenResult(resultsFromAccessToken, tripName, res, req);
        });
    });
};
function authenticateTripNameFromAccessTokenResult(resultsFromAccessToken, tripName, res, req) {
    var sql = "SELECT `id` FROM `trip` WHERE `user_id`=? && `name`=? LIMIT 1";
    connection.query(sql, [resultsFromAccessToken[0].user_id, tripName], function (err, responseFromUserTrip) {
        if (responseFromUserTrip.length > 0) {
            var response = constants.responseErrors.TRIP_NAME_ALREADY_EXIST;
            res.jsonp(response);
        }
        else {
            var response = constants.responseSuccess.TRIP_VERIFIED;
            res.jsonp(response);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Unique prototype
 * Input :
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
Array.prototype.unique = function () {
    var unique_array = [];
    for (var i = 0, l = this.length; i < l; i++) {
        if (unique_array.indexOf(this[i]) == -1) {
            unique_array.push(this[i]);
        }
    }
    return unique_array;
}