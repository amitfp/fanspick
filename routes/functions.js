var url = require('url');
var http = require('http');
var constants = require('./constants');
var gcm = require('node-gcm');
var dist = require('geolib');
var apns = require('apn');
var fs = require('fs');
var math = require('mathjs');
var AWS = require('aws-sdk');
var nodeMailer = require("nodemailer");
var async = require('async');
var smtpTransport = undefined;
var myContext = this;
/*
 * ------------------------------------------------------
 * Authenticate mandatory fields.
 * Input : Array of field names which need to be mandatory
 * Output : log or error message
 * ------------------------------------------------------
 */
exports.authenticateMandatoryFields = function (arr, res, callback) {
    var arrLength = arr.length;
    var flag = 0;
    for (var i = 0; i < arrLength; i++) {
        if (arr[i] == '') {
            var response = constants.responseErrors.MANDATORY_FIELDS
            res.jsonp(response);
            flag = 1;
            break;
        }
    }
    if (flag == 0) {
        return callback(1);
    }
};
/*
 * ------------------------------------------------------
 * Authenticate a user through Access token and return extra data
 * Input:Access token{Optional Extra data}
 * Output: User_id Or Json error{Optional Extra data}
 * ------------------------------------------------------
 */
exports.authenticateAccessTokenAndReturnExtraData = function (accesstoken, arr, res, callback) {
    var sql = "SELECT `user_id`";
    arr.forEach(function (entry) {
        sql += "," + entry;
    });
    sql += " FROM `user`";
    sql += " WHERE `access_token`=? && `is_deleted`=? LIMIT 1";
    connection.query(sql, [accesstoken, constants.userDeleteStatus.NOT_DELETED], function (err, result) {
        if (result.length > 0) {
            return callback(result);
        } else {
            var response = constants.responseErrors.INVALID_ACCESS;
            res.jsonp(response);
        }
    });
}

/*
 * ------------------------------------------------------
 * Authenticate a user through user id
 * Input: user id
 * Output:
 * ------------------------------------------------------
 */
exports.authenticateUser = function (userId, callback) {
    var sql = "SELECT `user_id`,`first_name`,`user_type`,`ios_device_token`,`android_device_token` FROM `user` WHERE `user_id`=? && is_deleted=? ";
    connection.query(sql, [userId, constants.userDeleteStatus.NOT_DELETED], function (err, responseFromAuthentication) {
        if (err) {
            var response = constants.responseSuccess.UPDATION_ERROR;
            return callback(response, null);
        }
        else {
            if(responseFromAuthentication.length > 0){
                if(responseFromAuthentication[0].user_type == constants.userProfileType.ANDROID){
                    if(responseFromAuthentication[0].android_device_token == ''){
                        return callback(responseFromAuthentication, null);
                    }
                }
                else{
                    if(responseFromAuthentication[0].ios_device_token == ''){
                        return callback(responseFromAuthentication, null);
                    }
                }
            return callback(null, responseFromAuthentication);
            }
            else{
                return callback(responseFromAuthentication, null);
            }
        }
    });
}

/*
 * ------------------------------------------------------
 * Verify user for blocking
 * Input: user id blocked id
 * Output:
 * ------------------------------------------------------
 */
exports.verifyUserForBlocking = function (userId, blockedId, callback) {
    var sql = "SELECT `id` FROM `blocked_user` WHERE `user_id`=? && `blocked_id`=? ";
    connection.query(sql, [userId, blockedId], function (err, responseFromAuthentication) {
        if (err) {
            var response = constants.responseSuccess.UPDATION_ERROR;
            return callback(response, null);
        }
        else {
            if(responseFromAuthentication.length == 0){
                return callback(null, responseFromAuthentication);
            }
            else{
                var response = constants.responseErrors.USER_BLOCKED;
                return callback(response, null);
            }
        }
    });
}
/*
 * ------------------------------------------------------
 * Authenticate promo code  and return extra data
 * Input:promocode{Optional Extra data}
 * Output: amount Or Json error{Optional Extra data}
 * ------------------------------------------------------
 */
exports.authenticatePromoAmountAndReturnExtraData = function (accesstoken, arr, res, callback) {
    var sql = "SELECT `id`";
    arr.forEach(function (entry) {
        sql += "," + entry;
    });
    sql += " FROM `promocode`";
    sql += " WHERE `name`=? && CURDATE() BETWEEN `valid_from` AND `valid_upto` LIMIT 1";
    connection.query(sql, [accesstoken, 0], function (err, result) {
        if (result.length > 0) {
            return callback(result);
        } else {
            var response = constants.responseErrors.INCORRECT_PROMO_CODE;
            res.jsonp(response);
        }
    });
}
/*
 * ------------------------------------------------------
 * Get all blocked user
 * Input: user id
 * Output:
 * ------------------------------------------------------
 */
exports.getAllBlockedUserThroughAccessTokenResult = function (userId, callback) {
    var sql = "SELECT `blocked_id` FROM `blocked_user` WHERE `user_id`=? ";
    connection.query(sql, [userId], function (err, responseFromBlockedUser) {
        if (err) {
            var response = constants.responseSuccess.UPDATION_ERROR;
            return callback(response, null);
        }
        else {
            return callback(null, responseFromBlockedUser);
        }
    });
}
/*
 * ------------------------------------------------------
 * Get user information through user id
 * Input: user id
 * Output:
 * ------------------------------------------------------
 */
exports.getUserInformationThroughUserId = function (userId, arr, callback) {
    var sql = "SELECT `user_id`";
    arr.forEach(function (entry) {
        sql += "," + entry;
    });
    sql += " FROM `user`";
    sql += " WHERE `user_id`=? LIMIT 1";
    connection.query(sql, [userId, 0], function (err, result) {
        if (err) {
            var response = constants.responseErrors.UPDATION_ERROR;
            return callback(response, null);
        }
        else {
            if (result.length > 0) {
                return callback(null, result);
            } else {
                return callback(null, []);
            }
        }
    });
}
/*
 * ------------------------------------------------------
 * Get user trip information
 * Input: user id
 * Output:
 * ------------------------------------------------------
 */
exports.getUserTripInformation = function (tripId, arr, callback) {
    var sql = "SELECT `user_id`";
    arr.forEach(function (entry) {
        sql += "," + entry;
    });
    sql += " FROM `trip`";
    sql += " WHERE `id`=?  LIMIT 1";
    connection.query(sql, [tripId], function (err, result) {
        if (err) {
            var response = constants.responseErrors.UPDATION_ERROR;
            return callback(response, null);
        }
        else {
            if (result.length > 0) {
                return callback(null, result);
            } else {
                var response = constants.responseErrors.INVALID_ACCESS;
                return callback(null, []);
            }
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Get user ride history
 * Input : access token
 * Output :
 * -----------------------------------------------------------------------------
 */
exports.getUserRideHistoryThroughAccessToken = function (resultsFromAccessToken, callback) {
    var sql = "SELECT `id`,`rider_id`,`driver_id`,`ride_start_location_string`,`ride_end_location_string`,`amount`,`distance`,`rider_rating`,`driver_rating`,";
    sql += " `ride_start_date_time`,`ride_end_date_time` FROM `user_ride`WHERE (`rider_id` = ? ||  `driver_id` =?) && (`is_ride_completed_by_rider` = ? ||  `is_ride_completed_by_driver` =?) ORDER BY `ride_end_date_time` DESC";
    connection.query(sql, [resultsFromAccessToken[0].user_id, resultsFromAccessToken[0].user_id, constants.rideCompletedByDriverOrRider.COMPLETED, constants.rideCompletedByDriverOrRider.COMPLETED],
        function (err, resultsFromUserRide) {
            if (err) {
                var response = constants.responseErrors.UPDATION_ERROR;
                return callback(response, null);
            }
            else {

                var rideIdArray = new Array();
                var opponentIdArray = new Array();
                var profileArray = new Array();
                var opponentRating = new Array();
                var tripHistory = [];
                var totalRides = resultsFromUserRide.length;
                if (totalRides > 0) {
                    for (var i = 0; i < totalRides; i++) {
                        rideIdArray.push(resultsFromUserRide[i].id);
                        if (resultsFromUserRide[i].rider_id == resultsFromAccessToken[0].user_id) {
                            opponentIdArray.push(resultsFromUserRide[i].driver_id);
                            profileArray.push(constants.userProfileType.RIDER);
                            opponentRating.push(resultsFromUserRide[i].driver_rating)
                        }
                        else {
                            opponentIdArray.push(resultsFromUserRide[i].rider_id);
                            profileArray.push(constants.userProfileType.DRIVER);
                            opponentRating.push(resultsFromUserRide[i].rider_rating);
                        }
                    }
                    var rideIdString = rideIdArray.toString(',');
                    var opponentIdString = opponentIdArray.toString(',');
                    var asyncResponse = [];
                    async.parallel([
                        function (callback) {
                            var sql = "SELECT c.card_number,c.card, p.ride_id  FROM user_card c INNER JOIN payment p";
                            sql += "  ON p.customer_id = c.customer_id WHERE ride_id IN (" + rideIdString + ") ORDER BY FIELD(`ride_id`," + rideIdString + ")";
                            connection.query(sql, [],
                                function (err, responseFromCard) {
                                    console.log(err);
                                    if (err) return callback(err);
                                    asyncResponse.card = responseFromCard;
                                    callback();
                                });
                        },
                        function (callback) {
                            var sql = "SELECT `user_id`,`first_name`,`last_name`,CASE WHEN image LIKE 'http%' THEN image ELSE ";
                            sql += " CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image) END as image FROM `user`";
                            sql += "  WHERE user_id IN(" + opponentIdString + ") ORDER BY FIELD(`user_id`," + opponentIdString + ")";
                            connection.query(sql, [],
                                function (err, resultFromUser) {
                                    console.log(err);
                                    if (err) return callback(err);
                                    asyncResponse.user = resultFromUser;
                                    callback();
                                });
                        }], function (err, callbackResponse) {
                        if (err) {
                            var response = constants.responseErrors.UPDATION_ERROR;
                            return callback(response, null);
                        }
                        else {
                            var cardLength = asyncResponse.card.length;
                            var opponentDetailsLength = asyncResponse.user.length;
                            var opponentUserLength = asyncResponse.user.length;
                            for (var i = 0; i < totalRides; i++) {
                                var isCardSet = 0;
                                for (var j = 0; j < cardLength; j++) {
                                    if (rideIdArray[i] == asyncResponse.card[j].ride_id) {
                                        var cardNumber = asyncResponse.card[j].card_number;
                                        var card = asyncResponse.card[j].card;
                                        isCardSet = 1;
                                        break;
                                    }
                                }
                                if (isCardSet == 0) {
                                    var cardNumber = '';
                                    var card = '';
                                }

                                for (var k = 0; k < opponentDetailsLength; k++) {
                                    if (opponentIdArray[i] == asyncResponse.user[k].user_id) {
                                        var userName = asyncResponse.user[k].first_name + ' ' + asyncResponse.user[k].last_name;
                                        var userImage = asyncResponse.user[k].image;
                                    }
                                }
                                tripHistory.push({"ride_start_location_string": resultsFromUserRide[i].ride_start_location_string,
                                    "ride_end_location_string": resultsFromUserRide[i].ride_end_location_string, "amount": resultsFromUserRide[i].amount,
                                    distance: resultsFromUserRide[i].distance, "card_number": cardNumber, card: card, "opponent_name": userName,
                                    user_image: userImage, opponent_rating: opponentRating[i], profile: profileArray[i],
                                    "ride_start_date_time": resultsFromUserRide[i].ride_start_date_time, "ride_end_date_time": resultsFromUserRide[i].ride_end_date_time
                                })
                            }
                            return callback(null, tripHistory);
                        }
                    });
                }
                else {
                    return callback(null, tripHistory);
                }
            }
        });
}
/*
 * -----------------------------------------------------------------------------
 * Get conversation details between two user
 * Input : access token
 * Output :
 * -----------------------------------------------------------------------------
 */
exports.getConversationDetailsBetweenTwoUser = function (userId, opponentId, callback) {
 conversationArray = [];
    async.series([
        function (callback) {
            var sql = "SELECT chat_id,sender_id,receiver_id FROM `conversation`";
            sql += "  WHERE ((sender_id = ? && receiver_id =?) || (sender_id = ? && receiver_id =?)) LIMIT 1 ";
            connection.query(sql, [userId, opponentId, opponentId, userId],
                function (err, resultsFromConversationUsers) {
                if (err) return callback(err);
                conversationArray.conversation = resultsFromConversationUsers;
                callback();
            });
        },
        function (callback) {
            var extraData = ["first_name",  "CASE WHEN `image` LIKE 'http%' THEN `image` ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',`image`) END as image"]
            myContext.getUserInformationThroughUserId(opponentId, extraData, function (err, resultFromUser) {
                console.log(err);
                if (err) return callback(err);
                conversationArray.user = resultFromUser;
                callback();
            });
        }], function (err, callbackResponse) {
        if (err) {
            var response = constants.responseErrors.UPDATION_ERROR;
            return callback(response, null);
        }
        else {
            var userName  = conversationArray.user[0].first_name
            var finalConversationResponse = {chat_id:  conversationArray.conversation[0].chat_id, sender_id: opponentId, sender_name: userName, sender_image: conversationArray.user[0].image}
            return callback(null, finalConversationResponse);
        }
    });
    //
}

/*
 * ------------------------------------------------------
 *  Get App Version
 *  Input :
 *  Output : update popup and critical
 * ------------------------------------------------------
 */

exports.getAppVersion = function (callback) {
    var sql = "SELECT `id`, `type`, `version`, `critical` FROM `app_version` LIMIT 2";
    connection.query(sql, [], function (err, responseFromQuery) {
        if (err) {
            var response = constants.responseErrors.VERSION_ERROR;
            return callback(response, null);
        }
        else if (responseFromQuery.length > 0) {
            return callback(null, responseFromQuery);
        }
        else{
            var response = constants.responseErrors.VERSION_ERROR;
            return callback(response, null);
        }
    });
};
/*
 * ------------------------------------------------------
 * Calculate distance between two paths
 * Input: start lat, start long, end lat ,end long
 * Output: distance
 * ------------------------------------------------------
 */
exports.calculateDistanceBetweenPath = function calculateDistance(lat1, long1, lat2, long2) {
    var distance = dist.getDistance(
        {latitude: lat1, longitude: long1},
        {latitude: lat2, longitude: long2}
    );
    distance = ((0.000621371) * distance);
    distance = Math.round(distance);
    return distance;
}
//
/* -----------------------------------------------------------------------------
 * sorting an array in ascending order
 * INPUT : array and key according to which sorting is to be done
 * OUTPUT : sorted array
 * -----------------------------------------------------------------------------
 */
exports.sortByKeyAsc = function (array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
};
/*
 * -----------------------------------------------------------------------------
 * sorting an array in descending order
 * INPUT : array and key according to which sorting is to be done
 * OUTPUT : sorted array
 * -----------------------------------------------------------------------------
 */
exports.sortByKeyDesc = function (array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x >= y) ? -1 : ((x < y) ? 1 : 0));
    });
};
/* -----------------------------------------------------------------------------
 * Encryption code
 * INPUT : string
 * OUTPUT : crypted string
 * -----------------------------------------------------------------------------
 */
exports.encrypt = function (text) {
    var crypto = require('crypto');
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}
/* -----------------------------------------------------------------------------
 * Send mail to user as forgot password
 * Input : email
 * Output :
 * -----------------------------------------------------------------------------
 */
exports.sendEmail = function (receiverMailId, message, subject, callback) {
    var smtpTransport = nodeMailer.createTransport("SMTP", config.get('mailSettings'));
// setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('appSettings.name'), // sender address
        to: receiverMailId, // list of receivers
        subject: subject, // Subject line
        text: message, // plaintext body
        html: message // html body
    }
// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (err, response) {
        if (err) {
            console.log(err);
            return callback(err, null);
        } else {
            return callback(null, 1);
        }
    });
};
/* -----------------------------------------------------------------------------
 * Get random string
 * Input :
 * Output : random string
 * -----------------------------------------------------------------------------
 */
exports.generateRandomString = function () {
    var math = require('mathjs');
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 6; i++)
        text += possible.charAt(math.floor(math.random() * possible.length));
    return text;
};
/* -----------------------------------------------------------------------------
 * Get random string
 * Input :
 * Output : random string
 * -----------------------------------------------------------------------------
 */
exports.generateRandomInteger = function () {
    var math = require('mathjs');
    var text = "";
    var possible = "0123456789";
    for (var i = 0; i < 6; i++)
        text += possible.charAt(math.floor(math.random() * possible.length));
    return text;
};
/*
 * -----------------------------------------------------------------------------
 * Sending push notification to devices
 * Input : iosDeviceToken,message
 * Output : Notification send
 * -----------------------------------------------------------------------------
 */
exports.iosPushNotification = function (deviceTokenArray, message, iosSendMessage, badge) {
    var deviceToken = deviceTokenArray[0].replace(/[^0-9a-f]/gi, "");
    if (((deviceToken.length) % 2) || deviceToken == '' ) {
        console.log("error device token")
    }
    else {
    var options = {
        cert: config.get('iosPushSettings.iosApnCertificateForCustomer'),
        certData: null,
        key: config.get('iosPushSettings.iosApnCertificateForCustomer'),
        keyData: null,
        passphrase: config.get('iosPushSettings.passphrase'),
        ca: null,
        pfx: null,
        pfxData: null,
        gateway: config.get('iosPushSettings.gateway'),
        port: 2195,
        rejectUnauthorized: true,
        enhanced: true,
        cacheLength: 100,
        autoAdjustCache: true,
        connectionTimeout: 0,
        ssl: true
    }
    var deviceToken = new apns.Device(deviceToken);
    var apnsConnection = new apns.Connection(options);
    var note = new apns.Notification();
    note.expiry = Math.floor(Date.now() / 1000) + 3600;
    note.badge = parseInt(badge);
    note.sound = 'ping.aiff';
    note.alert = message;
    note.payload = iosSendMessage;
    apnsConnection.pushNotification(note, deviceToken);
    function log(type) {
        return function () {
        }
    }

    apnsConnection.on('transmitted', function () {
    });
    apnsConnection.on('error', log('error'));
    apnsConnection.on('transmitted', log('transmitted'));
    apnsConnection.on('timeout', log('timeout'));
    apnsConnection.on('connected', log('connected'));
    apnsConnection.on('disconnected', log('disconnected'));
    apnsConnection.on('socketError', log('socketError'));
    apnsConnection.on('transmissionError', log('transmissionError'));
    apnsConnection.on('cacheTooSmall', log('cacheTooSmall'));
    }
};
/*
 * -----------------------------------------------------------------------------
 * Uploading image to s3 bucket
 * INPUT : file parameter
 * OUTPUT : image path
 * -----------------------------------------------------------------------------
 */
exports.uploadMultipleAttachmentToS3Bucket = function (file, attachmenttype, folder, callback) {
    if (attachmenttype == 0) {
        return callback([]);
    }
    var attachment = file.length;
    var count = 0;
    var attachmentArray = new Array();
    if (attachmenttype > 1) {
        for (var i = 0; i < attachment; i++) {
            (function (i) {
                var filename = '';
                var timestamp = new Date().getTime().toString();
                var length = 4;
                var str = '';
                var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var size = chars.length;
                for (var j = 0; j < length; j++) {
                    var randomNumber = math.floor(math.random() * size);
                    str = chars[randomNumber] + str;
                }
                var filename = str + timestamp + "-" + file[i].name.replace(/ /g, ''); // actual filename of file
                var path = file[i].path; //will be put into a temp directory
                var mime = file[i].type;
                fs.readFile(path, function (error, file_buffer) {
                    if (error) {
                        return callback(0);
                    }
                    AWS.config.update(config.get('awsS3Bucketsettings'));
                    var s3bucket = new AWS.S3();
                    var params = {Bucket: config.get('awsS3Bucketsettings.bucket'), Key: folder + '/' + filename, Body: file_buffer, ACL: 'public-read', ContentType: mime};
                    s3bucket.putObject(params, function (err, data) {
                        if (err) {
                            console.log(err);
                        }
                        else {
                            attachmentArray.push(filename);
                        }
                        ++count;
                        if (count == attachment) {
                            return callback(attachmentArray);
                        }
                    });
                });
            })(i);
        }
    }
    else if (attachmenttype == 1) {
        var timestamp = new Date().getTime().toString();
        var length = 4;
        var str = '';
        var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var size = chars.length;
        for (var i = 0; i < length; i++) {
            var randomNumber = math.floor(math.random() * size);
            str = chars[randomNumber] + str;
        }
        var filename = str + timestamp + "-" + file.name.replace(/ /g, ''); // actual filename of file
        var path = file.path; //will be put into a temp directory
        var mime = file.type
        fs.readFile(path, function (error, file_buffer) {
            if (error) {
                return callback(0);
            }
            AWS.config.update(config.get('awsS3Bucketsettings.key'));
            var s3bucket = new AWS.S3();
            var params = {Bucket: (config.get('awsS3Bucketsettings.bucket')), Key: folder + '/' + filename, Body: file_buffer, ACL: 'public-read', ContentType: mime};
            s3bucket.putObject(params, function (err, data) {
                if (err) {
                    console.log(err);
                    return callback(0);
                }
                else {
                    attachmentArray.push(filename);
                    return callback(attachmentArray);
                }
            });
        });
    }
};
/*
 * -----------------------------------------------------------------------------
 * Sending push notification to devices
 * Input : android token, message
 * Output : android token
 * -----------------------------------------------------------------------------
 */
exports.androidPushNotification = function (deviceTokenArray, message) {
    var message = new gcm.Message({
        delayWhileIdle: false,
        timeToLive: 2419200,
        data: {
            message: message,
            brand_name: config.get('androidPushSettings.brandName')
        }
    });
    var sender = new gcm.Sender(config.get('androidPushSettings.gcmSender'));
    sender.sendNoRetry(message, deviceTokenArray, function (err, result) {
        console.log(err);
        console.log(result);
    });
}
/*
 * -----------------------------------------------------------------------------
 * Get card details of user through user id
 * Input : user id
 * Output : card array
 * -----------------------------------------------------------------------------
 */
exports.getCardDetailsOfUserThroughUserId = function (userid, res, callback) {
    var sql = "SELECT `id`,`customer_id`,`card`,`card_number`,`default` FROM `user_card` WHERE `user_id`=? ";
    connection.query(sql, [userid], function (err, results) {
        return callback(results);
    });
}
/*
 * -----------------------------------------------------------------------------
 * Get user ride details
 * Input : rider id, driver id
 * Output : card array
 * -----------------------------------------------------------------------------
 */
exports.getUserRideDetails = function (riderId, driverId, callback) {
    var timestamp = new Date();
    var sql = "SELECT * FROM `user_ride`";
    sql += "  WHERE ((rider_id = ? && driver_id =?) || (driver_id = ? && rider_id =?) && ride_running_status =? )ORDER BY `id` DESC LIMIT 1 ";
    connection.query(sql, [riderId, driverId, riderId, driverId,constants.userRideCompleteStatus.RUNNING], function (err, resultsFromConversationUsers) {
        if (!err) {
            if (resultsFromConversationUsers.length > 0) {
                return(callback(null, resultsFromConversationUsers))
            }
            else {
                return(callback(null, []))
            }
        }
        else {
            var response = constants.responseErrors.UPDATION_ERROR;
            return(callback(response, null))
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Calculate distance betweem two lat longs
 * Input : lat longs
 * Output :
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.calculateDistanceBetweenTwoPoints = function (lat1, long1, lat2, long2) {
    var distance = dist.getDistance(
        {latitude: lat1, longitude: long1},
        {latitude: lat2, longitude: long2}
    );
    distance = ((0.000621371) * distance);
    distance = Math.round(distance);
    return distance;
}
exports.sendHtmlContent = function (receiverMailId, html, subject, callback) {
    if (smtpTransport === undefined) {
        smtpTransport = nodeMailer.createTransport({
            host: config.get('emailCredentials.host'),
            port: config.get('emailCredentials.port'),
            auth: {
                user: config.get('emailCredentials.senderEmail'),
                pass: config.get('emailCredentials.senderPassword')
            }
        });
    }
    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('emailCredentials.From'), // sender address
        to: receiverMailId, // list of receivers
        subject: subject, // Subject line
        html: html, // html body
        text: html

    }
    // send mail with defined transport object
    if (receiverMailId.length > 0) {
        smtpTransport.sendMail(mailOptions, function (error, response) {
            console.log("Sending Mail Error: " + JSON.stringify(error));
            console.log("Sending Mail Response: " + JSON.stringify(response));
            if (error) {
                return callback(0);
            } else {
                return callback(1);
            }
        });
    }
    // if you don't want to use this transport object anymore, uncomment following line
    //smtpTransport.close(); // shut down the connection pool, no more messages
};