/**
 * Created by clicklabs on 10/9/14.
 */
var func = require('./functions');
var constants = require('./constants');
var request = require('request');
var async = require('async');
var moment = require('moment');
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get all user conversation list
 * Input : access token,
 * Output :user's response
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.getAllUserConversationListThroughAccessToken = function (req, res) {
    var accessToken = req.body.accesstoken;
    var fbAccessToken = req.body.fbaccesstoken;
    var manValues = [accessToken];
    console.log(req.body);
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ['current_latitude', 'current_longitude'];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            var flag = constants.getConversationListOf.UN_BLOCK_USER;
            getAllUserConversationListThroughAccessTokenResult(resultsFromAccessToken, fbAccessToken, flag, res, req);
        });
    });
};
function getAllUserConversationListThroughAccessTokenResult(resultsFromAccessToken, fbAccessToken, flag, res, req) {
    var timestamp = new Date();
    func.getAllBlockedUserThroughAccessTokenResult(resultsFromAccessToken[0].user_id, function (err, responseFromBlockedUser) {
        if (err) {
            res.jsonp(err);
        }
        else {
            var blockedUser = new Array();
            var blockedCount = responseFromBlockedUser.length;
            for (var k = 0; k < blockedCount; k++) {
                blockedUser.push(responseFromBlockedUser[k].blocked_id);
            }
            var sql = "SELECT `chat_id`,`sender_id`,`receiver_id`,`sender_location_sharing`,`receiver_location_sharing`,`sender_last_message`,`receiver_last_message`";
            sql += "  FROM `conversation` WHERE (`sender_id`=? ) || (`receiver_id`=? ) ";
            connection.query(sql, [ resultsFromAccessToken[0].user_id, resultsFromAccessToken[0].user_id], function (err, resultsFromConversations) {
                var conversationLength = resultsFromConversations.length;
                var userIdArray = new Array();
                for (var i = 0; i < conversationLength; i++) {
                    if (resultsFromConversations[i].sender_id != resultsFromAccessToken[0].user_id) {
                        userIdArray.push(resultsFromConversations[i].sender_id);
                    }
                    else if (resultsFromConversations[i].receiver_id != resultsFromAccessToken[0].user_id) {
                        userIdArray.push(resultsFromConversations[i].receiver_id);
                    }
                }
                if (flag == constants.getConversationListOf.UN_BLOCK_USER) {
                    var newUserIdArray = arrayDiff(userIdArray, blockedUser);
                }
                else if (flag == constants.getConversationListOf.BLOCKED_USER) {
                    var newUserIdArray = blockedUser;
                }
                var newUserCount = newUserIdArray.length;
                if (newUserCount > 0) {
                    var str = newUserIdArray.toString(',');
                    var sql = "SELECT `fb_id`,`user_id`,`age`,`first_name`,CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image)  END as image,";
                    sql += "`profile`,`profile_rating`,`about`,`current_latitude`, `current_longitude` FROM `user` WHERE `user_id` IN(" + str + ")  && `is_deleted`=? ORDER BY FIELD(`user_id`," + str + ")";
                    connection.query(sql, [constants.userDeleteStatus.NOT_DELETED], function (err, resultsFromUsers) {
                        var userCount = resultsFromUsers.length;
                        if (userCount > 0) {
                            var newChatIdArray = new Array();
                            var filterChatArray = new Array();
                            var filterLastMessageArray = new Array();
                            var newIsLocationSharedArray = new Array();
                            var newLastLastMessageSharedArray = new Array();
                            var filterUserArray = new Array();
                            for (var d = 0; d < userCount; d++) {
                                filterUserArray.push(resultsFromUsers[d].user_id);
                            }
                            var filterStr = filterUserArray.toString(',');
                            var sql = "SELECT AVG(rate) AS `rating`,`rated_id` FROM `user_rating` WHERE `rated_id` IN (" + filterStr + ")";
                            sql += " GROUP BY rated_id ORDER BY FIELD(`rated_id`," + filterStr + ")";
                            connection.query(sql, function (err, resultsFromRatings) {
                                var sql = "SELECT  `rate`,`rated_id` FROM `user_rating` WHERE  `user_id` =? && `rated_id` IN (" + filterStr + ")";
                                sql += " GROUP BY rated_id ORDER BY FIELD(`rated_id`," + filterStr + ")";
                                connection.query(sql, [resultsFromAccessToken[0].user_id], function (err, resultsFromUserRatings) {
                                    var userRatingCount = resultsFromUserRatings.length;
                                    var flag = 0;
                                    var finalArray = [];
                                    var ratingLength = resultsFromRatings.length;
                                    var sql = "SELECT `id`, CASE WHEN image LIKE 'http%' THEN image ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',image)  END as image,";
                                    sql += "`user_id` FROM `user_image` WHERE `user_id` IN (" + filterStr + ") ";
                                    connection.query(sql, function (err, resultsFromImages) {
                                        var userImagesLength = resultsFromImages.length;
                                        var sql = "SELECT  `trip_id`,`hytched_id` FROM `mutual_hytched_user` WHERE  `user_id` =? ";
                                        connection.query(sql, [resultsFromAccessToken[0].user_id], function (err, resultsFromMutualHytched) {
                                            console.log(resultsFromAccessToken[0].user_id);
                                            console.log(resultsFromMutualHytched);
                                            var mutualCount = resultsFromMutualHytched.length;
                                            for (var i = 0; i < userCount; i++) {
                                                (function (i) {
                                                    var userProfile = parseInt(resultsFromUsers[i].profile);
                                                    var imagesArray = [];
                                                    var urls = "https://graph.facebook.com/v2.1/" + resultsFromUsers[i].fb_id + "?fields=context&access_token=" + fbAccessToken + "";
                                                    request(urls, function (error, response, body) {
                                                        if (!error && response.statusCode === constants.errorResponseCode.WHILE_FACEBOOK_AUTHENTICATION) {
                                                            var output = JSON.parse(body);
                                                            var mutualFriendsArray = [];
                                                            var mutualLikesArray = [];
                                                            if (output['context']['mutual_likes']['data'].length > 0) {
                                                                var fbLikesLength = (output['context']['mutual_likes']['data']).length;
                                                                for (var j = 0; j < fbLikesLength; j++) {
                                                                    mutualLikesArray.push({"id": output['context']['mutual_likes']['data'][j].id, "name": output['context']['mutual_likes']['data'][j].name,
                                                                        "image": "http://graph.facebook.com/" + output['context']['mutual_likes']['data'][j].id + "/picture?width=225&height=225"});
                                                                }
                                                            }
                                                            else {
                                                                mutualLikesArray = [];
                                                            }
                                                            if (output['context']['mutual_friends']) {
                                                                var fbFriendLength = (output['context']['mutual_friends']['data']).length;
                                                                for (var l = 0; l < fbFriendLength; l++) {
                                                                    mutualFriendsArray.push({"id": output['context']['mutual_friends']['data'][l].id, "name": output['context']['mutual_friends']['data'][l].name,
                                                                        "image": "http://graph.facebook.com/" + output['context']['mutual_friends']['data'][l].id + "/picture?width=225&height=225"});
                                                                }
                                                            }
                                                            else {
                                                                mutualFriendsArray = [];
                                                            }
                                                            for (var s = 0; s < userImagesLength; s++) {
                                                                if (resultsFromUsers[i].user_id == resultsFromImages[s].user_id) {
                                                                    imagesArray.push({"id": resultsFromImages[s].id, "image": resultsFromImages[s].image})
                                                                }
                                                            }
                                                        }
                                                        //Getting trip name
                                                        var tripIdArray = new Array();
                                                        var tripNameArray = new Array();
                                                        for (var p = 0; p < mutualCount; p++) {
                                                            if (resultsFromMutualHytched[p]['hytched_id'] == resultsFromUsers[i]['user_id']) {
                                                                tripIdArray.push(resultsFromMutualHytched[p]['trip_id']);
                                                            }
                                                        }
                                                        if (tripIdArray.length > 0) {
                                                            var tripString = tripIdArray.toString(',');
                                                        }
                                                        else {
                                                            var tripString = '0';
                                                        }
                                                        console.log('HELL')
                                                        console.log(tripString)
                                                        var tripSql = "SELECT  `name` FROM `trip` WHERE  `id` IN (" + tripString + ") ORDER BY FIELD(`id`," + tripString + ")"
                                                        connection.query(tripSql, [resultsFromAccessToken[0].user_id], function (err, resultsFromTripName) {
                                                            var tripNameLength = resultsFromTripName.length;
                                                            for (var r = 0; r < tripNameLength; r++) {
                                                                tripNameArray.push(resultsFromTripName[r]['name']);
                                                            }
                                                            var flagRating = 0;
                                                            var ratingFlag = 0;
                                                            if (userProfile === constants.userProfileType.RIDER) {
                                                                var profile = 'Rider';
                                                            }
                                                            else if (userProfile === constants.userProfileType.DRIVER) {
                                                                profile = 'Driver';
                                                            }
                                                            else if (userProfile === constants.userProfileType.BOTH) {
                                                                profile = 'Both';
                                                            }
                                                            else {
                                                                profile = '';
                                                            }
                                                            for (var j = 0; j < ratingLength; j++) {
                                                                if ((resultsFromUsers[i].user_id == resultsFromRatings[j].rated_id) && (resultsFromUsers[i].profile_rating == 1)) {
                                                                    var rating = resultsFromRatings[j].rating;
                                                                    flagRating = 1;
                                                                    break;
                                                                }
                                                            }
                                                            if (flagRating == 0) {
                                                                rating = 0;
                                                            }
                                                            for (var s = 0; s < userRatingCount; s++) {
                                                                if (resultsFromUsers[i].user_id == resultsFromUserRatings[s].rated_id) {
                                                                    var opponent_rating = resultsFromUserRatings[s].rate;
                                                                    ratingFlag = 1;
                                                                    break;
                                                                }
                                                            }
                                                            if (ratingFlag == 0) {
                                                                opponent_rating = 0;
                                                            }
                                                            var userLatitude = resultsFromAccessToken[0].current_latitude;
                                                            var userLongitude = resultsFromAccessToken[0].current_longitude;
                                                            var opponentLatitude = resultsFromUsers[i].current_latitude;
                                                            var opponentLongitude = resultsFromUsers[i].current_longitude;
                                                            var miles = func.calculateDistanceBetweenTwoPoints(userLatitude, userLongitude, opponentLatitude, opponentLongitude)
                                                            for (var m = 0; m < conversationLength; m++) {
                                                                if ((resultsFromConversations[m].sender_id == resultsFromUsers[i].user_id) && (resultsFromConversations[m].receiver_id == resultsFromAccessToken[0].user_id)) {
                                                                    var chatId = resultsFromConversations[m].chat_id;
                                                                    var lastMessage = resultsFromConversations[m].receiver_last_message;
                                                                    break;
                                                                }
                                                                else if ((resultsFromConversations[m].receiver_id == resultsFromUsers[i].user_id) && (resultsFromConversations[m].sender_id == resultsFromAccessToken[0].user_id)) {
                                                                    var chatId = resultsFromConversations[m].chat_id;
                                                                    var lastMessage = resultsFromConversations[m].receiver_last_message;
                                                                    break;
                                                                }
                                                            }
                                                            finalArray.push({"chat_id": chatId, "user_id": resultsFromUsers[i].user_id, "name": resultsFromUsers[i].first_name,
                                                                "image": resultsFromUsers[i].image, "age": resultsFromUsers[i].age, "profile": profile, "rating": rating,
                                                                "last_message": lastMessage,
                                                                "opponent_rating": opponent_rating, "mutual_friends": mutualFriendsArray, "mutual_likes": mutualLikesArray,
                                                                "image_array": imagesArray, "about": resultsFromUsers[i].about, "miles": miles, "Profile": profile, "trip_name": tripNameArray})
                                                            ++flag;
                                                            if (flag == userCount) {
                                                                var response = {"data": func.sortByKeyAsc(finalArray, 'miles')};
                                                                res.jsonp(response);
                                                            }
                                                        });
                                                    });
                                                })(i);
                                            }
                                        });
                                    });
                                });
                            });
                        }
                        else {
                            var response = {"data": []};
                            res.jsonp(response);
                        }
                    });
                }
                else {
                    var response = {"data": []};
                    res.jsonp(response);
                }
            });
        }
    });
}
/* ............................................................................................................................................................................................
 * ------------------------------------------------------
 *  This Function is called to send new message by user to individuals
 *  Input:Access token, game Id, message
 *  Output:
 * ------------------------------------------------------
 */
exports.sendNewMessage = function (req, res) {
    var accessToken = req.body.accesstoken;
    var userId = req.body.userid;
    var chatId = req.body.chatid;
    var message = req.body.message;
    var messageType = req.body.messagetype;
    var index = req.body.index;
    console.log(req.body);
    var manValues = [accessToken];
    func.authenticateMandatoryFields(manValues, res, function (aa) {
        var extraData = ["first_name", "CASE WHEN `image` LIKE 'http%' THEN `image` ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',`image`) END as image"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultFromAccessToken) {
            sendNewMessageResult(resultFromAccessToken, userId, chatId, message, messageType, index, res, req);
        });
    });
};
function sendNewMessageResult(resultFromAccessToken, userId, chatId, message, messageType, index, res, req) {
    var timestamp = new Date();
    var sql = "SELECT `is_deleted`, `android_device_token`,`ios_device_token`,`messages`,`push_notifications` FROM `user` WHERE `user_id` =?  LIMIT 1";
    connection.query(sql, [userId], function (err, resultsFromDeviceTokens) {
        if (resultsFromDeviceTokens[0].is_deleted == constants.userDeleteStatus.DELETED) {
            var response = constants.responseErrors.USER_DOES_NOT_EXIST;
            res.jsonp(response);
        }
        func.verifyUserForBlocking(userId, resultFromAccessToken[0].user_id, function (err, response) {
            if ((resultsFromDeviceTokens.length > 0) && (resultsFromDeviceTokens[0].messages == constants.defaultUserSettingValue.ENABLE)) {
                var block = 0
            }
            else {
                block = resultFromAccessToken[0].user_id;
            }
            if (!err) {
                var sql = "INSERT INTO `chat`(`chat_id`,`sender_id`,`sender_name`,`receiver_id`,`chat`,`type`,`blocked`,`created_at`) VALUES (?,?,?,?,?,?,?,?)";
                connection.query(sql, [chatId, resultFromAccessToken[0].user_id, resultFromAccessToken[0].first_name, userId, message, messageType, block, timestamp],
                    function (err, resultFromIndividualChat) {
                        console.log(err);
                        if (!err) {
                            var response = {"id": resultFromIndividualChat.insertId, "timestamp": timestamp, "name": resultFromAccessToken[0].first_name,
                                "chat": message, "type": messageType, "index": index};
                            res.jsonp(response);
                            var sql = "UPDATE `conversation` SET `sender_last_message`=?, `receiver_last_message`=? WHERE `chat_id`= ? LIMIT 1";
                            connection.query(sql, [message, message, chatId], function (err, result) {
                                if ((resultsFromDeviceTokens.length > 0) && (resultsFromDeviceTokens[0].push_notifications == constants.defaultUserSettingValue.ENABLE)
                                    && (resultsFromDeviceTokens[0].messages == constants.defaultUserSettingValue.ENABLE)) {
                                    var sql = "SELECT COUNT(`read`)as unread, receiver_id FROM `chat` WHERE `receiver_id` =? && `read`=?";
                                    connection.query(sql, [userId, 0], function (err, resultFromUnRead) {
                                        var androidTokenArray = new Array();
                                        var iosTokenArray = new Array();
                                        if (resultsFromDeviceTokens[0].android_device_token != '') {
                                            androidTokenArray.push(resultsFromDeviceTokens[0].android_device_token);
                                        }
                                        if (resultsFromDeviceTokens[0].ios_device_token != '' || resultsFromDeviceTokens[0].ios_device_token != '(null)') {
                                            iosTokenArray.push(resultsFromDeviceTokens[0].ios_device_token);
                                        }
                                        var iosUnread = [];
                                        var sql = "SELECT  `rate`,`rated_id` FROM `user_rating` WHERE  `user_id` =? && `rated_id`=? LIMIT 1";
                                        connection.query(sql, [resultFromAccessToken[0].user_id, userId], function (err, resultsFromUserRatings) {
                                            if (resultsFromUserRatings.length > 0) {
                                                var opponentRating = resultsFromUserRatings[0].rate;
                                            }
                                            else {
                                                var opponentRating = 0;
                                            }
                                            var androidMessage = ({"chat": message, "sender_id": resultFromAccessToken[0].user_id, "sender_name": resultFromAccessToken[0].first_name,
                                                "sender_image": resultFromAccessToken[0].image, "type": constants.pushNotificationsType.SEND_NEW_MESSAGE, "badge": resultFromUnRead[0].unread, "opponent_rating": opponentRating, "chat_id": chatId})
                                            var iosSendMessage = ({"sender_id": resultFromAccessToken[0].user_id, "sender_name": resultFromAccessToken[0].first_name,
                                                "sender_image": resultFromAccessToken[0].image, "type": constants.pushNotificationsType.SEND_NEW_MESSAGE, "opponent_rating": opponentRating, "chat_id": chatId})
                                            if (androidTokenArray.length > 0) {
                                                func.androidPushNotification(androidTokenArray, androidMessage);
                                            }
                                            if (iosTokenArray.length > 0) {
                                                iosUnread.push(resultFromUnRead[0].unread);
                                                var messageWithSender = resultFromAccessToken[0].first_name + ' : ' + message
                                                func.iosPushNotification(iosTokenArray, messageWithSender, iosSendMessage, iosUnread);
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    });
            }
            else {
                block = resultFromAccessToken[0].user_id;
                var sql = "UPDATE `conversation` SET `sender_last_message`=? WHERE `chat_id`= ? LIMIT 1";
                connection.query(sql, [message, chatId], function (err, result) {
                    var sql = "INSERT INTO `chat`(`chat_id`,`sender_id`,`sender_name`,`receiver_id`,`chat`,`type`,`blocked`,`created_at`) VALUES (?,?,?,?,?,?,?,?)";
                    connection.query(sql, [chatId, resultFromAccessToken[0].user_id, resultFromAccessToken[0].first_name, userId, message, messageType, block, timestamp],
                        function (err, resultFromIndividualChat) {
                            var response = {"id": resultFromIndividualChat.insertId, "timestamp": timestamp, "name": resultFromAccessToken[0].first_name,
                                "chat": message, "type": messageType, "index": index};
                            res.jsonp(response);
                        });
                });
            }
        });
    });
}
/* ............................................................................................................................................................................................
 * ------------------------------------------------------
 *  This Function is called to read or view unread message by user from chat
 *  Input:access token, sender_id
 *  Output: message
 * ------------------------------------------------------
 */
exports.getUnReadMessagesFromChat = function (req, res) {
    var accessToken = req.body.accesstoken;
    var userId = req.body.userid;
    var location = req.body.location;
    var manValues = [accessToken, userId];
    console.log(req.body);
    func.authenticateMandatoryFields(manValues, res, function (aa) {
        var extraData = ["messages", "profile", "current_latitude", "current_longitude", "default_trip"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultFromAccessToken) {
            getUnReadMessagesFromChatResult(resultFromAccessToken, userId, location, res, req);
        });
    });
};
function getUnReadMessagesFromChatResult(resultFromAccessToken, userId, location, res, req) {
    var sql = "SELECT c.sender_id, c.sender_name, c.chat, c.created_at, c.type, CASE WHEN image  LIKE 'http%' THEN `image` ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',`image`) END as image,";
    sql += " u.current_latitude,u.current_longitude,u.profile FROM chat c INNER JOIN user u ON c.sender_id = u.user_id WHERE c.sender_id =? && c.receiver_id =? && c.read =? && (c.blocked= ?  || c.blocked= ? )ORDER BY c.id  ";
    connection.query(sql, [userId, resultFromAccessToken[0].user_id, constants.defaultUserSettingValue.DISABLE, constants.defaultUserSettingValue.DISABLE, resultFromAccessToken[0].user_id], function (err, resultsFromChat) {
        if (!err) {
            func.getUserRideDetails(userId, resultFromAccessToken[0].user_id, function (err, responseFromRides) {
                if (err) {
                    res.jsonp(err);
                }
                else {
                    if (responseFromRides.length > 0) {
                        var rideStatus = responseFromRides[0].ride_status;
                        var isSetPickUpLocation = responseFromRides[0].is_set_pickup_location_by_rider;
                        var riderPickupLatitude = responseFromRides[0].rider_pickup_latitude;
                        var riderPickupLongitude = responseFromRides[0].rider_pickup_longitude;
                        var riderPickupLocationString = responseFromRides[0].rider_pickup_location_string;
                        var driverStartLatitude = responseFromRides[0].driver_start_latitude;
                        var driverStartLongitude = responseFromRides[0].driver_start_longitude;
                        var driverStartLocationString = responseFromRides[0].driver_start_location_string;
                        var routeStringLocation = responseFromRides[0].route_string_location;
                        var rideStartLatitude = responseFromRides[0].ride_start_latitude;
                        var rideStartLongitude = responseFromRides[0].ride_start_longitude;
                        var rideStartDateTime = moment(responseFromRides[0].ride_start_date_time).format("YYYY-MM-DD H:mm ss");
                        var rideEndDateTime = moment(responseFromRides[0].ride_end_date_time).format("YYYY-MM-DD H:mm ss");
                        var rideStartLocationString = responseFromRides[0].ride_start_location_string;
                        var rideEndLocationString = responseFromRides[0].ride_end_location_string;
                        var amount = responseFromRides[0].amount;
                        var distance = responseFromRides[0].distance;
                        var isRideCompletedByRider = responseFromRides[0].is_ride_completed_by_rider;
                        var isRideCompletedByDriver = responseFromRides[0].is_ride_completed_by_driver;
                        var rideId = responseFromRides[0].id;
                    }
                    else {
                        var rideStatus = constants.defaultUserSettingValue.DISABLE;
                        var isSetPickUpLocation = constants.defaultUserSettingValue.DISABLE;
                        var riderPickupLatitude = constants.defaultUserLocationValue.LATITUDE;
                        var riderPickupLongitude = constants.defaultUserLocationValue.LONGITUDE;
                        var riderPickupLocationString = '';
                        var driverStartLatitude = constants.defaultUserLocationValue.LATITUDE;
                        var driverStartLongitude = constants.defaultUserLocationValue.LONGITUDE;
                        var driverStartLocationString = '';
                        var routeStringLocation = '';
                        var rideId = constants.defaultUserSettingValue.DISABLE;
                        var rideStartLatitude = constants.defaultUserLocationValue.LATITUDE;
                        var routeStartLongitude = constants.defaultUserLocationValue.LONGITUDE;
                        var routeStartDateTime = ''
                        var rideEndDateTime = '';
                        var rideStartLocationString = ''
                        var rideEndLocationString = '';
                        var amount = '';
                        var distance = '';
                        var driverRideStatus = constants.defaultUserSettingValue.DISABLE;
                        var isRideCompletedByRider = constants.defaultUserSettingValue.DISABLE;
                        var isRideCompletedByDriver = constants.defaultUserSettingValue.DISABLE;
                    }
                    if (resultsFromChat.length > 0) {
                        if ((resultFromAccessToken[0].profile == constants.userProfileType.BOTH) && (resultsFromChat[0].profile != constants.userProfileType.BOTH)) {
                            if (resultsFromChat[0].profile == constants.userProfileType.DRIVER) {
                                var newUserProfile = constants.userProfileType.RIDER;
                            }
                            else if (resultsFromChat[0].profile == constants.userProfileType.RIDER) {
                                var newUserProfile = constants.userProfileType.DRIVER;
                            }
                            var sql = "UPDATE `user` SET `profile`=? WHERE `user_id` = ? LIMIT 1";
                            connection.query(sql, [newUserProfile, resultFromAccessToken[0].user_id], function (err, result) {
                            });
                            var sql = "UPDATE `trip` SET `profile`=? WHERE `id` = ? LIMIT 1";
                            connection.query(sql, [newUserProfile, resultFromAccessToken[0].default_trip], function (err, result) {
                            });
                        }
                        else {
                            newUserProfile = resultFromAccessToken[0].profile;
                        }
                        if (resultFromAccessToken[0].profile == constants.userProfileType.DRIVER) {
                            var driverCurrentLatitude = resultFromAccessToken[0].current_latitude;
                            var driverCurrentLongitude = resultFromAccessToken[0].current_longitude;
                        }
                        else {
                            driverCurrentLatitude = resultsFromChat[0].current_latitude;
                            driverCurrentLongitude = resultsFromChat[0].current_longitude;
                        }
                        var response = {"chat": resultsFromChat, updated_user_profile: newUserProfile, ride_status: rideStatus,
                            is_set_pickup_location_by_rider: isSetPickUpLocation, rider_pickup_latitude: riderPickupLatitude,
                            rider_pickup_longitude: riderPickupLongitude, rider_pickup_location_string: riderPickupLocationString,
                            driver_start_latitude: driverStartLatitude, driver_start_longitude: driverStartLongitude,
                            driver_start_location_string: driverStartLocationString, route_string_location: routeStringLocation,
                            driver_current_latitude: driverCurrentLatitude, driver_current_longitude: driverCurrentLongitude, ride_id: rideId,
                            ride_start_latitude: rideStartLatitude, ride_start_longitude: rideStartLongitude, ride_start_date_time: rideStartDateTime,
                            ride_end_date_time: rideEndDateTime, ride_start_location_string: rideStartLocationString, ride_end_location_string: rideEndLocationString,
                            amount: amount, distance: distance, is_ride_completed_by_rider: isRideCompletedByRider, is_ride_completed_by_driver: isRideCompletedByDriver
                        };
                        res.jsonp(response);
                        updateMessageReadStatus(userId, resultFromAccessToken[0].user_id);
                    }
                    else {
                        var sql = "SELECT `current_latitude`,`current_longitude`,`profile` FROM `user` WHERE `user_id` = ? && `is_deleted` =? LIMIT 1";
                        connection.query(sql, [userId, constants.userDeleteStatus.NOT_DELETED], function (err, resultFromUser) {
                            if (resultFromUser.length > 0) {
                                if ((resultFromAccessToken[0].profile == constants.userProfileType.BOTH) && (resultFromUser[0].profile != constants.userProfileType.BOTH)) {
                                    if (resultFromUser[0].profile == constants.userProfileType.DRIVER) {
                                        var newUserProfile = constants.userProfileType.RIDER;
                                    }
                                    else if (resultFromUser[0].profile == constants.userProfileType.RIDER) {
                                        var newUserProfile = constants.userProfileType.DRIVER;
                                    }
                                    var sql = "UPDATE `user` SET `profile`=? WHERE `user_id` = ? LIMIT 1";
                                    connection.query(sql, [newUserProfile, resultFromAccessToken[0].user_id], function (err, result) {
                                    });
                                    var sql = "UPDATE `trip` SET `profile`=? WHERE `id` = ? LIMIT 1";
                                    connection.query(sql, [newUserProfile, resultFromAccessToken[0].default_trip], function (err, result) {
                                    });
                                }
                                else {
                                    newUserProfile = resultFromAccessToken[0].profile;
                                }
                                if (resultFromAccessToken[0].profile == constants.userProfileType.DRIVER) {
                                    driverCurrentLatitude = resultFromAccessToken[0].current_latitude;
                                    driverCurrentLongitude = resultFromAccessToken[0].current_longitude;
                                }
                                else {
                                    driverCurrentLatitude = resultFromUser[0].current_latitude;
                                    driverCurrentLongitude = resultFromUser[0].current_longitude;
                                }
                            }
                            else {
                                if (resultFromAccessToken[0].profile == constants.userProfileType.DRIVER) {
                                    driverCurrentLatitude = resultFromAccessToken[0].current_latitude;
                                    driverCurrentLongitude = resultFromAccessToken[0].current_longitude;
                                }
                                else {
                                    driverCurrentLatitude = constants.defaultUserLocationValue.LATITUDE;
                                    driverCurrentLongitude = constants.defaultUserLocationValue.LONGITUDE;
                                }
                            }
                            var response = {"chat": [], updated_user_profile: newUserProfile, ride_status: rideStatus,
                                is_set_pickup_location_by_rider: isSetPickUpLocation, rider_pickup_latitude: riderPickupLatitude,
                                rider_pickup_longitude: riderPickupLongitude, rider_pickup_location_string: riderPickupLocationString,
                                driver_start_latitude: driverStartLatitude, driver_start_longitude: driverStartLongitude,
                                driver_start_location_string: driverStartLocationString, route_string_location: routeStringLocation,
                                driver_current_latitude: driverCurrentLatitude, driver_current_longitude: driverCurrentLongitude, ride_id: rideId,
                                ride_start_latitude: rideStartLatitude, ride_start_longitude: rideStartLongitude, ride_start_date_time: rideStartDateTime,
                                ride_end_date_time: rideEndDateTime, ride_start_location_string: rideStartLocationString, ride_end_location_string: rideEndLocationString,
                                amount: amount, distance: distance, is_ride_completed_by_rider: isRideCompletedByRider, is_ride_completed_by_driver: isRideCompletedByDriver};
                            res.jsonp(response);
                        });
                    }
                }
            });
        }
    });
}
/* ............................................................................................................................................................................................
 * ------------------------------------------------------
 *  This Function is called get all message of the conversation.
 *  Input:Access token, game Id
 *  Output: message
 * ------------------------------------------------------
 */
exports.getAllMessagesFromChat = function (req, res) {
    var accessToken = req.body.accesstoken;
    var userId = req.body.userid;
    console.log(req.body);
    var manValues = [accessToken, userId];
    func.authenticateMandatoryFields(manValues, res, function (aa) {
        var extraData = [];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultFromAccessToken) {
            getAllMessagesFromChatResult(resultFromAccessToken, userId, res, req);
        });
    });
};
function getAllMessagesFromChatResult(resultFromAccessToken, userId, res, req) {
    var sql = "SELECT c.sender_id,c.sender_name,c.chat,c.type,c.created_at,CASE WHEN image LIKE 'http%' THEN `image` ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',`image`) END as image";
    sql += " FROM chat c INNER JOIN user u On c.sender_id = u.user_id WHERE ((c.sender_id = ? && c.receiver_id =?) || (c.sender_id = ? && c.receiver_id =?) ) && c.blocked=? ORDER BY c.id";
    connection.query(sql, [userId, resultFromAccessToken[0].user_id, resultFromAccessToken[0].user_id, userId, constants.defaultUserSettingValue.DISABLE, resultFromAccessToken[0].user_id], function (err, resultsFromChat) {
        if (!err) {
            var chatLength = resultsFromChat.length;
            func.getCardDetailsOfUserThroughUserId(resultFromAccessToken[0].user_id, res, function (resultsFromCard) {
                if (chatLength > 0) {
                    getUserCurrentLocation(resultFromAccessToken, userId, function (resultFromLocation) {
                        if (userId == resultFromLocation[0].sender_id) {
                            if (resultFromLocation[0].sender_location_sharing == constants.defaultUserSettingValue.ENABLE) {
                                var opponentCurrentLocation = resultFromLocation[0].location;
                                var locationAccuracy = resultFromLocation[0].location_accuracy;
                            }
                            else {
                                var opponentCurrentLocation = '';
                                var locationAccuracy = '';
                            }
                            if (resultFromLocation[0].receiver_location_sharing == constants.defaultUserSettingValue.ENABLE) {
                                var userLocationEnable = constants.defaultUserSettingValue.ENABLE;
                            }
                            else {
                                userLocationEnable = constants.defaultUserSettingValue.DISABLE;
                            }
                        }
                        else {
                            if (resultFromLocation[0].receiver_location_sharing == constants.defaultUserSettingValue.ENABLE) {
                                var opponentCurrentLocation = resultFromLocation[0].location;
                                var locationAccuracy = resultFromLocation[0].location_accuracy;
                            }
                            else {
                                var opponentCurrentLocation = '';
                                var locationAccuracy = '';
                            }
                            if (resultFromLocation[0].sender_location_sharing == constants.defaultUserSettingValue.ENABLE) {
                                var userLocationEnable = constants.defaultUserSettingValue.ENABLE;
                            }
                            else {
                                userLocationEnable = constants.defaultUserSettingValue.DISABLE;
                            }
                        }
                        var response = {"chat": resultsFromChat, "opponent_location": opponentCurrentLocation, "user_location_enable": userLocationEnable,
                            "location_accuracy": locationAccuracy, "user_card": resultsFromCard};
                        res.jsonp(response);
                    });
                    updateMessageReadStatus(userId, resultFromAccessToken[0].user_id);
                }
                else {
                    getUserCurrentLocation(resultFromAccessToken, userId, function (resultFromLocation) {
                        if (userId == resultFromLocation[0].sender_id) {
                            if (resultFromLocation[0].sender_location_sharing == constants.defaultUserSettingValue.ENABLE) {
                                var opponentCurrentLocation = resultFromLocation[0].location;
                                var locationAccuracy = resultFromLocation[0].location_accuracy;
                            }
                            else {
                                var opponentCurrentLocation = '';
                                var locationAccuracy = '';
                            }
                            if (resultFromLocation[0].receiver_location_sharing == constants.defaultUserSettingValue.ENABLE) {
                                var userLocationEnable = constants.defaultUserSettingValue.ENABLE;
                            }
                            else {
                                userLocationEnable = constants.defaultUserSettingValue.DISABLE;
                            }
                        }
                        else {
                            if (resultFromLocation[0].receiver_location_sharing == constants.defaultUserSettingValue.ENABLE) {
                                var opponentCurrentLocation = resultFromLocation[0].location;
                                var locationAccuracy = resultFromLocation[0].location_accuracy;
                            }
                            else {
                                var opponentCurrentLocation = '';
                                var locationAccuracy = '';
                            }
                            if (resultFromLocation[0].sender_location_sharing == constants.defaultUserSettingValue.ENABLE) {
                                var userLocationEnable = constants.defaultUserSettingValue.ENABLE;
                            }
                            else {
                                userLocationEnable = constants.defaultUserSettingValue.DISABLE;
                            }
                        }
                        var response = {"chat": [], "opponent_location": opponentCurrentLocation, "user_location_enable": userLocationEnable,
                            "location_accuracy": locationAccuracy, "user_card": resultsFromCard};
                        res.jsonp(response);
                    });
                }
            });
        }
    });
}
/* ............................................................................................................................................................................................
 * ------------------------------------------------------
 *  This Function is used to update message read status.
 *  Input: sender id, receiver id
 *  Output:
 * ------------------------------------------------------
 */
function updateMessageReadStatus(senderId, recieverId) {
    var sql = "UPDATE `chat` SET `read`=? WHERE `sender_id` = ? && `receiver_id` =?";
    connection.query(sql, [1, senderId, recieverId], function (err, result) {
    });
}
/* ............................................................................................................................................................................................
 * ------------------------------------------------------
 *  This Function is called to rate user partner during the conversation.
 *  Input:Access token, rate, user_id
 *  Output: message
 * ------------------------------------------------------
 */
exports.rateUserPartnerDuringConversation = function (req, res) {
    var accessToken = req.body.accesstoken;
    var userId = req.body.userid;
    var rate = req.body.rate;
    console.log(req.body);
    var manValues = [accessToken, userId];
    func.authenticateMandatoryFields(manValues, res, function (aa) {
        var extraData = ["user_id"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultFromAccessToken) {
            rateUserPartnerDuringConversationResult(resultFromAccessToken, userId, rate, res, req);
        });
    });
};
function rateUserPartnerDuringConversationResult(resultFromAccessToken, userId, rate, res, req) {
    var timestamp = new Date();
    var sql = "SELECT `id` FROM `user_rating` WHERE `user_id` = ? && `rated_id` =? LIMIT 1";
    connection.query(sql, [resultFromAccessToken[0].user_id, userId], function (err, resultFromRating) {
        if (resultFromRating.length == 0) {
            var sql = "INSERT INTO `user_rating`(`user_id`,`rated_id`,`rate`,`created_at`) VALUES (?,?,?,?)";
            connection.query(sql, [resultFromAccessToken[0].user_id, userId, rate, timestamp], function (err, resultFromIndividualChat) {
                if (!err) {
                    var response = constants.responseSuccess.USER_RATED_SUCCESSFULLY;
                    res.jsonp(response);
                }
            });
        }
        else {
            var sql = "UPDATE `user_rating` SET `rate`=? WHERE `user_id` = ? && `rated_id` =? LIMIT 1";
            connection.query(sql, [rate, resultFromAccessToken[0].user_id, userId], function (err, result) {
                if (!err) {
                    var response = constants.responseSuccess.USER_RATED_SUCCESSFULLY;
                    res.jsonp(response);
                }
            });
        }
    });
}
/* ............................................................................................................................................................................................
 * ------------------------------------------------------
 *  This Function is called to check location during the conversation.
 *  Input:Access token, rate, user_id
 *  Output: message
 * ------------------------------------------------------
 */
exports.checkLocationDuringConversation = function (req, res) {
    var accessToken = req.body.accesstoken;
    var userId = req.body.userid;
    console.log(req.body);
    var manValues = [accessToken, userId];
    func.authenticateMandatoryFields(manValues, res, function (aa) {
        var extraData = ["first_name", "CASE WHEN `image` LIKE 'http%' THEN `image` ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',`image`) END as image"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultFromAccessToken) {
            checkLocationDuringConversationResult(resultFromAccessToken, userId, res, req);
        });
    });
};
function checkLocationDuringConversationResult(resultFromAccessToken, userId, res, req) {
    var timestamp = new Date();
    var sql = "SELECT `chat_id`,`sender_id`,`receiver_id`,`sender_location_sharing`,`receiver_location_sharing` FROM `conversation` WHERE";
    sql += " (sender_id =? && receiver_id =?) || (receiver_id =? && sender_id =?) LIMIT 1 ";
    connection.query(sql, [resultFromAccessToken[0].user_id, userId, resultFromAccessToken[0].user_id, userId], function (err, resultsFromConversations) {
        var sql = "SELECT `current_location`,`user_type`,`ios_device_token`,`android_device_token`,`push_notifications` FROM `user` WHERE `user_id`=? LIMIT 1 ";
        connection.query(sql, [userId], function (err, resultsFromUsers) {
            if (resultsFromConversations[0].sender_id == resultFromAccessToken[0].user_id) {
                var opponentLocation = resultsFromConversations[0].receiver_location_sharing;
            }
            else {
                var opponentLocation = resultsFromConversations[0].sender_location_sharing;
            }
            if (opponentLocation == constants.defaultUserSettingValue.ENABLE) {
                var response = {"location": resultsFromUsers[0].current_location};
                res.jsonp(response);
            }
            else {
                var response = {"location": ""};
                res.jsonp(response);
                if (resultsFromUsers[0].push_notifications == constants.defaultUserSettingValue.ENABLE) {
                    var androidTokenArray = new Array();
                    var iosTokenArray = new Array();
                    if (resultsFromUsers[0].android_device_token != '') {
                        androidTokenArray.push(resultsFromUsers[0].android_device_token);
                    }
                    if (resultsFromUsers[0].ios_device_token != '' || resultsFromUsers[0].ios_device_token != '(null)') {
                        iosTokenArray.push(resultsFromUsers[0].ios_device_token);
                    }
                    var iosUnread = [];
                    var message = resultFromAccessToken[0].first_name + constants.pushMessageToUser.FOR_VIEW_LOCATION;
                    androidMessage = ({"chat": message, "sender_id": resultFromAccessToken[0].user_id, "sender_name": resultFromAccessToken[0].first_name,
                        "sender_image": resultFromAccessToken[0].image, "type": 1, "user_id": userId, "badge": 1, "chat_id": resultsFromConversations[0].chat_id})
                    var iosSendMessage = ({"sender_id": resultFromAccessToken[0].user_id, "sender_name": resultFromAccessToken[0].first_name,
                        "sender_image": resultFromAccessToken[0].image, "user_id": userId, "type": 1, "chat_id": resultsFromConversations[0].chat_id})
                    if (androidTokenArray.length > 0) {
                        func.androidPushNotification(androidTokenArray, androidMessage);
                    }
                    if (iosTokenArray.length > 0) {
                        iosUnread.push(1);
                        func.iosPushNotification(iosTokenArray, message, iosSendMessage, iosUnread);
                    }
                }
            }
        });
    });
}
function getUserCurrentLocation(resultFromAccessToken, userId, callback) {
    var timestamp = new Date();
    var sql = "SELECT `sender_id`,`receiver_id`,`sender_location_sharing`,`receiver_location_sharing` FROM `conversation`";
    sql += "  WHERE ((sender_id = ? && receiver_id =?) || (sender_id = ? && receiver_id =?)) LIMIT 1 ";
    connection.query(sql, [resultFromAccessToken[0].user_id, userId, userId, resultFromAccessToken[0].user_id], function (err, resultsFromConversationUsers) {
        var sql = "SELECT `current_location`,`location_accuracy` FROM `user` WHERE `user_id`=? LIMIT 1 ";
        connection.query(sql, [userId], function (err, resultsFromUsers) {
            if (resultsFromConversationUsers.length > 0) {
                var locationData = [];
                locationData.push({"sender_id": resultsFromConversationUsers[0].sender_id, "receiver_id": resultsFromConversationUsers[0].receiver_id,
                    "sender_location_sharing": resultsFromConversationUsers[0].sender_location_sharing, "receiver_location_sharing": resultsFromConversationUsers[0].receiver_location_sharing,
                    "location": resultsFromUsers[0].current_location, "location_accuracy": resultsFromUsers[0].location_accuracy})
                return(callback(locationData))
            }
            else {
                return(callback([]))
            }
        });
    });
}
/* ............................................................................................................................................................................................
 * ------------------------------------------------------
 *  This Function is called to check location during the conversation.
 *  Input:Access token, rate, user_id
 *  Output: message
 * ------------------------------------------------------
 */
exports.enableDisableLocationSharingDuringConversation = function (req, res) {
    var accessToken = req.body.accesstoken;
    var userId = req.body.userid;
    console.log(req.body);
    var manValues = [accessToken, userId];
    func.authenticateMandatoryFields(manValues, res, function (aa) {
        var extraData = [];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultFromAccessToken) {
            enableDisableLocationSharingDuringConversationResult(resultFromAccessToken, userId, res, req);
        });
    });
};
function enableDisableLocationSharingDuringConversationResult(resultFromAccessToken, userId, res, req) {
    var timestamp = new Date();
    var sql = "SELECT chat_id,sender_id,receiver_id,sender_location_sharing,receiver_location_sharing FROM `conversation`";
    sql += "  WHERE ((sender_id = ? && receiver_id =?) || (sender_id = ? && receiver_id =?)) LIMIT 1 ";
    connection.query(sql, [resultFromAccessToken[0].user_id, userId, userId, resultFromAccessToken[0].user_id], function (err, resultsFromConversationUsers) {
        if (userId == resultsFromConversationUsers[0].sender_id) {
            if (resultsFromConversationUsers[0].receiver_location_sharing == constants.defaultUserSettingValue.ENABLE) {
                var locate = constants.defaultUserSettingValue.DISABLE;
            }
            else {
                var locate = constants.defaultUserSettingValue.ENABLE;
            }
            var sql = "UPDATE `conversation` SET `receiver_location_sharing`=? WHERE `chat_id` = ? LIMIT 1";
            connection.query(sql, [locate, resultsFromConversationUsers[0].chat_id], function (err, result) {
                if (!err) {
                    var response = constants.responseSuccess.SETTINGS_CHANGED;
                    res.jsonp(response);
                }
            });
        }
        else {
            if (resultsFromConversationUsers[0].sender_location_sharing == constants.defaultUserSettingValue.ENABLE) {
                var locate = constants.defaultUserSettingValue.DISABLE;
            }
            else {
                var locate = constants.defaultUserSettingValue.ENABLE;
            }
            var sql = "UPDATE `conversation` SET `sender_location_sharing`=? WHERE `chat_id` = ? LIMIT 1";
            connection.query(sql, [locate, resultsFromConversationUsers[0].chat_id], function (err, result) {
                if (!err) {
                    var response = constants.responseSuccess.SETTINGS_CHANGED;
                    res.jsonp(response);
                }
            });
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Get all blocked user
 * Input : access token
 * Output :
 * -----------------------------------------------------------------------------
 */
exports.getAllBlockedUserThroughAccessToken = function (req, res) {
    var accessToken = req.body.access_token;
    var fbAccessToken = req.body.fb_access_token;
    var manValues = [accessToken, fbAccessToken];
    console.log(req.body)
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ['current_latitude', 'current_longitude'];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            var flag = constants.getConversationListOf.BLOCKED_USER;
            getAllUserConversationListThroughAccessTokenResult(resultsFromAccessToken, fbAccessToken, flag, res, req);
        });
    });
};
/*
 * -----------------------------------------------------------------------------
 * Update ride information
 * Input : access token
 * Output :
 * -----------------------------------------------------------------------------
 */
exports.updateRideInformationThroughAccessToken = function (req, res) {
    var accessToken = req.body.access_token;
    var requestedParams = req.body;
    var manValues = [accessToken];
    console.log(req.body)
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ['first_name', 'last_name', 'profile', "CASE WHEN `image` LIKE 'http%' THEN `image` " +
            "ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',`image`) END as image", "default_trip"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            var flag = constants.getConversationListOf.BLOCKED_USER;
                    updateRideInformationThroughAccessTokenResult(resultsFromAccessToken, requestedParams, res, req);
        });
    });
};
function updateRideInformationThroughAccessTokenResult(resultsFromAccessToken, requestedParams, res, req) {
    var rideStatus = parseInt(requestedParams.ride_status);
    var userName = resultsFromAccessToken[0].first_name;
    if (rideStatus === constants.userRideStatus.ON_MY_WAY) {
        func.verifyUserForBlocking(requestedParams.user_id, resultsFromAccessToken[0].user_id, function (err, response) {
            if (err) {
                var extraData = ['first_name'];
                func.getUserInformationThroughUserId(requestedParams.user_id, extraData, function (err, responseFromUser) {
                    var errorMessage = responseFromUser[0].first_name + " isn't available for the ride."
                    var response = {error: errorMessage, flag: 25}
                    res.jsonp(response);
                });
            }
            else {
                var sql = "SELECT id FROM `user_ride` WHERE rider_id =? && ride_running_status =? LIMIT 1";
                connection.query(sql, [requestedParams.user_id, constants.userRideCompleteStatus.RUNNING], function (err, resultFromRide) {
                    if (resultFromRide.length > 0) {
                        var extraData = ['first_name'];
                        func.getUserInformationThroughUserId(requestedParams.user_id, extraData, function (err, responseFromUser) {
                            var errorMessage = responseFromUser[0].first_name + " isn't available for the ride."
                            var response = {error: errorMessage, flag: 25}
                            res.jsonp(response);
                        });
                    }
                    else {
                        func.authenticateUser(requestedParams.user_id, function (err, response) {
                            if (err) {
                                var errorMessage = err[0].first_name + " isn't available for the ride."
                                var response = {error: errorMessage, flag: 25}
                                res.jsonp(response);
                            }
                            else {
                        rideArray = {};
                        userArray = [];
                        pushResponse = [];
                        var conversationArray = [];
                        async.series([
                            function (callback) {
                                getPushNotificationsData(resultsFromAccessToken, requestedParams, function (err, responseFromPush) {
                                    if (err) return callback(err);
                                    else if ((responseFromPush.user[0].profile === constants.userProfileType.DRIVER) && (resultsFromAccessToken[0].profile == constants.userProfileType.DRIVER)) {
                                        var response = constants.responseErrors.OPPONENT_ALSO_SET_AS_DRIVER;
                                        return callback(response);
                                    }
                                    else if (responseFromPush.user[0].profile === constants.userProfileType.DRIVER) {
                                        var response = constants.responseErrors.UNABLE_TO_SET_ON_MY_WAY;
                                        return callback(response);
                                    }
                                    else {
                                        pushResponse = responseFromPush;
                                        callback();
                                    }
                                });
                            },
                            function (callback) {
                                var sql = "INSERT INTO `user_ride`(`driver_id`,`rider_id`,`ride_status`,`driver_start_latitude`,`driver_start_longitude`,";
                                sql += "`driver_start_location_string`,`ride_running_status`) VALUES (?,?,?,?,?,?,?)";
                                connection.query(sql, [resultsFromAccessToken[0].user_id, requestedParams.user_id, constants.userRideStatus.ON_MY_WAY, requestedParams.driver_start_latitude,
                                    requestedParams.driver_start_longitude, requestedParams.driver_start_location_string, constants.userRideCompleteStatus.RUNNING], function (err, resultFromUserRide) {
                                    if (err) {
                                        var response = constants.responseErrors.UPDATION_ERROR;
                                        return callback(err, null);
                                    }
                                    else {
                                        var sql = "UPDATE `user` SET `profile`=? WHERE `user_id` = ? LIMIT 1";
                                        connection.query(sql, [constants.userProfileType.RIDER, requestedParams.user_id], function (err, result) {
                                            if (!err) {
                                                rideArray.ride_id = resultFromUserRide.insertId;
                                                if (resultsFromAccessToken[0].profile == constants.userProfileType.BOTH) {
                                                    var sql = "UPDATE `user` SET `profile`=? WHERE `user_id` = ? LIMIT 1";
                                                    connection.query(sql, [constants.userProfileType.DRIVER, resultsFromAccessToken[0].user_id], function (err, result) {
                                                    });
                                                }
                                                callback();
                                            }
                                        });
                                    }
                                });
                            }, function (callback) {
                                var sql = "UPDATE `trip` SET `profile`=? WHERE `id` = ? LIMIT 1";
                                connection.query(sql, [constants.userProfileType.RIDER, pushResponse.user[0].default_trip], function (err, result) {
                                    if (!err) {
                                        var sql = "UPDATE `trip` SET `profile`=? WHERE `id` = ? LIMIT 1";
                                        connection.query(sql, [constants.userProfileType.DRIVER, resultsFromAccessToken[0].default_trip], function (err, result) {
                                        });
                                        callback();
                                    }
                                });
                            }, function (callback) {
                                var extraData = ['start_latitude', 'start_longitude', 'start_trip_string'];
                                func.getUserTripInformation(pushResponse.user[0].default_trip, extraData, function (err, responseFromTrip) {
                                    if (err) return callback(err);
                                    userTripArray = responseFromTrip;
                                    callback();
                                });
                            }], function (err, callbackResponse) {
                            if (err) {
                                res.jsonp(err);
                            }
                            else {
                                res.jsonp(rideArray);
                                var message = userName + constants.pushMessageToUser.ON_MY_WAY;
                                var androidTokenArray = new Array();
                                var iosTokenArray = new Array();
                                if (pushResponse.user[0].android_device_token != '') {
                                    androidTokenArray.push(pushResponse.user[0].android_device_token);
                                }
                                if (pushResponse.user[0].ios_device_token != '' || pushResponse.user[0].ios_device_token != '(null)') {
                                    iosTokenArray.push(pushResponse.user[0].ios_device_token);
                                }
                                var homeLatitude = userTripArray[0].start_latitude;
                                var homeLongitude = userTripArray[0].start_longitude;
                                var homeLocationString = userTripArray[0].start_trip_string;
                                var rideId = rideArray.ride_id;
                                var riderId = requestedParams.user_id;
                                var senderId = resultsFromAccessToken[0].user_id;
                                var senderName = userName;
                                var senderImage = resultsFromAccessToken[0].image;
                                var chatId = pushResponse.conversation.chat_id;
                                var driverStartLatitude = requestedParams.driver_start_latitude;
                                var driverStartLongitude = requestedParams.driver_start_longitude;
                                var unReadCount = pushResponse.unread[0].unread + 1;
                                var payload = {trip_home_latitude: homeLatitude, trip_home_longitude: homeLongitude, "badge": unReadCount,
                                    trip_home_location_string: homeLocationString, "type": constants.pushNotificationsType.ON_MY_WAY, driver_name: userName, ride_id: rideId,
                                    "sender_id": senderId, "sender_name": senderName,
                                    "sender_image": senderImage, chat_id: chatId, message: message,
                                    "driver_start_latitude": driverStartLatitude,
                                    "driver_start_longitude": driverStartLongitude};
                                updateRiderMissedPushStatusForPickUpLocation(rideId, constants.rideMissedPushStatus.ADD_NEW_DETAILS_FOR_TRACK, homeLatitude, homeLongitude, homeLocationString, riderId, resultsFromAccessToken[0].user_id,
                                    userName, resultsFromAccessToken[0].image, chatId,
                                    driverStartLatitude, driverStartLongitude, function (err, responseFromMissedPush) {
                                        if (androidTokenArray.length > 0) {
                                            func.androidPushNotification(androidTokenArray, payload);
                                        }
                                        if (iosTokenArray.length > 0) {
                                            var iosUnread = new Array();
                                            iosUnread.push(unReadCount);
                                            func.iosPushNotification(iosTokenArray, message, payload, iosUnread);
                                        }
                                    });
                            }
                        });
                    }
                });
                    }
                });
            }
        });
    }
    else if (rideStatus === constants.userRideStatus.I_AM_HERE) {
        responseFromPush = [];
        //
        async.series([
            function (callback) {
                var sql = "UPDATE `user_ride` SET `ride_status`=?";
                sql += " WHERE `id` = ?  LIMIT 1";
                connection.query(sql, [constants.userRideStatus.I_AM_HERE,
                    requestedParams.ride_id], function (err, result) {
                    if (err) return callback(err);
                    callback();
                });
            },
            function (callback) {
                getPushNotificationsData(resultsFromAccessToken, requestedParams, function (err, responseFromPushNotification) {
                    if (err) return callback(err);
                    responseFromPush = responseFromPushNotification;
                    callback();
                });
            },
            function (callback) {
                updateRiderMissedPushStatusForPickUpLocation(requestedParams.ride_id, constants.rideMissedPushStatus.REMOVE_DETAILS,
                    '', '', '', '', '', '', '', '', '', '', function (err, resultsFromMissedPush) {
                        if (err) return callback(err);
                        callback();
                    });
            }], function (err, callbackResponse) {
            if (err) {
                var response = constants.responseErrors.UPDATION_ERROR;
                res.jsonp(response);
            }
            else {
                var response = {ride_id: requestedParams.ride_id }
                res.jsonp(response);
                var message = userName + constants.pushMessageToUser.I_AM_HERE;
                var androidTokenArray = new Array();
                var iosTokenArray = new Array();
                if (responseFromPush.user[0].android_device_token != '') {
                    androidTokenArray.push(responseFromPush.user[0].android_device_token);
                }
                if (responseFromPush.user[0].ios_device_token != '' || responseFromPush.user[0].ios_device_token != '(null)') {
                    iosTokenArray.push(responseFromPush.user[0].ios_device_token);
                }
                var senderId = resultsFromAccessToken[0].user_id;
                var senderName = userName;
                var senderImage = resultsFromAccessToken[0].image;
                var chatId = responseFromPush.conversation.chat_id;
                var unReadCount = responseFromPush.unread[0].unread + 1;
                var payload = {
                    "type": constants.pushNotificationsType.I_AM_HERE_OR_START_OR_END_RIDE, driver_name: userName, ride_id: requestedParams.ride_id,
                    "sender_id": senderId, "sender_name": senderName, "badge": unReadCount,
                    "sender_image": senderImage, chat_id: chatId, message: message};
                if (androidTokenArray.length > 0) {
                    func.androidPushNotification(androidTokenArray, payload);
                }
                if (iosTokenArray.length > 0) {
                    var iosUnread = new Array();
                    iosUnread.push(unReadCount);
                    func.iosPushNotification(iosTokenArray, message, payload, iosUnread);
                }
            }
        });
    }
    else if (rideStatus === constants.userRideStatus.START_RIDE) {
        var sql = "UPDATE `user_ride` SET `ride_status`=?, `ride_start_date_time`=?, `ride_start_latitude`=?, `ride_start_longitude`=?";
        sql += " WHERE `id` = ?  LIMIT 1";
        connection.query(sql, [constants.userRideStatus.START_RIDE, requestedParams.ride_start_date_time, requestedParams.ride_start_latitude, requestedParams.ride_start_longitude,
            requestedParams.ride_id], function (err, result) {
            if (err) {
                var response = constants.responseErrors.UPDATION_ERROR;
                res.jsonp(response);
            }
            else {
                getPushNotificationsData(resultsFromAccessToken, requestedParams, function (err, responseFromPush) {
                    if (err) {
                        res.jsonp(err);
                    }
                    else {
                        var response = {ride_id: requestedParams.ride_id }
                        res.jsonp(response);
                        var message = userName + constants.pushMessageToUser.START_RIDE;
                        var androidTokenArray = new Array();
                        var iosTokenArray = new Array();
                        if (responseFromPush.user[0].android_device_token != '') {
                            androidTokenArray.push(responseFromPush.user[0].android_device_token);
                        }
                        if (responseFromPush.user[0].ios_device_token != '' || responseFromPush.user[0].ios_device_token != '(null)') {
                            iosTokenArray.push(responseFromPush.user[0].ios_device_token);
                        }
                        var senderId = resultsFromAccessToken[0].user_id;
                        var senderName = userName;
                        var senderImage = resultsFromAccessToken[0].image;
                        var chatId = responseFromPush.conversation.chat_id;
                        var unReadCount = responseFromPush.unread[0].unread + 1;
                        var payload = {
                            "type": constants.pushNotificationsType.I_AM_HERE_OR_START_OR_END_RIDE, driver_name: userName, ride_id: requestedParams.ride_id,
                            "sender_id": senderId, "sender_name": senderName, "badge": unReadCount,
                            "sender_image": senderImage, chat_id: chatId, message: message};
                        if (androidTokenArray.length > 0) {
                            func.androidPushNotification(androidTokenArray, payload);
                        }
                        if (iosTokenArray.length > 0) {
                            var iosUnread = new Array();
                            iosUnread.push(unReadCount);
                            func.iosPushNotification(iosTokenArray, message, payload, iosUnread);
                        }
                    }
                });
            }
        });
    }
    else if (rideStatus === constants.userRideStatus.END_RIDE) {
        pushArray = [];
        rideResponse = [];
        var miles = (requestedParams.distance / 1609.34);
        var amount = (miles / 4);
        async.series([
            function (callback) {
                var sql = "UPDATE `user_ride` SET `ride_status`=?, `ride_end_date_time`=?, `distance`=?, `ride_end_latitude`=?,";
                sql += "`ride_end_longitude`=?, `ride_start_location_string`=?, `ride_end_location_string`=?, `amount`=?";
                sql += " WHERE `id` = ?  LIMIT 1";
                connection.query(sql, [constants.userRideStatus.END_RIDE, requestedParams.ride_end_date_time, requestedParams.distance,
                    requestedParams.ride_end_latitude, requestedParams.ride_end_longitude, requestedParams.ride_start_location_string,
                    requestedParams.ride_end_location_string, amount, requestedParams.ride_id], function (err, result) {
                    var response = constants.responseErrors.UPDATION_ERROR;
                    console.log(err);
                    if (err) return callback(response);
                    callback();
                });
            },
            function (callback) {
                getPushNotificationsData(resultsFromAccessToken, requestedParams, function (err, responseFromPush) {
                    if (err) return callback(err);
                    pushArray = responseFromPush;
                    callback();
                });
            },
            function (callback) {
                func.getUserRideDetails(requestedParams.user_id, resultsFromAccessToken[0].user_id, function (err, responseFromRides) {
                    if (err) return callback(err);
                    rideResponse = responseFromRides;
                    callback();
                });
            }], function (err, callbackResponse) {
            if (err) {
                res.jsonp(err);
            }
            else {
                var rideStartDateTime = moment(rideResponse[0].ride_start_date_time).format("YYYY-MM-DD H:mm ss");
                var rideEndDateTime = moment(rideResponse[0].ride_end_date_time).format("YYYY-MM-DD H:mm ss");
                var rideStartLocationString = requestedParams.ride_start_location_string;
                var rideEndLocationString = requestedParams.ride_end_location_string;
                var distance = requestedParams.distance;
                var responseRide = {ride_id: requestedParams.ride_id, ride_start_location_string: rideStartLocationString, ride_end_location_string: rideEndLocationString,
                    ride_start_date_time: rideStartDateTime, ride_end_date_time: rideEndDateTime, distance: distance, amount: amount, ride_status: constants.userRideStatus.END_RIDE };
                res.jsonp(responseRide);
                var message = userName + constants.pushMessageToUser.END_RIDE;
                var androidTokenArray = new Array();
                var iosTokenArray = new Array();
                if (pushArray.user[0].android_device_token != '') {
                    androidTokenArray.push(pushArray.user[0].android_device_token);
                }
                if (pushArray.user[0].ios_device_token != '' || pushArray.user[0].ios_device_token != '(null)') {
                    iosTokenArray.push(pushArray.user[0].ios_device_token);
                }
                var senderId = resultsFromAccessToken[0].user_id;
                var senderName = userName;
                var senderImage = resultsFromAccessToken[0].image;
                var chatId = pushArray.conversation.chat_id;
                var unReadCount = pushArray.unread[0].unread + 1;
                var payload = {
                    "type": constants.pushNotificationsType.I_AM_HERE_OR_START_OR_END_RIDE, driver_name: userName, ride_id: requestedParams.ride_id,
                    "sender_id": senderId, "sender_name": senderName,
                    "sender_image": senderImage, chat_id: chatId, message: message, "badge": unReadCount,
                    ide_start_location_string: rideStartLocationString, ride_end_location_string: rideEndLocationString,
                    ride_start_date_time: rideStartDateTime, ride_end_date_time: rideEndDateTime, distance: distance,
                    amount: amount, ride_status: constants.userRideStatus.END_RIDE};
                if (androidTokenArray.length > 0) {
                    func.androidPushNotification(androidTokenArray, payload);
                }
                if (iosTokenArray.length > 0) {
                    var iosUnread = new Array();
                    iosUnread.push(unReadCount);
                    func.iosPushNotification(iosTokenArray, message, payload, iosUnread);
                }
            }
        });
    }
    else if (rideStatus === constants.userRideStatus.PAYMENT_OR_RATING) {
        async.series([
            function (callback) {
                func.getUserRideDetails(requestedParams.user_id, resultsFromAccessToken[0].user_id, function (err, responseFromRides) {
                    if (err) return callback(err);
                    rideResponseArray = responseFromRides;
                    callback();
                });
            },
            function (callback) {
                //Checking if driver rate rider
                if (resultsFromAccessToken[0].user_id == rideResponseArray[0].driver_id) {
                    //Checking if rider already rated the driver then complete the whole ride
                    if (rideResponseArray[0].is_ride_completed_by_rider == constants.rideCompletedByDriverOrRider.COMPLETED) {
                        var sql = "UPDATE `user_ride` SET `ride_status`=?, `ride_running_status`=?, `rider_rating`=?,";
                        sql += " `is_ride_completed_by_driver`=?,`rider_pickup_latitude`=?, `rider_pickup_longitude`=?,`driver_start_latitude`=?,";
                        sql += " `driver_start_longitude`=?,`is_set_pickup_location_by_rider`=? WHERE `id` = ?  LIMIT 1";
                        connection.query(sql, [constants.userRideStatus.NEW_OR_COMPLETED_RIDE, constants.userRideCompleteStatus.COMPLETED,
                            requestedParams.rating, constants.rideCompletedByDriverOrRider.COMPLETED, constants.defaultUserLocationValue.LATITUDE,
                            constants.defaultUserLocationValue.LONGIUDE, constants.defaultUserLocationValue.LATITUDE,
                            constants.defaultUserLocationValue.LONGIUDE, constants.userRideStatus.NEW_OR_COMPLETED_RIDE, requestedParams.ride_id], function (err, result) {
                            var response = constants.responseErrors.UPDATION_ERROR;
                            console.log(err);
                            if (err) return callback(response);
                            callback();
                        });
                    }
                    //Else update driver ride completion status
                    else {
                        var sql = "UPDATE `user_ride` SET `is_ride_completed_by_driver`=?, `rider_rating`=?, `ride_status`=?";
                        sql += " WHERE `id` = ?  LIMIT 1";
                        connection.query(sql, [constants.rideCompletedByDriverOrRider.COMPLETED,
                            requestedParams.rating, constants.userRideStatus.PAYMENT_OR_RATING,
                            requestedParams.ride_id], function (err, result) {
                            var response = constants.responseErrors.UPDATION_ERROR;
                            console.log(err);
                            if (err) return callback(response);
                            callback();
                        });
                    }
                }
                //Checking if rider rate driver
                else {
                    //Checking if driver already rated the rider then complete the whole ride
                    if (rideResponseArray[0].is_ride_completed_by_driver == constants.rideCompletedByDriverOrRider.COMPLETED) {
                        var sql = "UPDATE `user_ride` SET `ride_status`=?, `ride_running_status`=?, `driver_rating`=?,";
                        sql += " `is_ride_completed_by_rider`=?,`rider_pickup_latitude`=?, `rider_pickup_longitude`=?,`driver_start_latitude`=?,";
                        sql += " `driver_start_longitude`=?,`is_set_pickup_location_by_rider`=?  WHERE `id` = ?  LIMIT 1";
                        connection.query(sql, [constants.userRideStatus.NEW_OR_COMPLETED_RIDE, constants.userRideCompleteStatus.COMPLETED,
                            requestedParams.rating, constants.rideCompletedByDriverOrRider.COMPLETED, constants.defaultUserLocationValue.LATITUDE,
                            constants.defaultUserLocationValue.LONGIUDE, constants.defaultUserLocationValue.LATITUDE,
                            constants.defaultUserLocationValue.LONGIUDE, constants.userRideStatus.NEW_OR_COMPLETED_RIDE, requestedParams.ride_id], function (err, result) {
                            var response = constants.responseErrors.UPDATION_ERROR;
                            console.log(err);
                            if (err) return callback(response);
                            callback();
                        });
                    }
                    //Else update rider ride completion status
                    else {
                        var sql = "UPDATE `user_ride` SET `ride_status`=?,`is_ride_completed_by_rider`=?, `driver_rating`=?";
                        sql += " WHERE `id` = ?  LIMIT 1";
                        connection.query(sql, [constants.userRideStatus.PAYMENT_OR_RATING,
                            constants.rideCompletedByDriverOrRider.COMPLETED, requestedParams.rating,
                            requestedParams.ride_id], function (err, result) {
                            var response = constants.responseErrors.UPDATION_ERROR;
                            console.log(err);
                            if (err) return callback(response);
                            callback();
                        });
                    }
                }
            }], function (err, callbackResponse) {
            if (err) {
                res.jsonp(err);
            }
            else {
                var response = {ride_id: requestedParams.ride_id }
                res.jsonp(response);
            }
        });
    }
}
/*
 * -----------------------------------------------------------------------------
 * Update ride information
 * Input : access token
 * Output :
 * -----------------------------------------------------------------------------
 */
exports.updateRiderPickupLocationThroughAccessToken = function (req, res) {
    var accessToken = req.body.access_token;
    var requestedParams = req.body;
    var manValues = [accessToken];
    console.log(req.body)
    func.authenticateMandatoryFields(manValues, res, function (checkData) {
        var extraData = ['first_name', 'last_name', "CASE WHEN `image` LIKE 'http%'" +
            " THEN `image` ELSE CONCAT('" + config.get('awsS3Bucketsettings.userImageBasePath') + "',`image`) END as image"];
        func.authenticateAccessTokenAndReturnExtraData(accessToken, extraData, res, function (resultsFromAccessToken) {
            func.authenticateUser(requestedParams.user_id, function (err, response) {
                if (err) {
                    var errorMessage = err[0].first_name + " isn't available for the ride."
                    var response = {error: errorMessage, flag: 25}
                    res.jsonp(response);
                }
                else {
                    func.verifyUserForBlocking(requestedParams.user_id, resultsFromAccessToken[0].user_id, function (err, response) {
                        if (!err) {
                            updateRiderPickupLocationThroughAccessTokenResult(resultsFromAccessToken, requestedParams, res, req);
                        }
                        else {
                            res.jsonp(err);
                        }
                    });
                }
            });
        });
    });
};
function updateRiderPickupLocationThroughAccessTokenResult(resultsFromAccessToken, requestedParams, res, req) {
    var rideStatus = parseInt(requestedParams.ride_status);
    var userName = resultsFromAccessToken[0].first_name;
    if (rideStatus === constants.userRideStatus.CANCEL_RIDE) {
        pushArray = [];
        async.series([
            function (callback) {
                var sql = "UPDATE `user_ride` SET `ride_status`=?, `rider_pickup_latitude`=?, `rider_pickup_longitude`=?,`driver_start_latitude`=?,`driver_start_longitude`=?,";
                sql += " `is_set_pickup_location_by_rider`=?, `ride_running_status`=? WHERE `id` = ?  LIMIT 1";
                connection.query(sql, [constants.userRideStatus.CANCEL_RIDE, constants.defaultUserLocationValue.LATITUDE, constants.defaultUserLocationValue.LONGIUDE,
                    constants.defaultUserLocationValue.LATITUDE, constants.defaultUserLocationValue.LONGIUDE, constants.userRideStatus.NEW_OR_COMPLETED_RIDE,
                    constants.userRideCompleteStatus.NOT_COMPLETED, requestedParams.ride_id], function (err, result) {
                    if (err) return callback(err);
                    callback();
                });
            },
            function (callback) {
                getPushNotificationsData(resultsFromAccessToken, requestedParams, function (err, responseFromPush) {
                    if (err) return callback(err)
                    pushResponse = responseFromPush;
                    callback();
                });
            },
            function (callback) {
                updateRiderMissedPushStatusForPickUpLocation(requestedParams.ride_id, constants.rideMissedPushStatus.REMOVE_DETAILS, '', '', '', '', '', '', '', '', '', '', function (err, resultsFromMissedPush) {
                    if (err) return callback(err);
                    callback();
                });
            }], function (err, callbackResponse) {
            if (err) {
                var response = constants.responseErrors.UPDATION_ERROR;
                res.jsonp(response);
            }
            else {
                var response = constants.responseSuccess.RIDE_INFORMATION_UPDATED_SUCCESSFULLY;
                res.jsonp(response);
                var message = userName + constants.pushMessageToUser.CANCEL_RIDE;
                var androidTokenArray = new Array();
                var iosTokenArray = new Array();
                if (pushResponse.user[0].android_device_token != '') {
                    androidTokenArray.push(pushResponse.user[0].android_device_token);
                }
                if (pushResponse.user[0].ios_device_token != '' || pushResponse.user[0].ios_device_token != '(null)') {
                    iosTokenArray.push(pushResponse.user[0].ios_device_token);
                }
                var unReadCount = pushResponse.unread[0].unread + 1;
                var senderId = resultsFromAccessToken[0].user_id;
                var senderName = userName;
                var senderImage = resultsFromAccessToken[0].image;
                var chatId = pushResponse.conversation.chat_id;
                var payload = {
                    "type": constants.pushNotificationsType.CANCEL_RIDE, driver_name: userName, ride_id: requestedParams.ride_id,
                    "sender_id": senderId, "sender_name": senderName, "badge": unReadCount,
                    "sender_image": senderImage, "chat_id": chatId, "message": message,
                    "ride_status": constants.userRideStatus.CANCEL_RIDE};
                if (androidTokenArray.length > 0) {
                    func.androidPushNotification(androidTokenArray, payload);
                }
                if (iosTokenArray.length > 0) {
                    var iosUnread = new Array();
                    iosUnread.push(unReadCount);
                    func.iosPushNotification(iosTokenArray, message, payload, iosUnread);
                }
            }
        });
    }
    else if (rideStatus === constants.riderPickUpStatus.UPDATED_BY_RIDER) {
        pushArray = [];
        async.series([
            function (callback) {
                var sql = "UPDATE `user_ride` SET `is_set_pickup_location_by_rider`=?, `rider_pickup_latitude`=?,`rider_pickup_longitude`=?,`rider_pickup_location_string`=?,";
                sql += " `route_string_location`=? WHERE `id`=? LIMIT 1";
                connection.query(sql, [constants.defaultUserSettingValue.ENABLE, requestedParams.rider_pickup_latitude, requestedParams.rider_pickup_longitude,
                    requestedParams.rider_pickup_location_string, requestedParams.route_string_location, requestedParams.ride_id], function (err, result) {
                    if (err) return callback(err);
                    callback();
                });
            },
            function (callback) {
                getPushNotificationsData(resultsFromAccessToken, requestedParams, function (err, responseFromPush) {
                    if (err) return callback(err)
                    pushResponse = responseFromPush;
                    callback();
                });
            }, function (callback) {
                updateRiderMissedPushStatusForPickUpLocation(requestedParams.ride_id, constants.rideMissedPushStatus.REMOVE_DETAILS, '', '', '', '', '', '', '', '', '', '', function (err, resultsFromMissedPush) {
                    if (err) return callback(err);
                    callback();
                });
            }], function (err, callbackResponse) {
            if (err) {
                var response = constants.responseErrors.UPDATION_ERROR;
                res.jsonp(response);
            }
            else {
                var response = constants.responseSuccess.RIDE_INFORMATION_UPDATED_SUCCESSFULLY;
                res.jsonp(response);
                var message = constants.pushMessageToUser.PICKUP_LOCATION_SET_BY_RIDER + userName;
                var androidTokenArray = new Array();
                var iosTokenArray = new Array();
                if (pushResponse.user[0].android_device_token != '') {
                    androidTokenArray.push(pushResponse.user[0].android_device_token);
                }
                if (pushResponse.user[0].ios_device_token != '' || pushResponse.user[0].ios_device_token != '(null)') {
                    iosTokenArray.push(pushResponse.user[0].ios_device_token);
                }
                var unReadCount = pushResponse.unread[0].unread + 1;
                var senderId = resultsFromAccessToken[0].user_id;
                var senderName = userName;
                var senderImage = resultsFromAccessToken[0].image;
                var chatId = pushResponse.conversation.chat_id;
                var payload = {
                    "type": constants.pushNotificationsType.PICK_UP_LOCATION_UPDATED_BY_RIDER, driver_name: userName, ride_id: requestedParams.ride_id,
                    "sender_id": senderId, "sender_name": senderName, "badge": unReadCount,
                    "sender_image": senderImage, chat_id: chatId, message: message,
                    "rider_pickup_latitude": requestedParams.rider_pickup_latitude, "rider_pickup_longitude": requestedParams.rider_pickup_longitude,
                    "rider_pickup_location_string": requestedParams.rider_pickup_location_string, "route_string_location": requestedParams.route_string_location};
                if (androidTokenArray.length > 0) {
                    func.androidPushNotification(androidTokenArray, payload);
                }
                if (iosTokenArray.length > 0) {
                    var iosUnread = new Array();
                    iosUnread.push(unReadCount);
                    func.iosPushNotification(iosTokenArray, message, payload, iosUnread);
                }
            }
        });
    }
    else if (rideStatus === constants.riderPickUpStatus.UPDATED_BY_DRIVER) {
        var sql = "SELECT `is_set_pickup_location_by_rider` FROM `user_ride`";
        sql += "  WHERE `id`=? LIMIT 1 ";
        connection.query(sql, [requestedParams.ride_id], function (err, resultsFromRide) {
            if (err) {
                var response = constants.responseErrors.UPDATION_ERROR;
                res.jsonp(response);
            }
            else {
                if (resultsFromRide[0].is_set_pickup_location_by_rider == constants.defaultUserSettingValue.ENABLE) {
                    var response = constants.responseErrors.RIDER_ALREADY_SET_PICKUP_LOCATION;
                    res.jsonp(response);
                }
                else {
                    var sql = "UPDATE `user_ride` SET `rider_pickup_latitude`=?,`rider_pickup_longitude`=?,`rider_pickup_location_string`=?,";
                    sql += " `route_string_location`=? WHERE `id`=? LIMIT 1";
                    connection.query(sql, [requestedParams.rider_pickup_latitude, requestedParams.rider_pickup_longitude,
                        requestedParams.rider_pickup_location_string, requestedParams.route_string_location, requestedParams.ride_id], function (err, result) {
                        if (!err) {
                            var response = constants.responseSuccess.RIDE_INFORMATION_UPDATED_SUCCESSFULLY;
                            res.jsonp(response);
                        }
                        else {
                            console.log(err);
                            var response = constants.responseErrors.UPDATION_ERROR;
                            res.jsonp(response);
                        }
                    });
                }
            }
        });
    }
}
/*
 * -----------------------------------------------------------------------------
 * Get push notifications data
 * Input : access token result and requested params
 * Output : callback
 * -----------------------------------------------------------------------------
 */
function getPushNotificationsData(resultsFromAccessToken, requestedParams, callback) {
    pushArray = [];
    async.series([
        function (callback) {
            var extraData = ['first_name', 'user_type', 'ios_device_token', 'android_device_token', 'default_trip', 'profile'];
            func.getUserInformationThroughUserId(requestedParams.user_id, extraData, function (err, responseFromUser) {
                if (err) return callback(err);
                pushArray.user = responseFromUser;
                callback();
            });
        },
        function (callback) {
            func.getConversationDetailsBetweenTwoUser(resultsFromAccessToken[0].user_id, requestedParams.user_id, function (err, resultsFromConversationUsers) {
                if (err) return callback(err);
                pushArray.conversation = resultsFromConversationUsers;
                callback();
            });
        },
        function (callback) {
            getUnreadMessageCount(requestedParams.user_id, function (err, resultFromUnread) {
                if (err) return callback(err);
                pushArray.unread = resultFromUnread;
                callback();
            });
        }], function (err, callbackResponse) {
        if (err) {
            return callback(err, null);
        }
        else {
            return callback(null, pushArray);
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Update rider missed push status for pickup location
 * Input :
 * Output :
 * -----------------------------------------------------------------------------
 */
function updateRiderMissedPushStatusForPickUpLocation(rideId, flag, homeLatitude, homeLongitude, homeLocationString, riderId, senderId, senderName, senderImage, chatId, driverStartLatitude, driverStartLongitude, callback) {
    if (flag == constants.rideMissedPushStatus.ADD_NEW_DETAILS_FOR_TRACK) {
        var sql = "INSERT INTO `rider_pickup_location_missed_push` (`trip_home_latitude`,`rider_id`,`trip_home_longitude`,`trip_home_location_string`,`ride_id`,`sender_id`,`sender_name`,";
        sql += "`sender_image`,`chat_id`,`driver_start_latitude`,`driver_start_longitude`)";
        sql += " VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        connection.query(sql, [homeLatitude, riderId, homeLongitude,
            homeLocationString, rideId, senderId, senderName, senderImage, chatId, driverStartLatitude, driverStartLongitude], function (err, resultFromInsertion) {
            if (err) {
                console.log(err);
                var response = constants.responseErrors.UPDATION_ERROR;
                return callback(response, null);
            }
            else {
                return callback(null, resultFromInsertion.insertId);
            }
        });
    }
    else {
        var sql = "UPDATE `rider_pickup_location_missed_push` SET `is_deleted`=?";
        sql += "  WHERE `ride_id`=? LIMIT 1";
        connection.query(sql, [constants.userDeleteStatus.DELETED, rideId], function (err, result) {
            if (err) {
                console.log(err);
                var response = constants.responseErrors.UPDATION_ERROR;
                return callback(response, null);
            }
            else {
                return callback(null, 1);
            }
        });
    }
}
function getUnreadMessageCount(opponentId, callback) {
    var sql = "SELECT COUNT(`read`)as unread, receiver_id FROM `chat` WHERE `receiver_id` =? && `read`=?";
    connection.query(sql, [opponentId, 0], function (err, resultFromUnRead) {
        if (!err) {
            return callback(null, resultFromUnRead)
        }
        else {
            var response = constants.responseErrors.UPDATION_ERROR;
            return callback(response, null);
        }
    });
}
/*
 * -----------------------------------------------------------------------------
 * Calculate array difference
 * Input : first array, second array
 * Output : return difference
 * -----------------------------------------------------------------------------
 */
function arrayDiff(a1, a2) {
    var o1 = {}, o2 = {}, diff = [], i, len, k;
    for (i = 0, len = a1.length; i < len; i++) {
        o1[a1[i]] = true;
    }
    for (i = 0, len = a2.length; i < len; i++) {
        o2[a2[i]] = true;
    }
    for (k in o1) {
        if (!(k in o2)) {
            diff.push(k);
        }
    }
    for (k in o2) {
        if (!(k in o1)) {
            diff.push(k);
        }
    }
    return diff;
}
