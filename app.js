
/**
 * Module dependencies.
 */
process.env.NODE_ENV = 'production';
process.env.NODE_CONFIG_DIR = 'config/';
config = require('config');
var express = require('express');
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');
var favicon = require('static-favicon');
var errorhandler = require('errorhandler');
var logger = require('morgan');
var methodOverride = require('method-override');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var users = require('./routes/users');
var trip = require('./routes/trip');
var conversation = require('./routes/conversation');
var miscellaneous = require('./routes/miscellaneous');
var admin = require('./routes/admin');
connection          = undefined;
var mysqlLib = require('./routes/mysqlLib');


var app = express();

// all environments
var favicon = require('serve-favicon');
app.set('port', process.env.PORT || config.get('appSettings.port'));
app.set('views', path.join(__dirname, 'views'));
app.set('json spaces', 1);
app.set('view engine', 'jade');
app.use(favicon(__dirname + '/views/favicon.ico'));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(logger('dev'));
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));


// development only
if ('development' == app.get('env')) {
    app.use(errorhandler());
}
app.get('/test', function (req, res) {
    res.render('test');
});
users
app.post('/register_or_login_through_facebook', users.registerOrLoginThroughFacebook);
app.post('/login_through_accesstoken', users.loginFromAccessToken);
app.post('/get_user_profile_data', users.getUserProfileDataFromAccessToken);
app.use('/edit_user_profile_data', multipartMiddleware);
app.post('/edit_user_profile_data', users.editUserProfileDataFromAccessToken);
app.post('/register_user_through_web', users.registerNewUserFromWebsite);
app.post('/create_admin', users.createAdmin);

trip
app.post('/create_new_user_trip', trip.createNewUserTripThroughAccessToken);
app.post('/get_all_user_matches', trip.getAllMatchesOfUserDefaultTrip);
app.post('/hytch_or_dytch_user_based_on_matches', trip.hytchOrDytchUserBasedOnUserMatches);
app.post('/edit_or_delete_user_trip', trip.editOrDeleteTripThroughAccessToken);
app.post('/get_all_user_trips', trip.getAllUserTripsThroughAccessToken);
app.post('/authenticate_trip_name', trip.authenticateTripNameFromAccessToken);

//conversation
app.post('/get_all_user_conversation', conversation.getAllUserConversationListThroughAccessToken);
app.post('/send_new_message', conversation.sendNewMessage);
app.post('/get_unread_messages', conversation.getUnReadMessagesFromChat);
app.post('/get_all_messages', conversation.getAllMessagesFromChat);
app.post('/rate_user_partner_during_conversation', conversation.rateUserPartnerDuringConversation);
app.post('/check_location_during_conversation', conversation.checkLocationDuringConversation);
app.post('/enable_disable_location_sharing_during_conversation', conversation.enableDisableLocationSharingDuringConversation);
app.post('/update_user_ride_information', conversation.updateRideInformationThroughAccessToken);
app.post('/update_rider_pickup_location', conversation.updateRiderPickupLocationThroughAccessToken);
app.post('/get_all_blocked_user', conversation.getAllBlockedUserThroughAccessToken);


//miscellaneous
app.post('/update_user_app_settings', miscellaneous.updateUserAppSettingsThroughAccessToken);
app.post('/delete_user_account', miscellaneous.deleteUserAccountThroughAccessToken);
app.post('/update_user_current_location', miscellaneous.updateUserLocationThroughAccessToken);
app.post('/save_trip_as_default', miscellaneous.saveTripAsDefaultThroughAccessToken);
app.post('/add_or_remove_card_from_user_profile', miscellaneous.addOrRemoveUserCardFromProfileThroughCardId);
//app.post('/mark_user_card_as_default', miscellaneous.markUserCardAsDefault);
app.post('/make_payment_for_trip', miscellaneous.MakePaymentForTrip);
app.post('/update_user_secret_key_for_payment', miscellaneous.updateUserSecretKeyForPayment);
app.post('/update_user_invitations_from_facebook', miscellaneous.updateUserInvitationsFromFacebookThroughAccessToken);
app.post('/verify_promocode', miscellaneous.verifyPromoCode);
app.post('/get_user_logged_out_from_app', miscellaneous.getUserLogoutFromApp);
app.post('/blocked_or_unblock_user', miscellaneous.blockOrUnblockUserThroughAccessToken);


//admin
app.post('/dashboard', admin.getDataForDashboard);
app.post('/allusers', admin.getDataForAllUsers);
app.post('/userdetails', admin.getDataForUserThroughUserId);
app.post('/alluserspaginated', admin.getDataForAllUsersInPagination);
app.post('/get_user_latest_feed', admin.getLatestFeedOfUserThroughUserId);
app.post('/get_login_for_admin', admin.getLoginForAdmin);
app.post('/update_user_password', admin.updateUserPasswordFromAccessToken);
app.post('/get_all_latest_feed', admin.getLatestFeedForDashboardThroughAccessToken);
app.post('/get_graph_for_all_trips', admin.getGraphForAllTrips);
app.post('/get_all_user_trips_admin', admin.getDataForAllTrips);
app.post('/get_trip_data_through_tripid', admin.getTripDataThroughTripId);
app.post('/get_mutual_interest_and_friends', admin.getMutualInterestsAndFriends);
app.post('/forgot_password', admin.forgotPassword);
app.post('/get_graph_for_all_payments', admin.getGraphForAllPayments);
app.post('/get_all_user_payments', admin.getDataForAllPayments);
app.post('/get_user_data_for_maps', admin.getUserDataForMaps);
app.post('/send_mail_to_user', admin.sendMailToUser);
app.post('/get_all_promocode', admin.getAllPromoCode);
app.post('/get_promocode_details_through_id', admin.getPromoCodeDetailsThroughId);
app.post('/create_or_update_promocode_details_through_id', admin.createOrUpdatePromoCodeDetailsThroughId);
app.post('/delete_promocode_details_through_id', admin.deletePromoCodeDetailsThroughId);
app.post('/get_all_wait_list_user', admin.getAllWaitListedUserThroughAccessToken);
app.post('/create_or_update_admin_card', admin.addOrEditCreditCard);
app.post('/get_admin_card_details', admin.getAdminCardDetails);
app.post('/clear_user_wait_list', admin.clearUserWaitList);
app.post('/get_all_ride_history', admin.getAllRideHistoryThroughAccessToken);



http.createServer(app).listen(8001, function(){
    console.log('Express server listening on port ' + app.get('port'));
});